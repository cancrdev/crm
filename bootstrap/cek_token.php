<?php
header("Content-type: application/json");

curl https://sandbox.bca.co.id/api/oauth/token \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -H "Authorization: Basic jk5ZTkyYzgtYzAzNC00YmNhLWE0OTAtYWM4NGI0YTZiMjQxOjNmYWIwNGI1LWM4ODctNGZmMi05OGNkLTE1YjJmYTcyNzA1NA==" \
  -d "grant_type=client_credentials";
?>