<?php
ini_set('date.timezone', 'Asia/Jakarta');

header('Content-Type: application/json');
include '../model/config.php';
include '../model/koneksi.php';

$connect     = new Connection();
if(isset($_GET["acces"])) :
	$accesId = $connect->clean_all($_GET["acces"]);
	if($accesId == "addsoal") :
		if(isset($_POST["kategoriId"]) && isset($_POST["materiId"]) && isset($_POST["soal"]) && isset($_POST["jawabanbenar"])) :
			$katId    = $connect->clean_all($_POST["kategoriId"]);
			$materiId = $connect->clean_all($_POST["materiId"]);
			$soal     = $connect->clean_post($_POST["soal"]);
			$jawab_a  = $connect->clean_post($_POST["jawab_a"]);
			$jawab_b  = $connect->clean_post($_POST["jawab_b"]);
			$jawab_c  = $connect->clean_post($_POST["jawab_c"]);
			$jawab_d  = $connect->clean_post($_POST["jawab_d"]);
			$true     = $connect->clean_post($_POST["jawabanbenar"]);
			$created  = $connect->clean_all($_POST["created"]);
			$status   = "READY";

			$query = mysqli_query($conn, "INSERT INTO td_news_quiz (TQ_KATID, TQ_SUBKATID, 	TQ_SOAL, TQ_A, TQ_B, TQ_C, TQ_D, TQ_TRUE, TQ_STATUS, TN_CREATED_BY) VALUES ('$katId', '$materiId', '$soal', '$jawab_a', '$jawab_b', '$jawab_c', '$jawab_d' '$true', '$status', '$created')");

			if($query == TRUE) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Soal berhasil ditambahkan";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Soal Gagal ditambahkan";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Parameter anda kurang";
			echo json_encode($response);
		endif;
	elseif($accesId == "edit") :
		$value = $_GET["value"];
		if($value != "") :
			if(isset($_POST["soal"]) || isset($_POST["jawabanbenar"])) :
				$soal = $connect->clean_post($_POST["soal"]);
				$jawab_a = $connect->clean_post($_POST["jawab_a"]);
				$jawab_b = $connect->clean_post($_POST["jawab_b"]);
				$jawab_c = $connect->clean_post($_POST["jawab_c"]);
				$jawab_d = $connect->clean_post($_POST["jawab_d"]);
				$true = $connect->clean_post($_POST["jawabanbenar"]);

				$query = mysqli_query($conn, "UPDATE td_news_quiz SET TQ_SOAL = '$soal', TQ_A = '$jawab_a', TQ_B = '$jawab_b', TQ_C = '$jawab_c', TQ_D = '$jawab_d', TQ_TRUE = '$true' WHERE TQ_BIGID = '$value'");
				if($query == TRUE) :
					$response["error"]  = FALSE;
					$response["status"] = 200;
					$response["msg"]	= "Soal berhasil diubah";
					echo json_encode($response);
				else: 
					$response["error"]  = TRUE;
					$response["status"] = 200;
					$response["msg"]	= "Soal gagal diubah";
					echo json_encode($response);
				endif;
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Parameter anda kurang";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Soal tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "delete") :
		$value = $_GET["value"];
		if($value != "") :
			$query = mysqli_query($conn, "DELETE FROM td_news_quiz WHERE TQ_BIGID = '$value'");
			if($query == TRUE) :
				$response["error"]  = FALSE;
				$response["status"] = 200;
				$response["msg"]	= "Soal berhasil dihapus";
				echo json_encode($response);
			else :
				$response["error"]  = TRUE;
				$response["status"] = 200;
				$response["msg"]	= "Soal gagal dihapus";
				echo json_encode($response);
			endif;
		else :
			$response["error"]  = TRUE;
			$response["status"] = 200;
			$response["msg"]	= "Id Soal tidak ditemukan";
			echo json_encode($response);
		endif;
	elseif($accesId == "jawabsoal") :
	elseif($accesId == "soal") :
		$value = $_GET["value"];
		$key   = $_GET["key"];
		$rows  = array();
		$query = mysqli_query($conn, "SELECT * FROM td_news_quiz WHERE TQ_KATID = '$value' AND TQ_SUBKATID = '$key' ORDER BY TQ_CREATED_AT DESC");
		while($row = mysqli_fetch_assoc($query)) :
			$rows[] = $row;
		endwhile;

		if($rows == "" || $rows == null) :
		    $response["error"]  = TRUE;
    		$response["status"] = 200;
    		$response["msg"]	= "list data soal kosong";
		    $response["TQ_BIGID"] =  "";
            $response["TQ_KATID"] = "";
            $response["TQ_SUBKATID"]   = "";
            $response["TQ_SOAL"] = "";
            $response["TQ_TRUE"] = "";
            $response["TQ_STATUS"] = "";
            $response["TN_CREATED_BY"] = "";
            $response["TQ_CREATED_AT"] = "";
            echo json_encode($response);
		else :
    		$response["error"]  = FALSE;
    		$response["status"] = 200;
    		$response["msg"]	= "list data soal permateri";
    		$response["payload"] = $rows;
    		echo json_encode($response);
    	endif;
	endif;
else :
	$response["error"]  = TRUE;
	$response["status"] = 200;
	$response["msg"]	= "Pilih acces anda terlebih dahulu";
	echo json_encode($response);
endif;

?>