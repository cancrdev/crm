<?php
//87714744309
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();
$dev = $log->countProdev();
$gg  = $log->countProEnd();
$ver = $log->projectProDev();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>CRM - DASHBOARD</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="../../assets/css/master_style.css">
	
	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
	
	<!-- fullCalendar -->
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
	
	<!-- Bootstrap switch-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
	
	<!-- Morris charts -->
	<link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	
  </head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto w-p50">
				<h3 class="page-title">Dashboard Produksi</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item active" aria-current="page">Control</li>
						</ol>
					</nav>
				</div>
			</div>
			<div class="right-title text-right w-170">
				<span class="subheader_daterange font-weight-600" id="dashboard_daterangepicker">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title"></span>
						<span class="subheader_daterange-date text-primary"></span>
					</span>
					<a href="#" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content">		
	  <div class="row">
		<div class="col-lg-8 col-12"> 
          <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">Persentase Project</h4>

              <ul class="box-controls pull-right">
                  <li><a class="box-btn-close" href="#"></a></li>
                  <li><a class="box-btn-slide" href="#"></a></li>	
                  <li><a class="box-btn-fullscreen" href="#"></a></li>
                </ul>
            </div>
            <div class="box-body">
              <div class="chart" id="e_chart_1" style="height: 362px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
		</div>
		<div class="col-lg-4 col-12"> 
          <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">Kalender <?= date("Y"); ?></h4>

              <ul class="box-controls pull-right">
				  <li class="dropdown">
                    <a data-toggle="dropdown" href="#" aria-expanded="false"><i class="ti-more-alt rotate-90"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(-114px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                      <!-- <a class="dropdown-item" href="#"><i class="ti-import"></i> Import</a>
                      <a class="dropdown-item" href="#"><i class="ti-export"></i> Export</a>
                      <a class="dropdown-item" href="#"><i class="ti-printer"></i> Print</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a> -->
                    </div>
                  </li>
                  <li><a class="box-btn-close" href="#"></a></li>
                  <li><a class="box-btn-slide" href="#"></a></li>	
                  <li><a class="box-btn-fullscreen" href="#"></a></li>
                </ul>
            </div>
            <div class="box-body p-0">				
              <!-- THE CALENDAR -->
              <div id="calendar" class="dask"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box --> 
		</div>
		<div class="col-12 col-md-6 col-lg-4 col-xl-4">
			<div class="box bg-primary pull-up">
			  <div class="box-body text-center">
				  <span>
					<i class="fa fa-wheelchair pb-10 font-size-50"></i>
				  </span><br>
				  <h1><?= $dev["jml"]; ?></h1>
				  <span>Jumlah Project Develop</span>
			  </div>
			</div>
		</div>
		<div class="col-12 col-md-6 col-lg-4 col-xl-4">
			<div class="box bg-success pull-up">
			  <div class="box-body text-center">
				  <span>
					<i class="fa fa-file pb-10 font-size-50"></i>
				  </span><br>
				  <h1><?= $gg["jml1"]; ?></h1>
				  <span>Jumlah Project Selesai</span>
			  </div>
			</div>
		</div>
		<div class="col-12 col-md-6 col-lg-4 col-xl-4">
			<div class="box bg-warning pull-up">
			  <div class="box-body text-center">
				  <span>
					<i class="fa fa-calendar pb-10 font-size-50"></i>
				  </span><br>
				  <h1><?= $gg["jml1"]; ?></h1>
				  <span>Jumlah Project Maintenance</span>
			  </div>
			</div>
		</div>
		  
		<div class="col-12 col-lg-12">
          <div class="box">
            <div class="box-header with-border">
              <h4 class="box-title">List Project Priority</h4>
              <ul class="box-controls pull-right">
                  <li><a class="box-btn-close" href="#"></a></li>
                  <li><a class="box-btn-slide" href="#"></a></li>	
                  <li><a class="box-btn-fullscreen" href="#"></a></li>
                </ul>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="bb-2">No</th>
								<th class="bb-2">Project No</th>
								<th class="bb-2">Nama Project</th>
								<th class="bb-2">Priority</th>
								<th class="bb-2">Platform</th>
								<th class="bb-2">Order Masuk</th>
								<th class="bb-2">Project Due</th>
								<!-- <th class="bb-2">Status</th> -->
								<th class="bb-2">Result</th>
								<th class="bb-2 text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(is_array($ver)) {
							$no = 1;
							foreach($ver as $vr) {
							 ?>
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $vr["TP_NO_PROJECT"]; ?></td>
								<td><?= $vr["TP_NAMA_PROJECT"]; ?></td>
								<td><span class="badge badge-warning"><?= $vr["TJ_PRIORITAS"]; ?></span></td>
								<td><?= $vr["TP_PLATFORM"]; ?></td>
								<td><?= $log->TanggalIndo($vr["TP_PROJECT_TGL"]) ?></td>
								<td><?= $log->TanggalIndo($vr["TP_PROJECT_DUE"]) ?></td>
								<!-- <td><span class="badge badge-success">Result Added</span></td> -->
								<td>
									<a href="#" data-toggle="modal" data-target="#modal-fill" class="btn btn-sm btn-dark"><i class="mdi mdi-desktop-mac"></i> Detail</a>
									<div class="modal modal-fill fade" data-backdrop="false" id="modal-fill" tabindex="-1">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title" id="result-popup">Detail Project Id <?= $vr["TP_REG_PROJECT"]; ?> - Result</h4>
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												</div>
												<div class="modal-body">
													<div class="row justify-content-between">
														<div class="col-md-7 col-12">
														  <h4>Nama Project - <?= $vr["TP_NAMA_PROJECT"]; ?></h4>
														</div>
														<div class="col-md-5 col-12">
														  <h4 class="text-right">Project No : <?= $vr["TP_NO_PROJECT"]; ?></h4>
														</div>
													</div>
													<div class="table-responsive">
														<table class="table table-bordered">
														  <thead class="bg-secondary">
															<tr>
															  <th scope="col">Keterangan</th>
															  <th scope="col">Platform</th>
															  <th scope="col">Prioritas</th>
															</tr>
														  </thead>
														  <tbody>
															<tr>
															  <td><?= $vr["TP_DESKRIPSI"]; ?></td>
															  <td><?= $vr["TP_PLATFORM"]; ?></td>
															  <td><span class="badge badge-warning"><?= $vr["TJ_PRIORITAS"]; ?></span></td>
															</tr>
															<tr>
															  <td>&nbsp;</td>
															  <td>&nbsp;</td>
															  <td>&nbsp;</td>
															</tr>
														  </tbody>
														</table>
													</div>
													<div class="comment">
														<p><span class="font-weight-600">Comment</span> : <span class="comment-here text-light">Detail Project untuk status priority</span></p>
													</div>
													<div class="table-responsive">
														<table class="table">
														  <tbody>
															<tr>
															  <th></th>
															  <th colspan="2" class="b-0">Project Masuk</th>

															  <th colspan="2" class="b-0">Project Due</th>
															</tr>
															<tr class="bg-secondary">
															  <td></td>
															  <td>Time : <?= $log->TanggalIndo($vr["TP_PROJECT_TGL"]); ?></td>
															  <td></td>
															  <td>Time : <?= $log->TanggalIndo($vr["TP_PROJECT_DUE"]); ?></td>
															</tr>
														  </tbody>
														</table>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
													<!-- <button type="button" class="btn btn-info pull-right">Print</button> -->
													<!-- <button type="button" class="btn btn-success pull-right">Save</button> -->
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</td>
								<td>
									<?php if($vr["TJ_STATUS"] === "FINISH") { ?>
									<a href="ubah-status?v=<?= $vr["TP_REG_PROJECT"]; ?>&status=PROCESS" class="btn btn-success btn-sm check-sts"><i class="mdi mdi-check"></i> Selesai</a>
									<?php } elseif($vr["TJ_STATUS"] === "PROCESS") { ?>
									<a href="ubah-status?v=<?= $vr["TP_REG_PROJECT"]; ?>&status=FINISH" class="btn btn-warning btn-sm check-sts"><i class="mdi mdi-check"></i> Develop / Revisi</a>
									<?php } else { ?>
									<a href="ubah-status?v=<?= $vr["TP_REG_PROJECT"]; ?>&status=PROCESS" class="btn btn-danger btn-sm check-sts"><i class="mdi mdi-check"></i> Scedule</a>
									<?php } ?>
								</td>
							</tr>
							<?php }} ?>						
						</tbody>
					  </table>
					  <p style="color: red;">(*) Silahkan update status terlebih dahulu,untuk verifikasi sitem kami</p>
				</div>				
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>		
		</div> 
      </div>			      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	
	<!--Model Popup Area-->
  	
	<!-- result modal content -->
	
	<!-- /.modal -->


	<!-- comment modal content -->
	<!-- /.modal -->
	
	
   <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
	  
	<div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <!-- <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li> -->
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-danger"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Admin Birthday</h4>
                <p>Will be July 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-warning"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>
                <p>New Email : jhone_doe@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-info"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>
                <p>disha@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-success"></i>
              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Code Change</h4>

                <p>Execution time 15 Days</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->
        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Web Design
                <span class="label label-danger pull-right">40%</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Data
                <span class="label label-success pull-right">75%</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Order Process
                <span class="label label-warning pull-right">89%</span>
              </h4>
              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Development 
                <span class="label label-primary pull-right">72%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->
      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">

      </div>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	 
	  
	<!-- jQuery 3 -->
	<script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- date-range-picker -->
	<script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
	
	<!-- Slimscroll -->
	<script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- Sparkline -->
	<script src="../../assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>

	<!-- fullCalendar -->
	<script src="../../assets/vendor_components/fullcalendar/fullcalendar.js"></script>
	<script src="../../assets/js/pages/calendar.js"></script>

	<!-- e-chart -->
	<script src="../../assets/vendor_components/echarts/dist/echarts-en.min.js"></script>
	<script src="../../assets/vendor_components/echarts/echarts-liquidfill.min.js"></script>
	<script src="../../assets/vendor_components/echarts/ecStat.min.js"></script>
	
	<!-- Morris.js charts -->
	<script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
	<script src="../../assets/vendor_components/morris.js/morris.min.js"></script>		
	
	<!-- Superieur Admin App -->
	<script src="../../assets/js/template.js"></script>
	
	<!-- Superieur Admin for demo purposes -->
	<script src="../../assets/js/demo.js"></script>	
	
	<!-- Superieur Admin dashboard demo-->
	<script src="../../assets/js/pages/dashboard5.js"></script>
	<script src="../../assets/js/pages/dashboard5-chart.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script type="text/javascript">
		if( $('#e_chart_1').length > 0 ){
			var eChart_3 = echarts.init(document.getElementById('e_chart_1'));
			var colors = ['#ba69aa', '#69cce0'];
			var option3 = {
				tooltip: {
					backgroundColor: 'rgba(33,33,33,1)',
					borderRadius:0,
					padding:10,
					textStyle: {
						color: '#fff',
						fontStyle: 'normal',
						fontWeight: 'normal',
						fontFamily: "'Nunito Sans', sans-serif",
						fontSize: 12
					}	
				},
				series: [
					{
						name:'',
						type:'pie',
						radius: ['50%', '80%'],
						color: ['#ba69aa', '#05b085', '#69cce0', '#ef483e'],
						label: {
							normal: {
								formatter: '{b}\n{d}%'
							},
					  
						},
						data:[
							{value:20, name:'PROSES ADMINISTRATION'},
							{value:10, name:'PROJECT DEVELOP'},
							{value:5, name:'PROJECT MAINTENACE'},
							{value:30, name:'PROJECT SELESAI'},
						]
					}
				]
			};
			eChart_3.setOption(option3);
			eChart_3.resize();
		};

	</script>
	<script type="text/javascript">
		jQuery(document).ready(function($){
        $('.check-sts').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah yakin Ubah Status ?",
            text: "Status akan otomatis berubah",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "cancel",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
	</script>
</body>
</html>
