<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getProjectLeader();
$gr  = $log->getWunderlist($_GET["view"]);

if(isset($_POST["simpan_value"])) :
  $wunder["proId"]    = $_POST["proId"];
  $wunder["nama"]     = $_POST["nama"];
  $wunder["expired"]  = $_POST["expired"];
  $wunder["deskripsi"]= $_POST["deskripsi"];
  $cb = $log->addWunderlist($wunder);
endif;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
  
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">

  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
  <div class="content-header">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title">Userlist grid</h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item" aria-current="page">Contact</li>
              <li class="breadcrumb-item active" aria-current="page">Userlist grid</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>  

    <!-- Main content -->
    <section class="content">
    <button data-toggle="modal" data-target="#modal-fill" class="btn btn-dark btn-sm"><span class="mdi mdi-plus"></span> Tambah Wunderlist</button><br><br>
    <div class="row">
      <?php foreach($gr as $bl) : ?>
      <div class="col-12 col-lg-4">
        <div class="box">
          <div class="box-header no-border p-0">        
            <a href="#">
              <!-- <img class="img-fluid" src="../../assets/images/avatar/375x200/1.jpg" alt=""> -->
            </a>
          </div>
           <div class="box-body">
              <div class="text-center">
                <div class="user-contact flexbox">
                  <a href="#" class="btn btn-icon-circle btn-shadow"></a>
                  <a href="#" data-toggle="modal" data-target="#modal-fill<?= $bl["TW_NO"]; ?>" class="btn btn-icon-circle btn-shadow"><span class="mdi mdi-pencil"></span> Edit</a> 
                  <div class="modal modal-fill fade" data-backdrop="false" id="modal-fill<?= $bl["TW_NO"]; ?>" tabindex="-1">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <?php
                        $kbr = $log->editWunderlist($bl["TW_NO"]);
                        if(isset($_POST["update_value"])) :
                          $list["proId1"] = $_POST["proId1"];
                          $list["nama1"] = $_POST["nama1"];
                          $list["deskripsi"] = $_POST["deskripsi"];
                          $list["expired1"] = $_POST["expired1"];
                          $list["due"]   = $_POST["due"];
                          $vn  = $log->updateWunderlist($list);
                        endif;
                        ?>
                        <form action="" method="post">
                          <div class="modal-header">
                            <h4 class="modal-title" id="result-popup">Edit Data Wunderlist</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="proId1" value="<?= $bl["TW_NO"]; ?>">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Nama</label>
                                <input type="text" required class="form-control" name="nama1" value="<?= $kbr["TW_NAMA"]; ?>" placeholder="" required>
                              </div>
                              <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Deskripsi</label>
                                <textarea name="deskripsi1" id="" cols="30" rows="10" class="form-control"><?= $kbr["TW_DESKRIPSI"]; ?></textarea>
                              </div>
                              <div class="form-group col-md-12">
                                <label for="exampleInputEmail1">Wunderlist Due</label>
                                <input type="text" class="form-control" name="expired1" id="datepicker" data-date-format="dd-mm-yyyy">
                                <input type="hidden" name="due" value="<?= $bl["TW_DUE"]; ?>">
                              </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-info" type="submit" name="update_value">Update</button>
                            <a class="btn btn-danger pull-right text-white" data-dismiss="modal">Close</a>
                          </div>
                        </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>       
                </div>
                <h3 class="my-5"><a href="#"><?= $bl["TW_NAMA"]; ?></a></h3>
                <h6 class="user-info mt-0 mb-5 text-lighter"><?= $bl["TW_DESKRIPSI"]; ?></h6>
                <div class="gap-items user-social font-size-16 p-15">
                 <h6><?= $log->TanggalIndo($bl["TW_DUE"]); ?></h6>
                </div>
                <div class="text-uppercase text-fade">
                  <?php if($bl["TW_STATUS"] === "PROCESS") : ?>
                    <a href="change-log?view=<?= $bl["TW_NO"] ?>&key=<?= $bl["TW_PROJECTID"]; ?>" class="btn btn-primary btn-sm"><span class="mdi mdi-history"></span> Changelog</a>
                    <a href="status/index?key=<?= $bl["TW_NO"]; ?>&view=<?= $bl["TW_PROJECTID"]; ?>&status=COMPLETE" class="btn btn-warning btn-sm ubah-status">Process</a>
                  <?php else : ?>
                    <a href="#" class="btn btn-danger btn-sm changelog"><span class="mdi mdi-history"></span> Changelog</a>
                    <a href="status/index?key=<?= $bl["TW_NO"]; ?>&view=<?= $bl["TW_PROJECTID"]; ?>&status=PROCESS" id="" class="btn btn-danger btn-sm ubah-status">Complete</a>
                  <?php endif; ?>
                </div>
              </div>
           </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
    
    </section>
    <!-- /.content -->
  </div>
  <div class="modal modal-fill fade" data-backdrop="false" id="modal-fill" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form action="" method="post">
            <div class="modal-header">
              <h4 class="modal-title" id="result-popup">Tambah Data Wunderlist</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="proId" value="<?= $_GET["view"]; ?>">
              <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Nama</label>
                  <input type="text" required class="form-control" name="nama" placeholder="" required>
                </div>
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Deskripsi</label>
                  <textarea name="deskripsi" id="" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Wunderlist Due</label>
                  <input type="text" class="form-control" name="expired" id="datepicker" data-date-format="dd-mm-yyyy">
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-info" type="submit" name="simpan_value">Simpan</button>
              <a class="btn btn-danger pull-right text-white" data-dismiss="modal">Close</a>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.content-wrapper -->
 
   <?php include_once('../../layouts/footer.php'); ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  

  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- SlimScroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript">
    $(function () {
      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    });
  </script>
  <script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-status').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin ubah status?",
            text: "Data akan berubah otomatis",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });

    jQuery(document).ready(function($){
        $('.changelog').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Hai, <?= $_SESSION["U_FULLNAME"]; ?> :)",
            text: "Ubah Status terlebih dahulu untuk menggunakan fitur ini :)",
            type: "info",
            animation: true,
            customClass: 'animated tada',
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Paham",
            // cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false,
          });
        });
    });
  </script>
</body>
</html>
