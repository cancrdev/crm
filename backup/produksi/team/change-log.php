<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getWunderlistDetail($_GET["view"], $_GET["key"]);
$bl = $log->getDetailProject($_GET["key"]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
  
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="../../assets/css/master_style.css">

	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">	
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  <style type="text/css">
    label {
      font-size: 1.5em;
    }

    input[type=checkbox]:checked + label {
      color:red;
      text-decoration: line-through;
    }
  </style>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  
  
  <!-- Left side column. contains the logo and sidebar -->
   <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">Project List Log <?= $bl["TP_NAMA_PROJECT"]; ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Home</li>
							<li class="breadcrumb-item active" aria-current="page">Project List Log</li>
              <li class="breadcrumb-item active" aria-current="page"><button class="btn btn-dark btn-sm" data-toggle="modal" data-target="#modal-center"><span class="mdi mdi-plus"></span> Tambah List</button></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>  

    <!-- Main content -->
    <section class="content">
	  
	  <div class="row">
		<div class="col-md-11 connectedSortable">
		  <!-- Default box -->
		  <div class="box box-solid box-primary">
			<div class="box-header with-border">
			  <h4 class="box-title">List Wunderlist Project</h4>

			  <ul class="box-controls pull-right">
				<li><a class="box-btn-close" href="#"></a></li>
				<li><a class="box-btn-slide" href="#"></a></li>	
				<li><a class="box-btn-fullscreen" href="#"></a></li>
			  </ul>
			</div>
			<div class="box-body p-10">
			  <ul class="todo-list">
          <!-- foreach -->
          <?php $no = 1; $no1 = 1; foreach($ctrl as $cc) : ?>
          <!-- if -->
          <?php if(is_array($cc) == "" || is_array($cc) == NULL) : ?>
  				<li class="b-1 p-0 mb-15">
             <div class="position-relative p-20">
                <p>Data Kosong</p>
            </div>
          </li>
          <?php else :  ?>
          <li class="b-1 p-0 mb-15">
            <div class="position-relative p-20">
              <!-- drag handle -->
              <div class="handle handle2"></div>
              <!-- checkbox -->
                <?php if($cc["TWD_CHECK"] == '0') : ?>
                <input type="checkbox" id="basic_checkbox_<?= $no++; ?>" class="filled-in">
                <label for="basic_checkbox_<?= $no1++; ?>" class="mb-0 h-15 ml-15"></label>
                <!-- todo text -->
                <span class="text-line font-size-18"><?= $cc["TWD_NAMA"]; ?></span>
                <?php else : ?>
                <!-- todo text -->
                <input type="checkbox" checked disabled class="filled-in">
                <!-- <label for="basic_checkbox_19" class="mb-0 h-15 ml-15"></label> -->
                <span class="text-line font-size-18"><strike><?= $cc["TWD_NAMA"]; ?></strike></span>
                <?php endif; ?>
                <!-- General tools such as edit or delete-->
                <div class="pull-right d-block text-dark flexbox">
                <!-- <a href="#" data-toggle="modal" data-target="#modal-fill<?= $cc["TWD_WUNDERLISTNO"]; ?>"><i class="fa fa-edit"></i></a> -->
                
                <a href="delete-wunderlist?id=<?= $cc["TWD_BIGID"]; ?>&view=<?= $_GET['view'] ?>&key=<?= $_GET['key']; ?>" class="ubah-status" data-toggle="tooltip" data-container="body" title="" data-original-title="Remove"><i class="fa fa-trash-o"></i></a>
                </div>
                <!-- <div class="mt-5 ml-50 pl-5">Sed ut perspiciatis unde omnis iste natus error sit.</div> -->
                <?php $date = date("Y-m-d"); if($cc["TWD_DUE"] == $date && $cc["TWD_CHECK"] == '0') : ?>
                <script type="text/javascript">swal("Oops, <?= $_SESSION["U_FULLNAME"]; ?>", "Due List hari ini :)", "warning");</script>
                <div class="mt-5 ml-50 pl-5" style="color: yellow;">Due : <em><?= $log->TanggalIndo($cc["TWD_DUE"]); ?></em><br><p style="color: yellow;">(*) Due List hari ini</p></div>
                <?php elseif($cc["TWD_DUE"] < $date && $cc["TWD_CHECK"] == '0') : ?>
                <script type="text/javascript">swal("Oops, <?= $_SESSION["U_FULLNAME"]; ?>", "Over Due sudah berjalan :(", "warning");</script>
                <div class="mt-5 ml-50 pl-5" style="color: red;">Over Due :<em><?= $log->TanggalIndo($cc["TWD_DUE"]); ?></em><br><p style="color: red;">(*) Over Due sudah berjalan</p></div>
                <?php else : ?>
                <div class="mt-5 ml-50 pl-5" style="color: green;">Due : <em><?= $log->TanggalIndo($cc["TWD_DUE"]); ?></em></div>
                <?php endif ?>
                <?php if($cc["TWD_CHECK"] == '0') : 
                if(isset($_POST["checkbox_save"])) :
                  $cek["wId"] = $_POST["wId"];
                  $mn = $log->ubahStatusCheckin($cek);
                endif;
                ?>
                <form action="" method="post" class="text-center">
                  <input type="hidden" name="wId" value="<?= $cc["TWD_BIGID"]; ?>">
                  <button name="checkbox_save" class="btn btn-success btn-sm"><span class="mdi mdi-check"></span></button>
                </form>
              <?php endif; ?>
            </div>
          </li>
          <?php endif; ?>
          <!-- endif -->
          <?php endforeach; ?>
          <!-- end foreach -->
			  </ul>
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->
		</div>
	  </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
   <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  <li class="nav-item">
			<a class="nav-link" href="javascript:void(0)">FAQ</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Purchase Now</a>
		  </li>
		</ul>
    </div>
	  &copy; 2018 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
  </footer>
  <!-- Control Sidebar -->
  <div class="modal center-modal fade" id="modal-center" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <?php 
          if(isset($_POST["simpan_value"])) : 
            $wunder["no"]   = $_POST["no"];
            $wunder["nama"] = $_POST["nama"];
            $wunder["expired"] = $_POST["expired"];
            $cbn = $log->addDetailWunderlist($wunder);
          endif;
          ?>
          <form action="" method="post">
            <div class="modal-header">
              <h4 class="modal-title" id="result-popup">Tambah Data Wunderlist</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
              <input type="hidden" name="no" value="<?= $_GET["view"]; ?>">
              <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Nama List</label>
                  <input type="text" required class="form-control" name="nama" placeholder="" required>
                </div>
                <!-- <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Deskripsi</label>
                  <textarea name="deskripsi" id="" cols="30" rows="10" class="form-control"></textarea>
                </div> -->
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Wunderlist Due</label>
                  <input type="text" class="form-control" name="expired" id="datepicker" data-date-format="dd-mm-yyyy">
                </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-info" type="submit" name="simpan_value">Simpan</button>
              <a class="btn btn-danger pull-right text-white" data-dismiss="modal">Close</a>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


	<!-- jQuery 3 -->
	<script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="../../assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
	<!-- SlimScroll -->
	<script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	
	<!-- FastClick -->
	<script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- Superieur Admin App -->
	<script src="../../assets/js/template.js"></script>
	
	<!-- Superieur Admin for demo purposes -->
	<script src="../../assets/js/demo.js"></script>
	
	<script src="../../assets/js/pages/extra_taskboard.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
   $(function () {
      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    }); 

    jQuery(document).ready(function($){
        $('.ubah-status').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin hapus data?",
            text: "Data akan berubah otomatis",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
  </script>

</body>
</html>
