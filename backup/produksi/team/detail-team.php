<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getDetailProject($_GET["view"]);
$team = $log->getTeamProject($_GET["view"], $ctrl["TP_LEADER"]);
$ld   = $log->getNamaLead($ctrl["TP_LEADER"]);
$usr  = $log->getUserTeam();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Data List Project Per Leader</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Project No. <?= $ctrl["TP_NO_PROJECT"]." | ".$ctrl["TP_NAMA_PROJECT"]; ?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row"> 
         <div class="box">          
            <div class="panel panel-default">                                
                <div class="panel-body">
                    <h3>Data Detail Project</h3>
                    <table width="100%" >
                    <tr >
                    <td>No Project </td><td> :</td>
                    <td><?= $ctrl["TP_NO_PROJECT"]; ?></td> 
                    </tr>
                    <tr>
                    <td>Nama Project</td><td> :</td>
                    <td><?= $ctrl["TP_NAMA_PROJECT"]; ?></td> 
                    </tr>
                    <tr>
                    <td>Deskripsi Project</td><td> :</td>
                    <td><?= $ctrl["TP_DESKRIPSI"]; ?></td> 
                    </tr>
                    <tr>
                    <td>Platform</td><td> :</td>
                    <td><?= $ctrl["TP_PLATFORM"]; ?></td> 
                    </tr>
                    <tr>
                    <td>Project Masuk</td><td> :</td>
                    <td><?= $log->TanggalIndo($ctrl["TP_PROJECT_TGL"]); ?></td> 
                    </tr>
                    <tr>
                    <td>Project Due</td><td> :</td>
                    <td><b><?= $log->TanggalIndo($ctrl["TP_PROJECT_DUE"]); ?></b></td> 
                    </tr>
                     <tr>
                    <td>Lama Pengerjaan Project</td><td> :</td>
                    <td>
                      <?php
                      //jumlah hari
                     $start_date = new DateTime($ctrl["TP_PROJECT_TGL"]);
                     $end_date = new DateTime($ctrl["TP_PROJECT_DUE"]);
                     $interval = $start_date->diff($end_date);
                     echo "$interval->days hari / ".$log->diffInMonths($start_date, $end_date)." Bulan";
                       ?>
                    </td> 
                    </tr>
                    <tr>
                    <td>Waktu Produksi</td><td> :</td>
                    <?php 
                    //lama
                    $booking    =new DateTime($ctrl['TP_PROJECT_TGL']);
                    $today        =new DateTime();
                    $diff = $today->diff($booking);
                    $process = $diff->d;
                      ?>
                    <td><?= $process = $diff->d; echo " Hari"; ?></td> 
                    </tr>
                    <tr>
                    <td>Nama Leader</td><td> :</td>
                    <td><?= $ld["U_FULLNAME"]; ?></td> 
                    </tr>
                    <tr>
                    <td>Project Detail</td><td> :</td>
                    <td>
                      <?php if($ctrl["TP_PROJECT_DETAIL"] === NULL) { ?>
                      <!-- <a href="#" class="btn btn-danger btn-sm" id="data-kosong"><span class="mdi mdi-close-circle-outline"></span> Project Detail Kosong</a> -->
                      <a type="button" class="btn btn-danger btn-sm text-white" id="kosong"><span class="mdi mdi-close-circle-outline"></span> Project Detail Kosong</a>
                      <?php } else { ?>
                      <a href="../../assets/images/avatar/file/<?= $ctrl["TP_PROJECT_DETAIL"]; ?>" class="btn btn-info btn-sm" target="_blank"><i class="mdi mdi-download"></i> Lihat Project Detail</a>
                      <?php } ?>
                    </td> 
                    </tr>
                    </table>
                </div>
            </div>
            
        </div>       
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">List Team Project Per Leader</h3>
            <br><br>
            <?php if($ctrl["TP_LEADER"] === $_SESSION["U_REGID"]) { ?>
            <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-info mb-5 btn-sm"><i class="mdi mdi-account-multiple-plus"></i> Tambah Team</a> 
            <?php } ?>
          </div>
              <!-- /.box-header -->
          <div class="box-body">
              <div class="table-responsive">
                   <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Regist</th>
                        <th>Nama Project</th>
                        <th>Email</th>
                        <th>Jabatan</th>
                        <th>Photo Profile</th>
                        <th>Keterangan</th>
                        <td>Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($team)) {
                        $no = 1;
                        foreach($team as $cc) {
                      ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $cc["U_REGID"]; ?></td>
                        <td><?= $cc["U_FULLNAME"]; ?></td>
                        <td><?= $cc["U_EMAIL"] ?></td>
                        <td><?= $cc["U_POSISI_JABATAN"]; ?></td>
                        <td><a target="_blank" href="../../assets/images/avatar/profile/<?= $cc["U_AVATAR"]; ?>" class="btn btn-success btn-sm"><i class="mdi mdi-account-circle"></i></a></td>
                        <td><?= $cc["TPD_KETERANGAN"]; ?></td>
                        <td>
                          <?php if($cc["TPD_STATUS"] === "DISABLED") { ?>
                          <a href="status/index?key=<?= $cc["TPD_BIGID"]; ?>&status=AKTIF" class="btn btn-warning mb-5 btn-sm sts-aktif"><span class="mdi mdi-airplay"></span>Disabled</a>
                          <?php } else { ?>
                          <a href="status/index?key=<?= $cc["TPD_BIGID"]; ?>&status=DISABLED" class="btn btn-success mb-5 btn-sm sts-disabled"><span class="mdi mdi-airplay"></span>Aktif</a>
                          <?php } ?>
                          <a href="contribution?view=<?= $cc["U_REGID"] ?>" class="btn btn-dark btn-sm"><i class="mdi mdi-message-settings-variant"></i> Kontribusi</a>
                        </td>
                      </tr>
                      <?php } } ?>
                    </tbody>    
                  </table>
              </div>              
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->          
      </div>
      </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <a type="button" class="close" data-dismiss="modal">&times;</a>
          <h4 class="modal-title">Tambah Team Produksi</h4>
        </div>
        <?php 
        if(isset($_POST["save"])) {
           $team["nama"]        = $_POST["nama"];
           $team["keterangan"]  = $_POST["keterangan"];
           $team["projectId"]   = $_POST["projectId"];
           $team["leadId"]      = $_POST["leadId"];
           $vl = $log->tambahTeam($team);
        }
        ?>
        <form method="post">
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Nama Produksi</label>
            <select class="form-control" name="nama" required>
              <?php if(count($usr) < 1) { ?>
              <option disabled>-- Belum ada data --</option>
              <?php } else {
              if(is_array($usr)) {
                foreach($usr as $us) {
               ?>
              <option value="<?= $us["U_REGID"]; ?>"><?= $us["U_FULLNAME"]; ?></option>
              <?php } } } ?>
            </select>
          </div>
          <input type="hidden" name="projectId" value="<?= $ctrl["TP_REG_PROJECT"]; ?>">
          <input type="hidden" name="leadId" value="<?= $ctrl["TP_LEADER"]; ?>">
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Keterangan</label>
            <textarea class="form-control" name="keterangan"></textarea>
          </div>
        
          <div class="modal-footer">
            <a class="btn btn-default" data-dismiss="modal">Close</a>
            <input type="submit" name="save" class="btn btn-info" placeholder="Simpan">
            <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <script src="../../assets/js/pages/dashboard6.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
   
   <script type="text/javascript">
     $('#kosong').click(function(){
      swal({
       title: "Project Detail Kosong",
       text:  "Data tidak tersedia :(",
       type: "error",
       showCancelButton: true,
       confirmButtonClass: 'btn-danger',
       confirmButtonText: 'OK',
       cancelButtonText: "cancel",
       closeOnConfirm: false,
       closeOnCancel: false
      },
      function(isConfirm) {
       if(isConfirm){
            swal("Data Kosong", "Coba Lain kali :)", "success");
      } else {
          swal("Cancelled", "Coba lain kali :)", "error");
        }
      })
        
      });
   </script>
  
</body>
</html>
