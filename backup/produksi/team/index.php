<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getProjectLeader();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Data List Project Per Leader</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Project Per Leader</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">        
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">List Project Per Leader</h3>
            <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
            <br>
            <!-- <a href="" class="btn btn-info mb-5"><i class="mdi mdi-account-multiple-plus"></i></a> -->
          </div>
              <!-- /.box-header -->
          <div class="box-body">
              <div class="table-responsive">
                   <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Project</th>
                        <th>Nama Project</th>
                        <!-- <th>Deskripsi</th> -->
                        <th>Platform</th>
                        <th>Project Masuk</th>
                        <td style="text-align: center;">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($ctrl)) {
                        $no = 1;
                        foreach($ctrl as $cc) {
                      ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $cc["TP_NO_PROJECT"]; ?></td>
                        <td><?= $cc["TP_NAMA_PROJECT"]; ?></td>
                       <!--  <td>
                          <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-left"><i class="mdi mdi-magnify-plus"></i> Deskripsi</a>
                          
                        </td> -->
                        <td><?= $cc["TP_PLATFORM"]; ?></td>
                        <td><?= $log->TanggalIndo($cc["TP_PROJECT_TGL"]); ?></td>
                        <td>
                          <a href="detail-team?view=<?= $cc["TP_REG_PROJECT"]; ?>" class="btn btn-dark mb-5 btn-sm"><span class="mdi mdi-airplay"></span> Detail Team</a>
                          <a href="contribution?view=<?= $cc["TP_REG_PROJECT"]; ?>" class="btn btn-warning mb-5 btn-sm"><span class="mdi mdi-calendar-check"></span> Kontribusi</a>
                          <?php if($cc["TP_LEADER"] === $_SESSION["U_REGID"]) { if($cc["TP_STATUS"] === "PROJECT_SELESAI") { ?>
                          <a href="#" disabled class="btn btn-danger mb-5 btn-sm"><span class="mdi mdi-block-helper"></span> Project Selesai</a>
                          <?php } else { ?>
                          <a href="status/index?view=<?= $cc["TP_REG_PROJECT"]; ?>&status=PROJECT_SELESAI" class="btn btn-success mb-5 btn-sm ubah-status"><span class="mdi mdi-check-all"></span> Ubah Project Selesai</a>
                          <?php } } ?>
                        </td>
                      </tr>
                      <?php } } ?>
                    </tbody>    
                  </table>
              </div>              
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->          
      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <script src="../../assets/js/pages/dashboard6.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 

  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

  <script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-status').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Yakin mengubah status Project Selesai?",
            text: "Status akan otomatis berubah",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
  
</body>
</html>
