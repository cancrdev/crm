<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../assets/images/gallery/full/deals1.png">

    <title>Recovery Password = CRM</title>
  
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../assets/css/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">

	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">	

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body class="hold-transition bg-img" style="background-image: url(../assets/images/gallery/full/gampang.jpg)" data-overlay="4">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">
			<div class="col-12">
				<div class="row no-gutters justify-content-md-center">
					<div class="col-lg-4 col-md-5 col-12">
						<div class="content-top-agile h-p100">
							<h2>Recover Password</h2><br>
							<h2><a href="../login/" class="btn btn-info"><i class="ti-control-skip-backward"></i> Back</a></h2>							
						</div>				
					</div>
					<div class="col-lg-5 col-md-5 col-12">
						<div class="p-40 bg-white content-bottom">
							<?php 
							  include_once('../stucture/fungsi.php');
							  $log = new Model();

							  if(isset($_POST['g-recaptcha-response']) && isset($_POST['reset'])) {
							    $captcha = $_POST['g-recaptcha-response'];
							    if (!$captcha) { ?>
							      <script type="text/javascript">swal("Oops...", "Tolong cek akses anda dengan reCAPTCHA box. :)", "error");</script>
							   <?php } else {
							      $secret = '6LdtC30UAAAAANH7NN498SRcebME2O0cjtUs_WbD';
							      $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
							      if ($response != true) { ?>
							        <script type="text/javascript">swal("Oops...", "lengkapi parameter anda :(", "error");</script>
							     <?php } else {
							         $recover["email"] = $_POST["email"];
							         $ctrl = $log->recover1($recover);
							      }
							    }
							  } ?>
							<form action="" method="post" class="form-element">
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text bg-info border-info"><i class="ti-email"></i></span>
										</div>
										<input type="email" name="email" class="form-control pl-15" required placeholder="Masukkan Email anda">
									</div><br>
									<div class="input-group mb-3">
										 <div class="g-recaptcha" data-sitekey="6LdtC30UAAAAAPaiKkh3G8bETa4Xi9wTL2AqPoTT"></div>
									</div>
								</div>
								<!-- <div class="col-md-12 form-group">
			                      
			                    </div> -->
								  <div class="row">
									<div class="col-12 text-center">
									  <button type="submit" name="reset" class="btn btn-info btn-block margin-top-10">Reset</button>
									</div>
									<!-- /.col -->
								  </div>
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	



	<!-- jQuery 3 -->
	<script src="../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>

	<script src='https://www.google.com/recaptcha/api.js'></script>
	
	<!-- popper -->
	<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>

</body>
</html>
