<?php
include_once('../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getData($_GET["email"]);

if(isset($_POST["recov"])) {
  $ur["password"] = md5($_POST["password"]);
  $ur["konfirm"]  = md5($_POST["konfirm"]);
  $ur["email"]    = $_POST["email"];
  $bn =	$log->updateRecovery($ur);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../assets/images/gallery/full/deals1.png">

    <title>CRM - Ubah Password</title>
  
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../assets/css/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet" href="../assets/css/master_style.css">

	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../assets/css/skins/_all-skins.css">	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="hold-transition bg-img" style="background-image: url(../assets/images/gallery/full/anyar.jpg); background-size: 1500px 900px;background-repeat: no-repeat; height: 80%" data-overlay="4">
	
	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">
			<div class="col-12">
				<div class="row no-gutters justify-content-md-center">
					<div class="col-lg-4 col-md-5 col-12">
						<div class="content-top-agile h-p100">
							<img src="../assets/images/avatar/profile/<?= $ctrl["U_AVATAR"] ?>" alt="User Image" class="rounded-circle b-1">
							<h3 class="text-white mb-0"><?= $ctrl["U_FULLNAME"]; ?></h3>							
						</div>				
					</div>
					<div class="col-lg-5 col-md-5 col-12">
						<div class="p-40 bg-white content-bottom">

							<form  method="post" class="form-element">
								<input type="hidden" name="email" value="<?= $_GET["email"]; ?>">
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text bg-info border-info"><i class="ti-lock"></i></span>
										</div>
										<input type="password" name="password" class="form-control pl-15" placeholder="Password Baru">
									</div>
								</div>
								<div class="form-group">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text bg-info border-info"><i class="ti-lock"></i></span>
										</div>
										<input type="password" name="konfirm" class="form-control pl-15" placeholder="Kofirmasi Password Baru">
									</div>
								</div>
								  <div class="row">
									<div class="col-12 text-center">
									  <button type="submit" name="recov" class="btn btn-info btn-block margin-top-10">Ubah Password</button>
									</div>
									<!-- /.col -->
								  </div>
							</form>			

							<div class="text-center">
							  <p class="mt-20">- OR -</p>
							  <p class="mb-5">
								  Enter your password to retrieve your session
								</p>	
							</div>

							<div class="text-center">
								<p class="mt-15 mb-0">Or <a href="../login/" class="text-warning"><b>Sign In</b></a> as a Different User</p>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>	


	<!-- jQuery 3 -->
	<script src="../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
	
	
</body>
</html>
