<?php
/**
 * The font metrics class
 *
 * Global function system app CRM
 * 
 * powered by candev-hadi
 * 
 **/

// header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
// header('Cache-Control: no-store, no-cache, must-revalidate');
// header('Cache-Control: post-check=0, pre-check=0', FALSE);
// header('Pragma: no-cache');
// header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
/** GLOBAL FUNCTION **/
require_once 'config.php';
require_once 'security.php';

class model extends Security {
	function __construct() {
		date_default_timezone_set("Asia/Jakarta");
    }

    //fungsi global
    function TanggalIndo($date){
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
	 
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
		return($result);
	}

    function diffInMonths(\DateTime $date1, \DateTime $date2)
    {
        $diff =  $date1->diff($date2);

        $months = $diff->y * 12 + $diff->m + $diff->d / 30;

        return (int) round($months);
    }

	function frmDate($date,$code){
        $explode = explode("-",$date);
        $year  = $explode[0];
        $month = (substr($explode[1],0,1)=="0")?str_replace("0","",$explode[1]):$explode[1];
        $dated = $explode[2];
        $explode_time = explode(" ",$dated);
        $dates = $explode_time[0];
        switch($code){
            // Per Object
            case 4: $format = $dates; break;                                                   
            case 5: $format = $month; break;                                                       
            case 6: $format = $year; break;               
        }       
        return $format; 
    }


	function dateRange($start,$end){
        $xdate    = $start;
        $ydate    = $end;
        $xmonth    = $start;
        $ymonth    = $end;
        $xyear    = $start;
        $yyear    = $end;
        // Jika Input tanggal berada ditahun yang sama
        if($xyear==$yyear){
            // Jika Input tanggal berada dibulan yang sama
            if($xmonth==$ymonth){
                $nday=$ydate+1-$xdate;
            } else {
                $r2=NULL;
                $nmonth = $ymonth-$xmonth;           
                $r1 = nmonth($xmonth)-$xdate+1;
                for($i=$xmonth+1;$i<$ymonth;$i++){
                    $r2 = $r2+nmonth($i);
                }
                $r3 = $ydate;
                $nday = $r1+$r2+$r3;
            }
        } else {
            // Jika Input tahun awal berbeda dengan tahun akhir
            $r2=NULL; $r3=NULL;
            $r1=nmonth($xmonth)-$xdate+1;

            for($i=$xmonth+1;$i<13;$i++){
                $r2 = $r2+nmonth($i);
            }
            for($i=1;$i<$ymonth;$i++){
                $r3 = $r3+nmonth($i);
            }
            $r4 = $ydate;
            $nday = $r1+$r2+$r3+$r4;
        }           
        return $nday;
    }

    function mobileLogin($login) {
    	$mail 		= mysqli_escape_string($this->conn(), $login["email"]);
    	$pass 		= md5($login["loginPassword"]);
    	$password 	= $this->clean_all($pass);
    	$ip = $_SERVER['REMOTE_ADDR'];
		$browser = $_SERVER['HTTP_USER_AGENT'];

    	$query = $this->query("SELECT * FROM tc_user WHERE (U_EMAIL = '$mail' OR U_TELPON = '$mail') AND U_PASSWORD_HASH = '$password'");

	    if (mysqli_num_rows($query) > 0){ 
	    	//session_start(); 
	        //$users = mysqli_fetch_assoc($this->conn(), $user); 
	        $users = $query->fetch_assoc();
	        $userId = $users["U_EMAIL"]; 
	        //generate login token 
	        $dateNow = date("Y-m-d H:i:s"); 
	        $loginToken = substr(md5($users["U_FULLNAME"].$dateNow), 0, 30); 
	        $updateData = $this->query("UPDATE tc_user SET U_TOKEN = '$loginToken', U_LOGIN_WAKTU = '$dateNow', U_IP_POSITION = '$ip', U_DEFAULT_BROWSER = '$browser' WHERE U_EMAIL = '$userId'"); 

	        $_SESSION["U_REGID"] 		  = $users["U_REGID"];
	        $_SESSION["U_NAME"]  		  = $users["U_NAME"];
	        $_SESSION["U_FULLNAME"] 	  = $users["U_FULLNAME"];
	        $_SESSION["U_EMAIL"]		  = $users["U_EMAIL"];
	        $_SESSION["U_GROUP_RULE"]	  = $users["U_GROUP_RULE"];
	        $_SESSION["U_STATUS"]		  = $users["U_STATUS"];
	        $_SESSION["U_CLIENTID"] 	  = $users["U_CLIENTID"];
            $_SESSION["U_POSISI_JABATAN"] = $users["U_POSISI_JABATAN"];
            $_SESSION["U_ALAMAT"]         = $users["U_ALAMAT"];
            $_SESSION["U_TELPON"]         = $users["U_TELPON"]; 
            $_SESSION["U_AVATAR"]         = $users["U_AVATAR"];

            if($users["U_STATUS"] === "USER_ACTIVE") {
    	        if($users["U_GROUP_RULE"] === "TO_CEO") {
                    echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai CEO') 
                    location.replace('../ceo/home')</script>";
    	        } elseif($users["U_GROUP_RULE"] === "TO_ACCOUNT") {
                   echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Divisi Account') 
                    location.replace('../account/home')</script>";
    	        } elseif($users["U_GROUP_RULE"] === "TO_CLIENT") {
                    if($users["U_ACTIVE_CLIENT"] > 0) {
                        echo "<script>alert('Selamat datang ".$users["U_FULLNAME"]."') 
                        location.replace('../client/home/')</script>";
                    } else {
                        echo "<script>alert('Selamat datang ".$users["U_FULLNAME"]."') 
                        location.replace('../client/home/lock-screen')</script>";
                    }
    	        } elseif($users["U_GROUP_RULE"] === "TO_PRODUKSI") {
                    echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Divisi Produksi') 
                    location.replace('../produksi/home')</script>";
    	        } else {
    	        	echo "<script>alert('Mohon maaf anda tidak memiliki akses')</script>"; 
    	        }
            } else {
                echo "<script>alert('Status anda tidak aktif,silahkan hubungi administrator untuk info lebih lanjut')</script>"; 
            }
	    } else {
	    	echo "<script>alert('Periksa lagi data login anda')</script>"; 
	    }

    }

    function getLock($id) {
        $query = $this->query("SELECT * FROM tc_user WHERE U_CLIENTID = '$id'");
        $row   = $query->fetch_assoc();

        return $row;
    }

    function lockProfile($lock) {
        $id  = $_SESSION["U_REGID"];
        $password = $this->clean_all($lock["password"]);
        $confirm  = $this->clean_all($lock["confirm"]);

        if($password == $confirm) {
            $dd = $this->query("UPDATE tc_user SET U_PASSWORD_HASH = '$confirm', U_ACTIVE_CLIENT = '1' WHERE U_REGID = '$id'");
            if($dd) {
                echo "<script>alert('Selamat,akun anda sudah aktif') 
                        location.replace('../home/')</script>";
            } else {
                echo "<script>alert('Lock Profile gagal')</script>";
            }
        } else {
            echo "<script>alert('Password yang anda masukkan tidak sama')</script>"; 
        }
    }

    function mobileLogout($logout) {
    	session_start(); 
		$dateOut = date("Y-m-d H:i:s"); 
		$email = $_SESSION["U_EMAIL"]; 
		$name = $_SESSION["U_FULLNAME"];
		$logout = $this->query("UPDATE tc_user SET U_LOGOUT_WAKTU = '$dateOut' WHERE U_EMAIL = '$email'"); 
		session_destroy(); 
		setcookie('notifLogin','Berhasil Logout',time() + 10); 
		if($logout) {
			echo "<script>alert('Anda berhasil logout ".$name."') 
			location.replace('v1/login')</script>";
		}
    }

    function getProduksi(){
        $rows = array();
        $query = $this->query("SELECT * FROM tc_user WHERE U_GROUP_RULE = 'TO_PRODUKSI' ORDER BY U_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getAccount(){
        $rows = array();
        $query = $this->query("SELECT * FROM tc_user WHERE U_GROUP_RULE = 'TO_ACCOUNT' ORDER BY U_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getClient() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_client ORDER BY TC_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;   
    }
    
    function detailClient($id) {
        $query  = $this->query("SELECT * FROM tc_client WHERE TC_REGID = '$id'");
        $row    = $query->fetch_assoc();
        return $row;
    }

    //== PROJECT ==//
    function getAllProject() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function getProjectMasuk() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'ORDER_MASUK' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getDetailFile($id) {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_order AS A INNER JOIN tc_project AS B ON A.TP_PROJECTID = B.TP_REG_PROJECT WHERE A.TP_PROJECTID = '$id' ORDER BY A.TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getDetailPenawaran($id) {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_penawaran AS A INNER JOIN tc_project AS B ON A.TPN_PROJECTID = B.TP_REG_PROJECT WHERE A.TPN_PROJECTID = '$id' GROUP BY A.TPN_NO_PENAWARAN");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function detailBerkasPenawaran($id) {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_penawaran AS A INNER JOIN tc_project AS B ON A.TPN_PROJECTID = B.TP_REG_PROJECT WHERE A.TPN_PROJECTID = '$id'");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function detailPenawaran($id) {
        $query = $this->query("SELECT * FROM tc_penawaran WHERE TPN_PROJECTID = '$id'");
        $row = $query->fetch_assoc();
        return $row;
    }

    function getPenawaranByKode($id) {
        $query = $this->query("SELECT * FROM tc_penawaran WHERE TPN_KODE = '$id'");
        $row = $query->fetch_assoc();
        return $row;
    }

    function getDetailProject($projectId) {
        $query = $this->query("SELECT C.*, A.*, B.* FROM tc_order AS A INNER JOIN tc_project AS B ON B.TP_REG_PROJECT = A.TP_PROJECTID INNER JOIN tc_client AS C ON A.TP_CLIENTID = C.TC_REGID WHERE B.TP_REG_PROJECT = '$projectId'");
        $row = $query->fetch_assoc();
        return $row; 
    }

    function getWaitingAnalis() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'WAITING_ANALIS' AND TP_CHECK != 'Y' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }
    
    function analisBack() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'WAITING_ANALIS' AND TP_CHECK = 'Y' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }


    function getWaitingCEO() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'PROJECT_ANALIS_ACC' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function getFollowup() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'PROJECT_FOLLOWUP' ORDER BY TP_CREATED_AT DESC");
        while($row = mysqli_fetch_assoc($query)) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function cekFollowup() {
         $query = $this->query("SELECT COUNT(*) AS jml FROM tc_project WHERE TP_STATUS = 'PROJECT_FOLLOWUP'");
         $row   = $query->fetch_assoc();
         return $row;
    }

    function getHotProspek() {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_prospek_project AS A INNER JOIN tc_project AS B ON A.TPR_PROJECTID = B.TP_REG_PROJECT ORDER BY A.TPR_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function getProjectReject() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'PROJECT_REJECT' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function getDealProject() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS = 'PROJECT_DEAL' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function tambahProduksi($produksi) {
        //get nomer urut
        $coll = $this->query("SELECT COUNT(U_NAME) AS jml FROM tc_user WHERE U_GROUP_RULE = 'TO_PRODUKSI'");
        $row = $coll->fetch_assoc();
        $kode = $row["jml"];
        $tmbhKode = intval($kode)+1;
        $regId = $produksi["username"]."0".$kode;
        $username = $this->clean_post($produksi["username"]);
        $fullname = $this->clean_post($produksi["fullname"]);
        $email = $this->clean_post($produksi["email"]);
        $telp = $this->clean_post($produksi["noTelpon"]);
        $alamat = $this->clean_post($produksi["alamat"]);
        $posisi = $this->clean_post($produksi["posisi"]);
        $password = md5(12345678);

        //get data email atau username
        $user = $this->query("SELECT * FROM tc_user WHERE U_EMAIL = '$email' OR U_NAME = '$username' OR U_TELPON = '$telp'");

        if(mysqli_num_rows($user) > 0) {
            echo "<script>alert('Username/Email sudah ada')</script>";
        } else {
            $query = $this->query("INSERT INTO tc_user (U_REGID, U_NAME, U_FULLNAME, U_EMAIL, U_GROUP_RULE, U_STATUS, U_PASSWORD_HASH, U_TELPON, U_ALAMAT, U_POSISI_JABATAN) VALUES ('$regId', '$username', '$fullname', '$email', 'TO_PRODUKSI', 'USER_ACTIVE', '$password', '$telp', '$alamat', '$posisi')");
            if($query) {
                echo "<script>alert('User Produksi berhasil ditambahkan') 
                location.replace('index')</script>";
            } else {
                echo "<script>alert('gagal ditambahkan')</script>";
            }
        }
    }

    function deleteProduksi($id) {
        $query = $this->query("DELETE FROM tc_user WHERE U_REGID = '$id'");
    }

    function detalProduksi($id) {
        $query = $this->query("SELECT * FROM tc_user WHERE U_REGID = '$id'");
        $row = $query->fetch_assoc();
        return $row;
    }

    function tambahAccount($account) {
        $coll = $this->query("SELECT COUNT(U_NAME) AS jml FROM tc_user WHERE U_GROUP_RULE = 'TO_ACCOUNT'");
        $row = $coll->fetch_assoc();
        $kode = $row["jml"];
        $tmbhKode = intval($kode)+1;
        $regId = $account["username"]."0".$kode;
        $username = $this->clean_post($account["username"]);
        $fullname = $this->clean_post($account["fullname"]);
        $email = $this->clean_post($account["email"]);
        $telp = $this->clean_post($account["noTelpon"]);
        $alamat = $this->clean_post($account["alamat"]);
        $posisi = $this->clean_post($account["posisi"]);
        $password = md5(12345678);

        //get data email atau username
        $user = $this->query("SELECT * FROM tc_user WHERE U_EMAIL = '$email' OR U_NAME = '$username' OR U_TELPON = '$telp'");

        if(mysqli_num_rows($user) > 0) {
            echo "<script>alert('Username/Email sudah ada')</script>";
        } else {
            $query = $this->query("INSERT INTO tc_user (U_REGID, U_NAME, U_FULLNAME, U_EMAIL, U_GROUP_RULE, U_STATUS, U_PASSWORD_HASH, U_TELPON, U_ALAMAT, U_POSISI_JABATAN) VALUES ('$regId', '$username', '$fullname', '$email', 'TO_ACCOUNT', 'USER_ACTIVE', '$password', '$telp', '$alamat', '$posisi')");
            if($query) {
                echo "<script>alert('User Account berhasil ditambahkan') 
                location.replace('index')</script>";
            } else {
                echo "<script>alert('gagal ditambahkan')</script>";
            }
        }
    }

    function deleteAccount($id) {
        $query = $this->query("DELETE FROM tc_user WHERE U_REGID = '$id'");
    }

    function detalAccount($id) {
        $query = $this->query("SELECT * FROM tc_user WHERE U_REGID = '$id'");
        $row = $query->fetch_assoc();
        return $row;
    }

    function ubahStatus($id, $status) {
        $query = $this->query("UPDATE tc_user SET U_STATUS = '$status' WHERE U_REGID = '$id'");

        if($query) {
            echo "<script>alert('Status berhasil diubah') 
                location.replace('index')</script>";
        } else {
            echo "<script>alert('Status gagal diubah')</script>";
        }
    }

    function profile() {
        $id = $_SESSION["U_REGID"];
        $query  = $this->query("SELECT * FROM tc_user WHERE U_REGID = '$id'");
        $row    = $query->fetch_assoc();
        return $row;
    }

    function getProfileClient() {
        $id = $_SESSION["U_CLIENTID"];
        $query = $this->query("SELECT * FROM tc_client WHERE TC_REGID = '$id'");
        $row    = $query->fetch_assoc();
        return $row;
    }

    function updateProfil($profil) {
        $date     = date("Y-m-d H:i:s");
        $regId    = $this->clean_post($profil["regId"]);
        $fullname = $this->clean_post($profil["fullname"]);
        $email    = $this->clean_post($profil["email"]);
        $telp     = $this->clean_post($profil["noTelpon"]);
        $alamat   = $this->clean_post($profil["alamat"]);
        $key      = base64_encode($fullname.$date);

        $query    = $this->query("UPDATE tc_user SET U_FULLNAME = '$fullname', U_EMAIL = '$email', U_TELPON = '$telp', U_ALAMAT = '$alamat', U_KEY = '$key' WHERE U_REGID = '$regId'");
        if($query) {
            echo "<script>alert('Profile berhasil diubah') 
                location.replace('my-profile')</script>";
        } else {
            echo "<script>alert('Status gagal diubah')</script>";
        }
    }

    function updateAvatar($logo) {
        $userId = $this->clean_post($logo["regId"]);
        // $file = mysqli_escape_string($this->conn(), $logo["IMG_PATH"]["name"]);

        $target_dir = "../../assets/images/avatar/profile/";

        $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["IMG_PATH"]['name']));
        $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["IMG_PATH"]['name']));
        $target_file = $target_dir . $newfilename; 
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        move_uploaded_file($_FILES["IMG_PATH"]["tmp_name"], $target_file);
        $query = $this->query("UPDATE tc_user SET U_AVATAR = '$newname' WHERE U_REGID = '$userId'");

        if($query){
            echo "<script>alert('Avatar Berhasil diupdate')
            location.replace('my-profile')</script></script>";
        } else {
            echo "<script>alert('Failed,update avatar gagal')</script>";
        }
    }

    function settings($setting) {
        $regId    = $this->clean_post($setting["regId"]);
        $password = $this->clean_post($setting["password"]);
        $konfirm  = $this->clean_post($setting["konfirm"]);

        if($password != $konfirm) {
            echo "<script>alert('Password yang anda masukkan tidak sama')</script>";
        } else {
            $query = $this->query("UPDATE tc_user SET U_PASSWORD_HASH = '$password' WHERE U_REGID = '$regId'");

            if($query) {
                echo "<script>alert('Password Berhasil diupdate')
                location.replace('setting')</script>";
            } else {
                echo "<script>alert('Failed,update password gagal')</script>";
            }
        }
    }


    function orderApp($order) {
 
        $cek1 = $this->query("SELECT * FROM tc_setting ORDER BY TS_BIGID DESC LIMIT 1");
        $cek  = $cek1->fetch_assoc();
        $ex  = $cek["TS_PROJECT_PREFIX"];
        $ai  = $cek["TS_PNWR_PREFIX"];

        $no_pn = intval($ai)+1;
        $no_pr = intval($ex)+1;

        $b = 'PP';
        $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
        $d = date('y');
        $e = date("m");
        $no_penawaran = $no_pn.'/'.$b.'/'.'CAN/'.$c[date('n')].'/'.date("Y");
        $no_project   = $b.$d.$e.$no_pr;

        //nomer urut order 
        $coll = $this->query("SELECT COUNT(TP_PROJECTID) AS jml FROM tc_order");
        $row = $coll->fetch_assoc();
        $kode = $row["jml"];
        $tmbhKode = intval($kode)+1;

        //nomer urut client
        $cl = $this->query("SELECT COUNT(TC_BIGID) AS jmlCo FROM tc_client");
        $clients = $cl->fetch_assoc();
        $urut    = $clients["jmlCo"];
        $no = intval($urut)+1;
        $clientId = "CL".date("His").$no;

        $name       = $this->clean_post($order["nama"]);
        $email      = $this->clean_post($order["email"]);
        $noTelpon   = $this->clean_post($order["noTelpon"]);
        $subject    = $this->clean_post($order["subject"]);
        $pl         = implode(', ', $order['platform']);
        $platform   = $this->clean_post($pl);
        $deskripsi  = $this->clean_post($order["deskripsi"]);
        $date       = date("Y-m-d");
        $projectId  =  "PR".date("His").$tmbhKode;
        $status     = "ORDER_MASUK";
        $pendukung  = $_FILES["pendukung"]['name'];

        $target_dir = "../assets/images/avatar/gambar/";

        if (count($pendukung) > 0 || count($pendukung) == "") {
            for ($i=0; $i < count($pendukung); $i++) { 
                $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["pendukung"]['name'][$i]));
                $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["pendukung"]['name'][$i]));
                $target_file = $target_dir . $newfilename; 
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                move_uploaded_file($_FILES["pendukung"]["tmp_name"][$i], $target_file);
                $query = $this->query("INSERT INTO tc_order (TP_PROJECTID, TP_CLIENTID, TO_BUKTI_PENDUKUNG, TP_STATUS) VALUES ('$projectId', '$clientId', '$newfilename', '$status')");
            }
            //tambah ke tabel project
            $query1 = $this->query("INSERT INTO tc_project (TP_REG_PROJECT, TP_NO_PROJECT, TP_NAMA_PROJECT, TP_DESKRIPSI, TP_PLATFORM, TP_STATUS, TP_PROJECT_TGL) VALUES ('$projectId', '$no_project', '$subject', '$deskripsi', '$platform', '$status', '$date')");
            if($query1) {
            //tambah ke tabel client
                $df = $this->query("UPDATE tc_setting SET TS_PROJECT_PREFIX = '$no_pr' WHERE TS_BIGID = '1'");
                $dataCl = $this->query("SELECT * FROM tc_client WHERE TC_EMAIL = '$email'");
                $datas = $dataCl->fetch_assoc();
                $id = $datas["TC_REGID"];
                if(mysqli_num_rows($dataCl) > 0) {
                    $query3 = $this->query("UPDATE tc_order SET TP_CLIENTID = '$id' WHERE TP_PROJECTID = '$projectId'");
                } else {
                    $query2 = $this->query("INSERT INTO tc_client (TC_REGID, TC_NAMA, TC_EMAIL, TC_TELPON) VALUES ('$clientId', '$name', '$email', '$noTelpon')");
                }
                echo "<script>alert('Order Berhasil disimpan,selanjutnya tunggu konfirmasi dari tim analis kami')</script>";
            } else {
                echo "<script>alert('Failed,order gagal')</script>";
            }
        }

    }
     
    // === FUNCTION STATUS === //
    //status ubah ketika project baru masuk untuk siap analisa pada account
    function ubahStatusProject($id) {
        $sess   = $_SESSION["U_REGID"];
        $sts    = 'WAITING_ANALIS';

        $query = $this->query("UPDATE tc_order SET TP_STATUS = '$sts', TP_APPROVE = '$sess' WHERE TP_PROJECTID = '$id'");
        if($query) {
            $an = $this->query("INSERT INTO  tc_cangelog (TC_PROJECTID, TC_KETERANGAN, TC_CREATED_BY) VALUES ('$id', 'Project baru Siap dianalisa oleh Account', '$sess')");
            $sql = $this->query("UPDATE tc_project SET TP_STATUS = '$sts' WHERE TP_REG_PROJECT = '$id'");
            echo "<script>alert('Status Berhasil diupdate')
                location.replace('../new')</script>";
        } else {
            echo "<script>alert('Failed,Status gagal diubah')</script>";
        }
    }

    function statusDealReject($id, $status) {
        $sess   = $_SESSION["U_REGID"];
        $query = $this->query("UPDATE tc_project SET TP_STATUS = '$status' WHERE TP_REG_PROJECT = '$id'");
        if($query) {
            $qq = $this->query("UPDATE tc_order SET TP_STATUS = '$status' WHERE TP_PROJECTID = '$id'");
            if($status === "PROJECT_DEAL") {
                $an = $this->query("INSERT INTO  tc_cangelog (TC_PROJECTID, TC_KETERANGAN, TC_CREATED_BY) VALUES ('$id', 'Project Telah dinyatakan deal,siap untuk diproduksi', '$sess')");
            } else {
                $an = $this->query("INSERT INTO  tc_cangelog (TC_PROJECTID, TC_KETERANGAN, TC_CREATED_BY) VALUES ('$id', 'Project direject atau tidak jadi', '$sess')");
            }

             echo "<script>alert('Status Berhasil diupdate')
                location.replace('../ready-followup')</script>";
        } else {
            echo "<script>alert('Failed,Status gagal diubah')
                location.replace('../ready-followup')</script>";
        }
    }

    function statusReject($reject) {
        $penawaranId = $this->clean_all($reject["penId"]);
        $projectId   = $this->clean_post($reject["proId"]);
        $status      = $this->clean_post($reject["status"]);
        $keterangan  = $this->clean_post($reject["keterangan"]);

        $query = $this->query("UPDATE tc_project SET TP_STATUS = '$status' WHERE TP_REG_PROJECT = '$projectId'");
        if($query) {
            if($status === "WAITING_ANALIS") {
                $hh   = $this->query("UPDATE tc_penawaran SET TP_STATUS = '0' WHERE TPN_KODE = '$penawaranId'");
                $query1 = $this->query("UPDATE tc_order SET TP_STATUS = '$status', TP_CHECK = 'Y' WHERE TP_PROJECTID = '$projectId'");
            } else {
                $hh   = $this->query("UPDATE tc_penawaran SET TP_STATUS = '1' WHERE TPN_KODE = '$penawaranId'");
                $query1 = $this->query("UPDATE tc_order SET TP_STATUS = '$status', TP_CHECK = 'N' WHERE TP_PROJECTID = '$projectId'");
            }
            
            $logs = $this->query("INSERT INTO tc_cangelog (TC_PROJECTID, TC_PENAWARANID, TC_KETERANGAN) VALUES ('$projectId', '$penawaranId', '$keterangan')");
            echo "<script>alert('Status Berhasil diupdate')
                location.replace('waiting-analis')</script>";
        } else {
            echo "<script>alert('Failed,Status gagal diubah')</script>";
        }
    }

    function updateLog($logs) {
        $penawaranId = $this->clean_all($logs["penId"]);
        $projectId   = $this->clean_post($logs["proId"]);
        $keterangan  = $this->clean_post($logs["keterangan"]);

        $logs = $this->query("INSERT INTO tc_cangelog (TC_PROJECTID, TC_PENAWARANID, TC_KETERANGAN) VALUES ('$projectId', '$penawaranId', '$keterangan')");
        if($query && $query1) {
            echo "<script>alert('Cange Log Berhasil diupdate')
                location.replace('../detail-project')</script>";
        } else {
            echo "<script>alert('Failed,Status gagal diubah')</script>";
        }
    }

    function submitAnalis($analis) {
        $cek1 = $this->query("SELECT * FROM tc_setting ORDER BY TS_BIGID DESC LIMIT 1");
        $cek  = $cek1->fetch_assoc();
        $ex  = $cek["TS_PROJECT_PREFIX"];
        $ai  = $cek["TS_PNWR_PREFIX"];

        $no_pn = intval($ai)+1;
        $no_pr = intval($ex)+1;

        $b = 'PP';
        $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
        $d = date('y');
        $e = date("m");
        $no_penawaran = $no_pn.'/'.$b.'/'.'CAN/'.$c[date('n')].'/'.date("Y");
        $no_project   = $b.$d.$e.$no_pr;

        //nomer urut order 
        $coll = $this->query("SELECT COUNT(TPN_KODE) AS jml FROM tc_penawaran");
        $row = $coll->fetch_assoc();
        $kode = $row["jml"];
        $tmbhKode = intval($kode)+1;
        $nomer = "PRN".date("His").$tmbhKode;
        $status = 'PROJECT_ANALIS_ACC';

        $projectId = $this->clean_all($analis["projectId"]);
        $file1      = $_FILES["pendukung"]['name'];
        $target_dir = "../../../assets/images/avatar/file/";

        if(count($file1) > 0) {
            //echo "Sukses";
            for ($i=0; $i < count($file1); $i++) { 
                $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["pendukung"]['name'][$i]));
                $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["pendukung"]['name'][$i]));
                $target_file = $target_dir . $newfilename; 
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                move_uploaded_file($_FILES["pendukung"]["tmp_name"][$i], $target_file);

                $query = $this->query("INSERT INTO tc_penawaran (TPN_NO_PENAWARAN, TPN_KODE, TPN_PROJECTID, TPN_BERKAS) VALUES ('$no_penawaran', '$nomer', '$projectId', '$newfilename')");
            }

            $query1 = $this->query("UPDATE tc_order SET TP_PENAWARANID = '$nomer', TP_STATUS = '$status' WHERE TP_PROJECTID = '$projectId'");
            if($query1) {
                $df = $this->query("UPDATE tc_setting SET TS_PNWR_PREFIX = '$no_pn' WHERE TS_BIGID = '1'");
                $an = $this->query("INSERT INTO  tc_cangelog (TC_PROJECTID, TC_PENAWARANID) VALUES ('$projectId', '$nomer')");
                $sql = $this->query("UPDATE tc_project SET TP_STATUS = '$status' WHERE TP_REG_PROJECT = '$projectId'");
                echo "<script>alert('Status Berhasil diupdate')
                location.replace('../waiting-analis')</script>";
            } else {
               echo "<script>alert('Failed,Status gagal diubah')</script>";
            }
        } else {
            echo "File kosong";
        }
    }

    function updateClient($data) {
        $clientId   = $this->clean_all($data["clientId1"]);
        $noTelp     = $this->clean_all($data["noTelp"]);
        $instansi   = $this->clean_post($data["instansi"]);
        $alamat     = $this->clean_post($data["alamat"]);

        $query = $this->query("UPDATE tc_client SET TC_ALAMAT = '$alamat', TC_TELPON = '$noTelp', TC_INSTANSI = '$instansi' WHERE TC_REGID = '$clientId'");
        if($query) {
            echo "<script>alert('Client Berhasil diupdate')</script>";
        } else {
            echo "<script>alert('Failed,Client gagal diupdate')</script>";
        }
    }

    function tambahClient($akun) {
        //nomer urut order 
        $coll = $this->query("SELECT COUNT(U_REGID) AS jml FROM tc_user WHERE U_GROUP_RULE = 'TO_CLIENT'");
        $row = $coll->fetch_assoc();
        $kode = $row["jml"];
        $tmbhKode = intval($kode)+1;

        $clientId = $this->clean_all($akun["clientId"]);
        $username = $this->clean_all($akun["username"]);
        $fullname = $this->clean_post($akun["fullname"]);
        $email    = $this->clean_all($akun["email"]);
        $noTelpon = $this->clean_post($akun["noTelpon"]);
        $alamat   = $this->clean_post($akun["alamat"]);
        $password = md5("12345678");
        $status   = "USER_ACTIVE";
        $rule     = "TO_CLIENT";
        $nomer    = $username."0".$tmbhKode; 

        $query = $this->query("INSERT INTO tc_user (U_REGID, U_NAME, U_FULLNAME, U_EMAIL, U_GROUP_RULE, U_STATUS, U_ALAMAT, U_PASSWORD_HASH, U_TELPON, U_CLIENTID, U_ACTIVE_CLIENT) VALUES ('$nomer', '$username', '$fullname', '$email', '$rule', '$status', '$alamat', '$password', '$noTelpon', '$clientId', '0')");

        if($query) {
             echo "<script>alert('Akun Client Berhasil ditambahkan')</script>";
        } else {
             echo "<script>alert('Akun Client gagal ditambahkan')</script>";
        }

    }

    function updateLeader($leaderId) {
        $projectId = $this->clean_post($leaderId["projectId"]);
        $leader    = $this->clean_all($leaderId["leader"]);
        
        $query = $this->query("UPDATE tc_project SET TP_LEADER = '$leader' WHERE TP_REG_PROJECT = '$projectId'");

        if($query) {
             echo "<script>alert('Leader Berhasil ditambahkan')</script>";
        } else {
             echo "<script>alert('Leader gagal ditambahkan')</script>";
        }
    }

    function updateBerkas($berkas) {
        $projectId = $this->clean_all($berkas["projectId1"]);

        $target_dir = "../../assets/images/avatar/file/";

        //mou
        $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["mou"]['name']));
        $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["mou"]['name']));
        $target_file = $target_dir . $newfilename; 
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        move_uploaded_file($_FILES["mou"]["tmp_name"], $target_file);

        //PD
        $newname1 = date('dmYHis').str_replace(" ", "", basename($_FILES["projectDetail"]['name']));
        $newfilename1 = date('dmYHis').str_replace(" ", "", basename($_FILES["projectDetail"]['name']));
        $target_file1 = $target_dir . $newfilename1; 
        $imageFileType1 = pathinfo($target_file1, PATHINFO_EXTENSION);

        move_uploaded_file($_FILES["projectDetail"]["tmp_name"], $target_file1);

        //BP
        $newname2 = date('dmYHis').str_replace(" ", "", basename($_FILES["buktiPembayaran"]['name']));
        $newfilename2 = date('dmYHis').str_replace(" ", "", basename($_FILES["buktiPembayaran"]['name']));
        $target_file2 = $target_dir . $newfilename2; 
        $imageFileType2 = pathinfo($target_file2, PATHINFO_EXTENSION);

        move_uploaded_file($_FILES["projectDetail"]["tmp_name"], $target_file2);

        //invoice
        $newname3 = date('dmYHis').str_replace(" ", "", basename($_FILES["invoice"]['name']));
        $newfilename3 = date('dmYHis').str_replace(" ", "", basename($_FILES["invoice"]['name']));
        $target_file3 = $target_dir . $newfilename3; 
        $imageFileType3 = pathinfo($target_file3, PATHINFO_EXTENSION);

        move_uploaded_file($_FILES["invoice"]["tmp_name"], $target_file3);

        //kwitansi
        $newname4 = date('dmYHis').str_replace(" ", "", basename($_FILES["kwitansi"]['name']));
        $newfilename4 = date('dmYHis').str_replace(" ", "", basename($_FILES["kwitansi"]['name']));
        $target_file4 = $target_dir . $newfilename4; 
        $imageFileType4 = pathinfo($target_file4,PATHINFO_EXTENSION);

        move_uploaded_file($_FILES["kwitansi"]["tmp_name"], $target_file4);

        $query = $this->query("UPDATE tc_project SET TP_MOU = '$newfilename', TP_PROJECT_DETAIL = '$newfilename1', TP_BUKTI_PEMBAYARAN = '$newfilename2', TP_INVOICE = '$newfilename3', TP_KWITANSI = '$newfilename4' WHERE TP_REG_PROJECT = '$projectId'");

        if($query) {
            echo "<script>alert('Berkas Pendukung Berhasil ditambahkan')</script>";
        } else {
            echo "<script>alert('Berkas Pendukung gagal ditambahkan')</script>";
        }
    }

    function tambahProspek($prospek) {
        $project = $this->clean_all($prospek["project"]);

        $query = $this->query("INSERT INTO tc_prospek_project (TPR_PROJECTID) VALUES ('$project')");

        if($query) {
            echo "<script>alert('Project prospek berhasil Berhasil ditambahkan')</script>";
        } else {
            echo "<script>alert('Project prospek gagal ditambahkan')</script>";
        }
    }

    function tambahProjectOffline($offline) {
        $cek1 = $this->query("SELECT * FROM tc_setting ORDER BY TS_BIGID DESC LIMIT 1");
        $cek  = $cek1->fetch_assoc();
        $ex  = $cek["TS_PROJECT_PREFIX"];
        $ai  = $cek["TS_PNWR_PREFIX"];

        $no_pn = intval($ai)+1;
        $no_pr = intval($ex)+1;

        $b = 'PP';
        $c = array('','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
        $d = date('y');
        $e = date("m");
        $no_penawaran = $no_pn.'/'.$b.'/'.'CAN/'.$c[date('n')].'/'.date("Y");
        $no_project   = $b.$d.$e.$no_pr;

        $nama           = $this->clean_post($offline["nama"]);
        $noTelpon       = $this->clean_all($offline["noTelpon"]);
        $email          = $this->clean_post($offline["email"]);
        $instansi       = $this->clean_post($offline["instansi"]);
        $alamat         = $this->clean_post($offline["alamat"]);
        $projectMasuk   = $this->clean_post($offline["projectMasuk"]);
        $subject        = $this->clean_post($offline["subject"]);
        $platform       = implode(', ', $offline['platform']);
        $deskripsi      = $this->clean_post($offline["deskripsi"]);
        $berkas         = $_FILES["berkasOrder"]['name'];
        $date           = $this->clean_post($offline["projectMasuk"]);
        $status         = "WAITING_ANALIS";

        //nomer urut order 
        $coll = $this->query("SELECT COUNT(TP_PROJECTID) AS jml FROM tc_order");
        $row = $coll->fetch_assoc();
        $kode = $row["jml"];
        $tmbhKode = intval($kode)+1;
        $projectId  =  "PR".date("His").$tmbhKode;

        //nomer urut client
        $cl = $this->query("SELECT COUNT(TC_BIGID) AS jmlCo FROM tc_client");
        $clients = $cl->fetch_assoc();
        $urut    = $clients["jmlCo"];
        $no = intval($urut)+1;
        $clientId = "CL".date("His").$no;

        $target_dir = "../../assets/images/avatar/gambar/";

        if (count($berkas) > 0 || count($berkas) == "") {
            for ($i=0; $i < count($berkas); $i++) { 
                $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["berkasOrder"]['name'][$i]));
                $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["berkasOrder"]['name'][$i]));
                $target_file = $target_dir . $newfilename; 
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                move_uploaded_file($_FILES["berkasOrder"]["tmp_name"][$i], $target_file);

                $query = $this->query("INSERT INTO tc_order (TP_PROJECTID, TP_CLIENTID, TO_BUKTI_PENDUKUNG, TP_STATUS) VALUES ('$projectId', '$clientId', '$newfilename', '$status')");
            }
            //tambah ke tabel project
            $query1 = $this->query("INSERT INTO tc_project (TP_REG_PROJECT, TP_NO_PROJECT, TP_NAMA_PROJECT, TP_DESKRIPSI, TP_PLATFORM, TP_STATUS, TP_PROJECT_TGL) VALUES ('$projectId', '$no_project', '$subject', '$deskripsi', '$platform', '$status', '$date')");
            if($query1) {
                $df = $this->query("UPDATE tc_setting SET TS_PROJECT_PREFIX = '$ex' WHERE TS_BIGID = '1'");
            //tambah ke tabel client
                $dataCl = $this->query("SELECT * FROM tc_client WHERE TC_EMAIL = '$email'");
                $datas = $dataCl->fetch_assoc();
                $id = $datas["TC_REGID"];
                if(mysqli_num_rows($dataCl) > 0) {
                    $query3 = $this->query("UPDATE tc_order SET TP_CLIENTID = '$id' WHERE TP_PROJECTID = '$projectId'");
                } else {
                    $query2 = $this->query("INSERT INTO tc_client (TC_REGID, TC_NAMA, TC_ALAMAT, TC_EMAIL, TC_TELPON, TC_INSTANSI) VALUES ('$clientId', '$nama', '$alamat', '$email', '$noTelpon', '$instansi')");
                }

                echo "<script>alert('Data Order Project berhasil ditambahkan')</script>";
            } else {
                echo "<script>alert('Project gagal ditambahkan')</script>";
            }
        }
    }

    function recover1($recover) {
         require '../assets/PHPMailer/src/Exception.php';
         require '../assets/PHPMailer/src/PHPMailer.php';
         require '../assets/PHPMailer/src/SMTP.php';

        $mail = $this->clean_post($recover["email"]);

        $get = $this->query("SELECT * FROM tc_user WHERE U_EMAIL = '$mail'");
        if(mysqli_num_rows($get) > 0) {
            function generateRandomString($length = 10) {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
            }
              $response= array();
              $email=null;

              $email_masuk = $mail;
              $key = generateRandomString();
              $email_key = base64_encode($email_masuk.$key);
              // $querySelect =mysqli_query($this->conn(),"SELECT * FROM to_user WHERE U_EMAIL ='$email_masuk'");
              while($data = mysqli_fetch_array($get)){
                     $id_user= $data['U_BIGID'];
                     $email = $data['U_EMAIL'];             
              }

             if($email){
                  $queryUpdate= mysqli_query($this->conn(),"UPDATE tc_user SET U_FORGOT_PASSWORD = '$email_key', U_KEY_FORGOT = '$key' WHERE U_BIGID ='$id_user'");
                  $mail = new PHPMailer\PHPMailer\PHPMailer();
                  $mail->IsSMTP();
                  try {
                      $mail->SMTPDebug = 0;                                 // Enable verbose debug output
                      $mail->isSMTP();                                      // Set mailer to use SMTP
                      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                      $mail->SMTPAuth = true;                               // Enable SMTP authentication
                      $mail->Username = 'crmanagement.official@gmail.com';                 // SMTP username
                      $mail->Password = 'P@ssw0rdadmin';                           // SMTP password
                      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                      $mail->Port = 587;                                      // TCP port to connect to
                      $mail->SMTPOptions = array(
                          'ssl' => array(
                              'verify_peer' => false,
                              'verify_peer_name' => false,
                              'allow_self_signed' => true
                          )
                      ); 
            
                    $mail->AddAddress($email); //email tujuan isi dengan emailmu misal test@test.com
                    $message ="
                    Halo, silahkan reset password anda <a href='http://mitra.can.co.id/login/recover?email=$email_masuk&key=$key'>disini</a>";
                    $mail->SetFrom('crmanagement.official@gmail.com','Reset Password(noreply)'); // email pengirim
                    $mail->Subject = 'Recovery Password';                       
                    $mail->MsgHTML('<p>'.$message);
                    $mail->Send();{
                    echo "<script>alert('password berhasil di reset silahkan cek email')
                            location.replace('../login/')</script></script>";
                    }   
                  } catch (phpmailerException $e) {
                     echo $e->errorMessage(); 
                 } 
             } 
        } else {
           echo "<script>alert('Akun tidak ditemukan,silahkan lengkapi data anda dengan benar')</script>";
        }
    }

    function getData($user){
        $query = $this->query("SELECT * FROM tc_user WHERE U_EMAIL = '$user'");
        $row = $query->fetch_assoc();
        return $row;
    }

    function updateRecovery($ur){
        $password = $this->clean_all($ur["password"]);
        $konfirm  = $this->clean_all($ur["konfirm"]);
        $email = $this->clean_all($ur["email"]);

        if($password != $konfirm) {
            echo "<script>alert('Password yang anda masukkan tidak sama')</script>";
        } else {
            $query = $this->query("UPDATE tc_user SET U_PASSWORD_HASH = '$konfirm' WHERE U_EMAIL = '$email'");

            if($query) {
               echo "<script>alert('password berhasil diubah')
                            location.replace('../login/')</script></script>";
            } else {
                echo "<script>alert('password gagal diubah')</script>";
            }
        }

    }

    function upClient($cc) {
        $id         = $this->clean_all($cc["clientId"]);
        $instansi   = $this->clean_post($cc["instansi"]);
        $alamat     = $this->clean_post($cc["alamat"]);

        $query      = $this->query("UPDATE tc_client SET TC_ALAMAT = '$alamat', TC_INSTANSI = '$instansi' WHERE TC_REGID = '$id'");

        if($query) {
            echo "<script>alert('data Client berhasil diubah')</script>";
        } else {
            echo "<script>alert('data Client gagal diubah')</script>";
        }
    }

    //GET NOTIF 
    function notif1() {
        $query = $this->query("SELECT COUNT(*) AS jml1 FROM tc_project WHERE TP_STATUS = 'ORDER_MASUK'");
        $row   = $query->fetch_assoc();
        return $row;
    }

     function notif2() {
        $query = $this->query("SELECT COUNT(*) AS jml2 FROM tc_project WHERE TP_STATUS = 'WAITING_ANALIS'");
        $row   = $query->fetch_assoc();
        return $row;
    }

    function getLog($id) {
        $ses = $_SESSION["U_REGID"];
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_cangelog AS A INNER JOIN tc_project AS B ON A.TC_PROJECTID = B.TP_REG_PROJECT WHERE A.TC_PROJECTID = '$id'");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getHelp() {
        $rows = array();
        $query = $this->query("SELECT * FROM tc_help ORDER BY TH_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function insertHelp($help) {
        $nama  = $_SESSION["U_FULLNAME"];
        $title = $this->clean_all($help["title"]);
        $isi   = $this->clean_post($help["isi"]);

        $query = $this->query("INSERT INTO tc_help (TH_TITTLE, TH_ISI, TG_CREATED_BY) VALUES ('$title', '$isi', '$nama')");

        if($query) {
            echo "<script>alert('Data Help berhasil ditambahkan')</script>";
        } else {
            echo "<script>alert('Data Help gagal ditambahkan')</script>";
        }
    }

    function editHelpId($id) {
        $query  = $this->query("SELECT * FROM tc_help WHERE TH_BIGID = '$id'");
        $row    = $query->fetch_assoc();

        return $row;
    }

    function updateHelp($help) {
        $id    = $this->clean_all($help["helpId"]);
        $title = $this->clean_all($help["title"]);
        $isi   = $this->clean_post($help["isi"]);

        $query = $this->query("UPDATE tc_help SET TH_TITTLE = '$title', TH_ISI = '$isi' WHERE TH_BIGID = '$id'");

         if($query) {
            echo "<script>alert('Data Help berhasil diubah')
                            location.replace('../help/')</script></script>";
        } else {
            echo "<script>alert('Data Help gagal diubah')</script>";
        }
    }

    //NEW FUNCTION
    /* cop paste di server */

    function getProjectLeader() {
        $id = $_SESSION["U_REGID"];
        $rows = array();
        $query = $this->query("SELECT * FROM tc_project WHERE TP_STATUS IN ('PROJECT_DEAL', 'PROJECT_DEVELOP') AND TP_LEADER = '$id' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows; 
    }

    function getTeamProject($id, $lead) {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_project_team AS A INNER JOIN tc_user AS B ON A.TPD_LEADER = B.U_REGID WHERE A.TPD_PROJECTID = '$id' AND A.TPD_LEADER = '$lead' ORDER BY A.TPD_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getUserTeam() {
        $id = $_SESSION["U_REGID"];
        $rows  = array();
        $query = $this->query("SELECT * FROM tc_user WHERE U_GROUP_RULE = 'TO_PRODUKSI' AND U_STATUS = 'USER_ACTIVE' AND U_REGID != '$id'");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getNamaLead($id) {
        $query = $this->query("SELECT U_FULLNAME FROM tc_user WHERE U_REGID = '$id'");
        $row   = $query->fetch_assoc();
        return $row;
    }

    function tambahTeam($team) {
        $ses = $_SESSION["U_REGID"];
        $nama = $this->clean_all($team["nama"]);
        $keterangan = $this->clean_post($team["keterangan"]);
        $projectId = $this->clean_all($team["projectId"]);
        $leadId = $this->clean_all($team["leadId"]);

        $query = $this->query("INSERT INTO (TPD_PROJECTID, TPD_TEAM, TPD_KETERANGAN, TPD_LEADER, TPD_STATUS, TPD_CREATED_BY) VALUES ('$projectId', '$nama', '$keterangan', '$leadId', 'AKTIF', '$ses')");

        if($query) {
            echo "<script>alert('Data team Berhasil ditambahkan')</script>";;
        } else {
            echo "<script>alert('Data gagal ditambahkan')</script>";
        }
    }

    function ubahSelesai($id, $status) {
        $query = $this->query("UPDATE tc_project SET TP_STATUS = '$status' WHERE TP_REG_PROJECT = '$id'");
        if($query) {
            $query1 = $this->query("UPDATE tc_order SET TP_STATUS = '$status' WHERE TP_PROJECTID = '$id'");
            echo "<script>alert('status berhasil diubah')
                            location.replace('../../team')</script></script>";
        } else {
            echo "<script>alert('Data gagal diubah')</script>";
        }
    }


    function getPriority() {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_project_priority AS A INNER JOIN tc_project AS B ON A.TJ_KODEPROJECT = B.TP_REG_PROJECT ORDER BY A.TJ_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function projectProDev() {
        $rows = array();
        $query = $this->query("SELECT A.*, B.* FROM tc_project AS A INNER JOIN tc_project_priority AS B ON A.TP_REG_PROJECT = B.TJ_KODEPROJECT WHERE A.TP_STATUS = 'PROJECT_DEVELOP' OR A.TP_STATUS = 'PROJECT_DEAL'");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;

    }

    //COUNT PROJECT
    function countProdev() {
        $query = $this->query("SELECT COUNT(*) AS jml FROM tc_project WHERE TP_STATUS IN ('PROJECT_DEAL', 'PROJECT_DEVELOP')");
        $row = $query->fetch_assoc();

        return $row;
    }

    function countProEnd() {
        $query = $this->query("SELECT COUNT(*) AS jml1 FROM tc_project WHERE TP_STATUS IN ('PROJECT_SELESAI')");
        $row = $query->fetch_assoc();

        return $row;
    }

    function countProcess() {
        $query = $this->query("SELECT COUNT(*) AS jml2 FROM tc_project WHERE TP_STATUS NOT IN ('PROJECT_SELESAI', 'PROJECT_DEAL', 'PROJECT_DEVELOP')");
        $row = $query->fetch_assoc();

        return $row;
    }

     function get_chart_project_proces($bulan) {
        $tahun = date('Y');
        $rows = NULL;
        $query = $this->query("SELECT COUNT(TP_REG_PROJECT) AS jmlbln1 FROM tc_project WHERE month(TP_PROJECT_TGL) = '$bulan' AND year(TP_PROJECT_TGL) = '$tahun' AND TP_STATUS NOT IN ('PROJECT_SELESAI')");
        while($row = $query->fetch_assoc()) {
            $rows = $row["jmlbln1"];
        }
        return $rows;   
    }

    function get_chart_project_selesai($bulan) {
        $tahun = date('Y');
        $rows = NULL;
        $query = $this->query("SELECT COUNT(TP_REG_PROJECT) AS jmlbln2 FROM tc_project WHERE month(TP_PROJECT_TGL) = '$bulan' AND year(TP_PROJECT_TGL) = '$tahun' AND TP_STATUS = 'PROJECT_SELESAI'");
        while($row = $query->fetch_assoc()) {
            $rows = $row["jmlbln2"];
        }
        return $rows;   
    }

    function get_project_reject() {
        $rows = array();
        $query = $this->query("SELECT COUNT(TP_REG_PROJECT) AS jmlre FROM tc_project WHERE TP_STATUS = 'PROJECT_REJECT'");
        while($row = $query->fetch_assoc()) {
            $rows = $row["jmlre"];
        }
        return $rows;  
    }


    //END COUNT

    function tambahPrio($prio) {
        $id      = $this->clean_post($prio["proId"]);
        $priotas = $this->clean_post($prio["priotas"]);
        $status  = "SCEDULE";

        $query   = $this->query("INSERT INTO tc_project_priority (TJ_KODEPROJECT, TJ_PRIORITAS, TJ_STATUS) VALUES ('$id', '$priotas', '$status')");

         if($query) {
            echo "<script>alert('Project Priority berhasil ditambahkan')
                            location.replace('../project/priority')</script>";
        } else {
            echo "<script>alert('Data Priority gagal ditambahkan')</script>";
        }
    }

    function statusPriority($id, $status) {
        $sess   = $_SESSION["U_REGID"];
        $status1 = "PROJECT_DEVELOP"; 
        $query = $this->query("UPDATE tc_project_priority SET TJ_STATUS = '$status' WHERE TJ_KODEPROJECT = '$id'");
        if($query) {
            if($status === "PROCESS") {
                $qq = $this->query("UPDATE tc_project SET TP_STATUS = '$status1' WHERE TP_REG_PROJECT = '$id'");
                $fr = $this->query("UPDATE tc_order SET TP_STATUS = '$status1' WHERE TP_PROJECTID = '$id'");
                $an = $this->query("INSERT INTO  tc_cangelog (TC_PROJECTID, TC_KETERANGAN, TC_CREATED_BY) VALUES ('$id', 'Project Proses Produksi / Maintenance', '$sess')");

                 echo "<script>alert('Status Project Priority berhasil diubah')
                            location.replace('../home/')</script>";
            } elseif($status === "FINISH") {
                $an = $this->query("INSERT INTO  tc_cangelog (TC_PROJECTID, TC_KETERANGAN, TC_CREATED_BY) VALUES ('$id', 'Project Selesai Develop dan siap process Quality Control', '$sess')");

                 echo "<script>alert('Status Project Priority berhasil diubah')
                            location.replace('../home/')</script>";
            }
        } else {
            echo "<script>alert('Failed,Status gagal diubah')</script>";
        }
        
    }

    function getReportPerStatus($report) {
        $status     = $this->clean_all($report["status"]);
        $dateStart  = date("Y-m-d", strtotime($_POST["tanggalAwal"]));
        $dateEnd    = date("Y-m-d", strtotime($_POST["tanggalAkhir"]));

        $rows = array();
        $query = $this->query("SELECT * FROM tc_project AS A WHERE A.TP_STATUS = '$status' AND TP_PROJECT_TGL BETWEEN '$dateStart' AND '$dateEnd' ORDER BY TP_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function getReportByClient($report) {
        $id         = $_SESSION["U_CLIENTID"];
        $status     = $this->clean_all($report["status"]);
        $dateStart  = date("Y-m-d", strtotime($_POST["tanggalAwal"]));
        $dateEnd    = date("Y-m-d", strtotime($_POST["tanggalAkhir"]));

        $rows = array();
        $query = $this->query("SELECT * FROM tc_project AS A INNER JOIN tc_order AS B ON A.TP_REG_PROJECT = B.TP_PROJECTID WHERE A.TP_STATUS = '$status' AND A.TP_PROJECT_TGL BETWEEN '$dateStart' AND '$dateEnd' AND B.TP_CLIENTID = '$id' ORDER BY A.TP_CREATED_AT DESC LIMIT 0,1");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }
    
    //== WINDERLIST PRODUKSI ==/
    //SELECT A.*, B.* FROM tc_wunderlist AS A INNER JOIN tc_wunderlist_detail AS B ON A.TW_NO = B.TWD_WUNDERLISTNO WHERE A.TW_PROJECTID = '$id'

    function getWunderlist($id) {
        $rows  = array();
        $query = $this->query("SELECT * FROM tc_wunderlist WHERE TW_PROJECTID = '$id' ORDER BY TW_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function addWunderlist($wunder) {
        //no urut
        $qr = $this->query("SELECT COUNT(TW_NO) AS jml FROM tc_wunderlist");
        $data = $qr->fetch_assoc();
        $urut = $data["jml"];
        $no = intval($urut)+1;
        $no_urut = date("YdHi")."0".$no;

        //response html
        $proId      = $this->clean_all($wunder["proId"]);
        $nama       = $this->clean_post($wunder["nama"]);
        $due        = date("Y-m-d", strtotime($this->clean_post($wunder["expired"])));
        $deskripsi  = $this->clean_post($wunder["deskripsi"]);
        $status     = "PROCESS";

        // print_r($proId);
        // exit();
        
        $query      = $this->query("INSERT INTO tc_wunderlist (TW_NO, TW_PROJECTID, TW_NAMA, TW_DUE, TW_DESKRIPSI, TW_STATUS) VALUES ('$no_urut', '$proId', '$nama', '$due', '$deskripsi', '$status')");
        if($query) {
            echo "<script>alert('Wunderlist berhasil ditambahkan')
                            location.replace('../team/contribution?view=".$proId."')</script>";
        } else {
             echo "<script>alert('Wunderlist gagal ditambahkan')</script>";
        }
    }

    function editWunderlist($id) {
        $query = $this->query("SELECT * FROM tc_wunderlist WHERE TW_NO = '$id'");
        $row   = $query->fetch_assoc();

        return $row;
    }

    function updateWunderlist($list) {
        $bigId      = $_GET["view"];
        $id         = $this->clean_all($list["proId1"]);
        $nama       = $this->clean_post($list["nama1"]);
        $deskripsi  = $this->clean_post($list["deskripsi1"]);
        $expired    = $this->clean_all($list["expired1"]);
        $exp        = date("Y-m-d", strtotime($expired));
        $due        = $this->clean_all($list["due"]);
        $tgl        = date("Y-m-d");
        // $coba       = "UPDATE tc_wunderlist SET TW_NAMA = '$nama', TW_DESKRIPSI = '$deskripsi', TW_DUE = '$exp' WHERE TW_NO = '$id'";
        // echo $coba;
        // exit();
        $query      = $this->query("UPDATE tc_wunderlist SET TW_NAMA = '$nama', TW_DESKRIPSI = '$deskripsi', TW_DUE = '$exp' WHERE TW_NO = '$id'");
        if($query) {
            if($date) :
                $query      = $this->query("UPDATE tc_wunderlist SET TW_DUE = '$due' WHERE TW_NO = '$id'");
            endif;
             echo "<script>alert('Wunderlist berhasil diubah')
                            location.replace('../team/contribution?view=".$bigId."')</script>";
        } else {
             echo "<script>alert('Wunderlist gagal diubah')</script>";
        }
    }

    function deleteWunderlist($id) {
        $bigId = $_GET["view"];
        $query = $this->query("DELETE FROM tc_wunderlist WHERE TW_NO = '$id'");
        if($query) {
            $sql = $this->query("DELETE FROM tc_wunderlist_detail WHERE TWD_WUNDERLISTNO = '$id'");
             echo "<script>alert('Wunderlist berhasil dibuat')
                            location.replace('../team/contribution?view=".$bigId."')</script>";
        } else {
             echo "<script>alert('Wunderlist gagal ditambahkan')</script>";
        }
    }

    function ubahStatusWunderlist($id, $status) {
        $bigId = $_GET["view"];
        $query = $this->query("UPDATE tc_wunderlist SET TW_STATUS = '$status' WHERE TW_NO = '$id'");
        if($query) :
            echo "<script>alert('Status Wunderlist berhasil diubah')
                            location.replace('../contribution?view=".$bigId."')</script>";
        else :
            echo "<script>alert('Wunderlist gagal ditambahkan')</script>";
        endif;
    }

    function getWunderlistDetail($id, $proId) {
        $rows  = array();
        $query = $this->query("SELECT * FROM tc_wunderlist_detail AS A INNER JOIN tc_wunderlist AS B ON A.TWD_WUNDERLISTNO WHERE A.TWD_WUNDERLISTNO = '$id' AND B.TW_PROJECTID = '$proId'");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function addDetailWunderlist($wunder) {
        $no     = $this->clean_all($wunder["no"]);
        $nama   = $this->clean_post($wunder["nama"]);
        $expired= $this->clean_all($wunder["expired"]);
        $exp    = date("Y-m-d", strtotime($expired));
        $check  = '0';
        $status = 'PROCESS';

        $query  =  $this->query("INSERT INTO tc_wunderlist_detail (TWD_WUNDERLISTNO, TWD_NAMA, TWD_DUE, TWD_CHECK,  TWD_STATUS_CHECKIN) VALUES ('$no', '$nama', '$exp', '$check', '$status')");
        if($query) {
            echo "<script>alert('Wunderlist berhasil dibuat')</script>";
        } else {
            echo "<script>alert('Wunderlist gagal ditambahkan')</script>";
        }
    }

    function editDetailWunderlist($id) {
        $query = $this->query("SELECT * FROM tc_wunderlist_detail WHERE TWD_BIGID = '$id'");
        $row   = $query->fetch_assoc();

        return $row;
    }

    function updateDetailWunder($wunder) {
        $id     = $this->clean_all($wunder["id"]);
        $nama   = $this->clean_post($wunder["nama"]);

        $query = $this->query("UPDATE tc_wunderlist_detail SET TWD_NAMA = '$nama' WHERE TWD_BIGID = '$id'");
  
        if($query) {
            echo "<script>alert('Wunderlist berhasil diubah')</script>";
        } else {
            echo "<script>alert('Wunderlist gagal diubah')</script>";
        }
    }

    function ubahStatusCheckin($cek) {
        $bigId = $_GET["view"];
        $key   = $_GET["key"];
        $id = $this->clean_all($cek["wId"]);
        $query = $this->query("UPDATE tc_wunderlist_detail SET TWD_CHECK = '1' WHERE TWD_BIGID = '$id'");

        if($query) {
           echo "<script>alert('Status Wunderlist berhasil diubah')
                            location.replace('../team/change-log?view=".$bigId."&key=".$key."')</script>";
        } else {
            echo "<script>alert('Wunderlist gagal diubah')</script>";
        }
    }

    function deleteWunderlistDetail($id) {
        $bigId = $_GET["view"];
        $key   = $_GET["key"];
        $query = $this->query("DELETE FROM tc_wunderlist_detail WHERE TWD_BIGID = '$id'");

        if($query) {
           echo "<script>alert('Status Wunderlist berhasil dihapus')
                            location.replace('../team/change-log?view=".$bigId."&key=".$key."')</script>";
        } else {
            echo "<script>alert('Wunderlist gagal dihapus')</script>";
        }
    }

    //page client
    function getProjectWhereClient() {
        $id = $_SESSION["U_CLIENTID"];

        $query = $this->query("SELECT TO_BIGID, TP_PROJECTID, TP_CLIENTID FROM tc_order WHERE TP_CLIENTID = '$id' GROUP BY TP_PROJECTID");
        $row   = $query->fetch_assoc();

        return $row;
    }

    function getTicketWhereClient() {
        $id    = $_SESSION["U_CLIENTID"];
        $rows  = array();
        $query = $this->query("SELECT * FROM tc_ticket WHERE TT_CLIENT = '$id' ORDER BY TT_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

     function addTicket($ticket) {
        $projectId = $this->clean_all($ticket["projectId"]);
        $clientId  = $this->clean_all($ticket["clientId"]);
        $judul     = $this->clean_all($ticket["judul"]);
        $subject   = $this->clean_post($ticket["subject"]);
        $status    = "WAITING_CONFIRM";

        $query = $this->query("INSERT INTO tc_ticket (TT_PROJECTID, TT_CLIENTID, TT_TITLE, TT_SUBJECT,  TT_STATUS) VALUES ('$projectId', '$clientId', '$judul', '$subject', '$status')");

        if($query) {
           echo "<script>alert('Ticket Anda berhasil dibuat')
                            location.replace('../ticket')</script>";
        } else {
            echo "<script>alert('Tiket gagal dibuat')</script>";
        }
    }

    //page admin
    function getTicket() {
        $rows  = array();
        $query = $this->query("SELECT * FROM tc_ticket AS A JOIN tc_project AS B ON A.TT_PROJECTID = B.TP_REG_PROJECT JOIN tc_client AS C A.TT_CLIENTID = C.TC_REGID ORDER BY A.TT_CREATED_AT DESC");
        while($row = $query->fetch_assoc()) {
            $rows[] = $row;
        }

        return $rows;
    }

    function ubahStatusTicket($id, $status) {
        $query = $this->query("UPDATE tc_ticket SET TT_STATUS = '$status' WHERE TT_BIGID = '$id'");
        if($query) {
           echo "<script>alert('Status Ticket berhasil diubah')
                            location.replace('../ticket')</script>";
        } else {
            echo "<script>alert('Status Tiket gagal diubah')</script>";
        }
    }

    function get_ticket_by_id($id) {
        $query = $this->query("SELECT * FROM tc_ticket WHERE TT_BIGID = '$id'");
        $row   = $query->fetch_assoc();

        return $row;
    }

    function update_keterangan_ticket($data) {
        $id    = $this->clean_all($_GET["view"]);
        $ket   = $this->clean_post($data["keterangan"]);

        $query = $this->query("UPDATE tc_ticket SET TT_PEMBARUAN WHERE TT_BIGID = '$id'");
        if($query) {
           echo "<script>alert('Status Ticket berhasil diubah')
                            location.replace('../ticket')</script>";
        } else {
            echo "<script>alert('Status Tiket gagal diubah')</script>";
        }
    }

    function get_count_allticket() {
        $query = $this->query("SELECT COUNT(TT_BIGID) AS jmlKt FROM tc_ticket WHERE TT_STATUS = 'WAITING'");
        $row   = $query->fetch_assoc();

        return $row;
    }

}