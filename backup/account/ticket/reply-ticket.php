<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->get_ticket_by_id($_GET["view"]);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">

   <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->    
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Data Ticket</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Data List Ticket</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">        
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">List Ticket Client</h3>
          </div>
              <!-- /.box-header -->
          <div class="box-body">
            <?php 
            if(isset($_POST["update"])) {
              $data["keterangan"] = $_POST["keterangan"];
              $vbg = $log->update_keterangan_ticket($data);
            }
            ?>
             <form action="" class="form-horizontal">
                <h3><?= $ctrl["TT_TITLE"]; ?></h3><br>
                <h6><?= $ctrl["TT_SUBJECT"]; ?></h6>
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Keterangan</label>
                  <textarea name="keterangan" id="editor1" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group col-md-12">
                  <a href="index" class="btn btn-primary"><span class="mdi mdi-arrow-left"></span> Back</a>
                  <a type="submit" name="update" class="btn btn-primary"><span class="mdi mdi-check"></span> Update</a>
                </div>
             </form>             
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->          
      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once('../../layouts/footer.php'); ?>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
    
   
    
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- SlimScroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>

  <!-- CK Editor -->
  <script src="../../assets/vendor_components/ckeditor/ckeditor.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin for editor -->
  <script src="../../assets/js/pages/editor.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 

   <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
</body>
</html>
