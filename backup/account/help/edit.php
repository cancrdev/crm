<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->editHelpId($_GET["view"]);

if(isset($_POST["save"])) {
  $help["title"] = $_POST["title"];
  $help["isi"]   = $_POST["isi"];
  $ctrl = $log->insertHelp($help);
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <!-- <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css"> -->


  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Edit Data Help/Bantuan</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Edit Master Help / Bantuan</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">        
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Data master Help / Bantuan</h3>
            <!-- <a href="" class="btn btn-info mb-5"><i class="mdi mdi-account-multiple-plus"></i></a> -->
          </div>
              <!-- /.box-header -->
          <div class="box-body">
             <form method="post">
                <div class="form-group col-md-12">
                  <label for="exampleInputEmail1">Judul Help</label>
                  <input type="text" required class="form-control" name="title" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Judul Help" value="<?= $ctrl["TH_TITTLE"]; ?>">
                </div>
                <div class="form-group col-md-12">
                  <textarea id="editor1" name="isi" rows="10" cols="80"><?= $ctrl["TH_ISI"]; ?></textarea>
                </div>
                <div class="modal-footer">
                  <a href="../help" class="btn btn-default">Close</a>
                  <input type="submit" name="save" class="btn btn-info" placeholder="Simpan">
                  <!-- <b name="save" class="btn btn-primary">Simpan</button> -->
                </div>
              </form>              
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->         
      </div>
      </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <!-- <aside class="control-sidebar control-sidebar-light"> -->
    
  <!-- <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>   -->
    <!-- Create the tabs -->
    <!-- <ul class="nav nav-tabs control-sidebar-tabs"> -->
      <!-- <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li> -->
      <!-- <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li> -->
    <!-- </ul> -->
    <!-- Tab panes -->
  <!-- </aside> -->
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- SlimScroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- CK Editor -->
  <script src="../../assets/vendor_components/ckeditor/ckeditor.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin for editor -->
  <script src="../../assets/js/pages/editor.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 

   <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  


  
</body>
</html>
