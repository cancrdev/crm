<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="../../assets/css/master_style.css">
	
	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
	
	<!-- fullCalendar -->
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
	
	<!-- Data Table-->
	<link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
	
	<!-- Bootstrap switch-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
	
	<!-- Morris charts -->
	<link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto w-p50">
				<h3 class="page-title">CRM - (Customer Relationship Management)</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item active" aria-current="page">Selamat datang di Dashboard Account</li>
						</ol>
					</nav>
				</div>
			</div>
			<div class="right-title text-right w-170">
				<span class="subheader_daterange font-weight-600" id="dashboard_daterangepicker">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title"></span>
						<span class="subheader_daterange-date text-primary"></span>
					</span>
					<a href="#" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content">		
	  	<div class="row">
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-info mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-account-circle font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getProduksi()); ?></div>
					<span>Total User Produksi</span>
				  </div>
				</div>
			</div>
			<!-- /.col -->
			<!-- /.col -->
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-success mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-thumb-up-outline font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getClient()); ?></div>
					<span>Total Client</span>
				  </div>
				</div>
			</div>
			<!-- /.col -->
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-danger mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getAllProject()); ?></div>
					<span>Total Project</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-danger mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getProjectMasuk()); ?></div>
					<span>Total Project Baru</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-primary mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getWaitingAnalis()); ?></div>
					<span>Project Menunggu Analisis</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-warning mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getFollowup()); ?></div>
					<span>Total Project Followup</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-danger mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getHotProspek()); ?></div>
					<span>Total Project Hot Prospek</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-success mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getDealProject()); ?></div>
					<span>Total Project Siap Produksi(Deal)</span>
				  </div>
				</div>
			</div>
			<!-- /.col -->
			<div class="col-12 col-lg-4">
				<div class="box">
				  <div class="box-header with-border">
					<h3 class="box-title">Tickets share per category</h3>
					<div class="box-tools pull-right">
						<ul class="card-controls">
						  <li class="dropdown">
							<a data-toggle="dropdown" href="#"><i class="ion-android-more-vertical"></i></a>
							<div class="dropdown-menu dropdown-menu-right">
							  <a class="dropdown-item active" href="#">Today</a>
							  <a class="dropdown-item" href="#">Yesterday</a>
							  <a class="dropdown-item" href="#">Last week</a>
							  <a class="dropdown-item" href="#">Last month</a>
							</div>
						  </li>
						  <li><a href="" class="link card-btn-reload" data-toggle="tooltip" title="" data-original-title="Refresh"><i class="mdi mdi-toggle-switch-off-thin"></i></a></li>
						</ul>
					</div>
				  </div>

				  <div class="box-body">
					<div class="text-center py-20">                  
					  <div class="donut" data-peity='{ "fill": ["#fc4b6c", "#ffb22b", "#398bf7"], "radius": 120, "innerRadius": 80  }' >9,6,5</div>
					</div>

					<ul class="list-inline">
					  <li class="flexbox mb-5">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Technical</span>
						</div>
						<div >8952</div>
					  </li>

					  <li class="flexbox mb-5">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Accounts</span>
						</div>
						<div>7458</div>
					  </li>

					  <li class="flexbox">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Other</span>
						</div>
						<div>3254</div>
					  </li>
					</ul>
				  </div>
				</div>
			</div>
			<!-- /.col -->
			<div class="col-12 col-lg-4">
				<div class="box">
				  <div class="box-header with-border">
					<h3 class="box-title">Tickets share per agent</h3>

					<div class="box-tools pull-right">
						<ul class="card-controls">
						  <li class="dropdown">
							<a data-toggle="dropdown" href="#"><i class="ion-android-more-vertical"></i></a>
							<div class="dropdown-menu dropdown-menu-right">
							  <a class="dropdown-item active" href="#">Today</a>
							  <a class="dropdown-item" href="#">Yesterday</a>
							  <a class="dropdown-item" href="#">Last week</a>
							  <a class="dropdown-item" href="#">Last month</a>
							</div>
						  </li>
						  <li><a href="" class="link card-btn-reload" data-toggle="tooltip" title="" data-original-title="Refresh"><i class="mdi mdi-toggle-switch-off-thin"></i></a></li>
						</ul>
					</div>
				  </div>

				  <div class="box-body">
					<div class="flexbox mt-70">
						<div class="bar" data-peity='{ "fill": ["#666EE8", "#1E9FF2", "#28D094", "#FF4961", "#FF9149"], "height": 298, "width": 250, "padding":0.2 }'>952,558,1254,427,784</div>
					  <ul class="list-inline align-self-end text-muted text-right mb-0">
						<li>Jacob <span class="badge badge-primary ml-2">952</span></li>
						<li>William <span class="badge badge-info ml-2">558</span></li>
						<li>Emily <span class="badge badge-success ml-2">1254</span></li>
						<li>Chloe <span class="badge badge-danger ml-2">427</span></li>
						<li>Daniel <span class="badge badge-warning ml-2">784</span></li>
					  </ul>
					</div>

				  </div>
				</div>
			</div>
			<!-- /.col -->			
			<div class="col-lg-4 col-12">		  
			  <!-- AREA CHART -->
			  <div class="box">
				<div class="box-header with-border">
					<h4 class="box-title">Ticket Overview</h4>
				</div>
				<div class="box-body">
					<ul class="list-inline text-right mt-10 mr-10">
						<li>
							<h5 class="mb-0"><i class="mdi mdi-toggle-switch-off mr-5 text-info"></i>Total</h5>
						</li>
						<li>
							<h5 class="mb-0"><i class="mdi mdi-toggle-switch-off mr-5 text-danger"></i>Close</h5>
						</li>
					</ul>
				  <div id="morris-area-chart2" style="height: 330px;"></div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->		      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	
	
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
	  
	<div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	 
	  
	<!-- jQuery 3 -->
	<script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- date-range-picker -->
	<script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
	
	<!-- Slimscroll -->
	<script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- peity -->
	<script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
	
	<!-- Morris.js charts -->
	<script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
	<script src="../../assets/vendor_components/morris.js/morris.min.js"></script>
	
	<!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
	
	<!-- Bootstrap WYSIHTML5 -->
	<script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
	
	<!-- Superieur Admin App -->
	<script src="../../assets/js/template.js"></script>
	
	<!-- Superieur Admin for demo purposes -->
	<script src="../../assets/js/demo.js"></script>	
	
	<!-- Superieur Admin dashboard demo-->
	<script src="../../assets/js/pages/dashboard6.js"></script>
	
	<!-- Superieur Admin for Data Table -->
	<script src="../../assets/js/pages/data-table.js"></script>	
	
</body>
</html>
