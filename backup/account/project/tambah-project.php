<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

if(isset($_POST["order"])) {
  $offline["nama"]      = $_POST["nama"];
  $offline["noTelpon"]  = $_POST["noTelpon"];
  $offline["email"]     = $_POST["email"];
  $offline["instansi"]  = $_POST["instansi"];
  $offline["alamat"]    = $_POST["alamat"];
  $offline["projectMasuk"] = $_POST["projectMasuk"];
  $offline["subject"]   = $_POST["subject"];
  $offline["platform"]  = $_POST["platform"];
  $offline["deskripsi"] = $_POST["deskripsi"];
  $offline["berkasOrder"] = $_FILES["berkasOrder"];
  $offline["projectMasuk"] = $_POST["projectMasuk"];
  $ctrl = $log->tambahProjectOffline($offline);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
   <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Tambah Project Offline</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Tambah Project Offline</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box">
        <div class="box-header with-border">
          <h4 class="box-title">Tambah Project Offline</h4>
          <!-- <h6 class="box-subtitle">Bootstrap Form Validation check the <a class="text-warning" href="http://reactiveraven.github.io/jqBootstrapValidation/">official website </a></h6> -->
      
          <ul class="box-controls pull-right">
            <li><a class="box-btn-close" href="#"></a></li>
            <li><a class="box-btn-slide" href="#"></a></li> 
            <li><a class="box-btn-fullscreen" href="#"></a></li>
          </ul>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col">
                  <form novalidate method="post" enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-12">            
                      <div class="form-group">
                        <h5>Masukkan Nama Client<span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="text" name="nama" class="form-control" required data-validation-required-message="This field is required"> </div>
                        <!-- <div class="form-control-feedback"><small>Add <code>required</code> attribute to field for required validation.</small></div> -->
                      </div>
                      
                      <div class="form-group">
                        <h5>Masukkan No Telpon Client<span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="text" name="noTelpon" class="form-control" required data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="No Characters Allowed, Only Numbers"> </div>
                      </div>
                      <div class="form-group">
                        <h5>Masukkan Email Client <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="email" name="email" class="form-control" placeholder="Email Address" data-validation-regex-regex="([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})" data-validation-regex-message="Enter Valid Email"> </div>
                      </div>
                      <div class="form-group">
                        <h5>Masukkan Instansi <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="text" name="instansi" class="form-control">
                      </div><br>
                      <div class="form-group">
                        <h5>Alamat <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <textarea name="alamat" id="textarea" class="form-control" required placeholder="Textarea text"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Project Masuk <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="text" name="projectMasuk" class="form-control">
                      </div><br>
                      <div class="form-group">
                        <h5>Subject <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="text" name="subject" class="form-control">
                      </div><br>

                      <div class="form-group">
                          <h5>Pilih Platform sesuai aplikasi anda <span class="text-danger">*</span></h5>
                          <div class="controls">
                            <fieldset>
                              <input type="checkbox" name="platform[]" id="checkbox_2" required value="Web">
                              <label for="checkbox_2">Aplikasi Web</label>
                            </fieldset>
                            <fieldset>
                              <input type="checkbox" name="platform[]" id="checkbox_3" value="Android">
                              <label for="checkbox_3">Aplikasi Android</label>
                            </fieldset>
                            <fieldset>
                              <input type="checkbox" name="platform[]" id="checkbox_4" value="IOS">
                              <label for="checkbox_4">Aplikasi IOS</label>
                            </fieldset>
                          </div>
                      </div>
                       <div class="form-group">
                        <h5>Deskripsi <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <textarea name="deskripsi" id="textarea" class="form-control" required placeholder="Textarea text"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>File Pendukung <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <input type="file" name="berkasOrder[]" class="form-control" multiple > </div>
                      </div>
                    </div>
                  <div class="text-xs-right">
                    <input type="submit" name="order" class="btn btn-info" value="Simpan">
                    <!-- <button type="submit" name="offline" class="btn btn-info">Submit</button> -->
                  </div>
                </form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- date-range-picker -->
  <script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Morris.js charts -->
  <script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
  <script src="../../assets/vendor_components/morris.js/morris.min.js"></script>
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <script src="../../assets/js/pages/dashboard6.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 
  
</body>
</html>
