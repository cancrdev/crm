<?php
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getDetailFile($_GET["view"]);

$cl = $log->getDetailProject($_GET["view"]);

$pnw = $log->getDetailPenawaran($_GET["view"]);

$buff = $log->detailPenawaran($_GET["view"]);

$clog = $log->getLog($_GET["view"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM </title>
  
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">

    <!-- bootstrap slider -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-slider/slider.css">

    <!-- Timeline CSS -->
    <link href="../../assets/vendor_components/horizontal-timeline/css/horizontal-timeline.css" rel="stylesheet">
   
    <!-- Popup CSS -->
    <link href="../../assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">

  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    .alias {
      position: relative;
      z-index: 1;
      top: 0px;
    }
    .text-alias {
      position: absolute;
      top: 60px;
      z-index: 2;
      color: #000;
      text-align: center;
    }
  </style>
  
</head>

<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>
  
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title">Detail File Pendukung</h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
              <!-- <li class="breadcrumb-item" aria-current="page">Dashboard</li> -->
              <li class="breadcrumb-item active" aria-current="page">Detail File & Penawaran pada Project <?= $cl["TP_NAMA_PROJECT"]." | No Project : ". $cl["TP_NO_PROJECT"]; ?></li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
    <!-- Main content -->
    <section class="content">
     
      <!-- START Card With Image -->
      <h4 class="box-title mb-10">Detail File pendukung Project No. : <?= $cl["TP_NO_PROJECT"]; ?></h4><br>
        <!-- /.col -->
      <!-- START Card With Image -->
      <div class="row fx-element-overlay">
        <?php if(is_array($ctrl) || is_object($ctrl)) { 
          foreach($ctrl as $cc) {
        ?>
        <div class="col-md-12 col-lg-3">
          <div class="box box-default">
            <div class="fx-card-item">
              <div class="fx-card-avatar fx-overlay-1"> 
                <?php $ext = array("bmp", "jpg", "png");
                      $fileName = $cc["TO_BUKTI_PENDUKUNG"];
                      $pecah = explode(".", $fileName);
                      $ekstensi = $pecah[1];
                      if(in_array($ekstensi, $ext)) {
                 ?>
                <img src="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>" alt="data pendukung">
                <div class="fx-overlay scrl-dwn">
                  <ul class="fx-info">
                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>"><i class="ion-search"></i></a></li>
                    <li><a class="btn default btn-outline" href="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>" download><i class="ion-link"></i></a></li>
                  </ul>
                </div>
                 <?php } else { ?>
                <div>
                  <iframe style="font-size: auto" src="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>"></iframe><br><br>
                  <a class="btn btn-success" target="_blank" href="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>"><i class="mdi mdi-desktop-mac"></i> Lihat File</a>
                </div>
                <?php } ?>
              </div>
              <div class="fx-card-content">
                <h4 class="box-title">Project No : <?= $cc["TP_NO_PROJECT"]; ?></h4> <small><?= $log->tanggalIndo($cc["TP_CREATED_AT"]); ?></small>
                <br> </div>
              </div>
          </div>
          <!-- /.box -->
        </div>
        <?php } } ?>
      </div><br>
      <h4>Detail Penawaran Pada Project No <?= $cc["TP_NO_PROJECT"]; ?></h4>
      <div class="row fx-element-overlay">
        <?php if(is_array($pnw) || is_object($pnw)) { 
          foreach($pnw as $ai) {
        ?>
        <div class="col-md-12 col-lg-3">
          <div class="box box-default">
            <div class="fx-card-item">
              <div class="fx-card-avatar fx-overlay-1"> 
                <?php $ext = array("bmp", "jpg", "png");
                      $pdf = array("pdf");
                      $fileName = $ai["TPN_BERKAS"];
                      $pecah = explode(".", $fileName);
                      $ekstensi = $pecah[1];
                      if(in_array($ekstensi, $ext)) {
                 ?>
                <img src="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>" alt="user">
                <div class="fx-overlay scrl-dwn">
                  <ul class="fx-info">
                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>"><i class="ion-search"></i></a></li>
                    <li><a class="btn default btn-outline" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>" download><i class="ion-link"></i></a></li>
                  </ul>
                </div>
                <?php } elseif(in_array($ekstensi, $pdf)) { ?>
                <div>
                  <iframe style="font-size: auto" src="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>"></iframe><br><br>
                  <a class="btn btn-success" target="_blank" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>"><i class="mdi mdi-desktop-mac"></i> Lihat File</a>
                </div>
                <?php } else { ?>
                <img src="../../assets/images/gallery/thumb/12.jpg" alt="aliases" class="alias"><h5 class="text-alias">File bisa berisi .doc,.csv,.xls dll</h5><br>
                <a class="btn btn-info" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>" download><i class="mdi mdi-download"></i> Download</a>
                <?php } ?>
              </div>
              <div class="fx-card-content">
                <h4 class="box-title">Penawaran No. : <?= $ai["TPN_NO_PENAWARAN"];; ?></h4> <small><?= $log->tanggalIndo($ai["TP_CREATED_AT"]); ?></small>
                <br> </div>
              </div>
          </div>
          <!-- /.box -->
        </div>
        <?php } } ?>
      </div>
      <div>
        <a onclick="goBack()" class="btn btn-primary" style="color: #fff;"><i class="mdi mdi-arrow-left"></i> Kembali</a>
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-center">
          <i class="mdi mdi-history"></i> Lihat Change Log
        </button>
      </div><br><br>
      <?php if($cl["TP_STATUS"] === "PROJECT_FOLLOWUP") { ?>
      <div class="row">
        <div class="col-md-12">
          <?php 
          if (isset($_POST["kirim"])) {
            # code...\
            $logs["penId"] = $_POST["penId"];
            $logs["proId"] = $_POST["proId"];
            $logs["keterangan"] = $_POST["keterangan"];
            $bn = $log->updateLog($logs);
          }
          ?>
          <form method="post">
            <input type="hidden" name="proId" value="<?= $cl["TP_PROJECTID"]; ?>">
            <input type="hidden" name="penId" value="<?= $buff["TPN_BIGID"]; ?>">
            <div class="form-group">
              <textarea class="form-control" name="keterangan" placeholder="Isikan Keterangan Followup"></textarea>
            </div>
            <div class="form-group">
              <button type="submit" name="kirim" class="btn btn-success">Kirim <i class="mdi mdi-send"></i></button>
            </div>
          </form>
        </div>
      </div>
      <?php } ?>
        <!-- /.col -->
      <!-- END Card with image -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   <?php include_once('../../layouts/footer.php'); ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-danger"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                <p>Will be July 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-warning"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                <p>New Email : jhone_doe@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-info"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                <p>disha@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-success"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Code Change</h4>

                <p>Execution time 15 Days</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Web Design
                <span class="label label-danger pull-right">40%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Data
                <span class="label label-success pull-right">75%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Order Process
                <span class="label label-warning pull-right">89%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Development 
                <span class="label label-primary pull-right">72%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

 <div class="modal center-modal fade" id="modal-center" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title"><i class="mdi mdi-history"></i> List Change Log</h5>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body"><br><br>
              <br><h2>Change Log Project <?php echo $cl["TP_NAMA_PROJECT"]." | No Project : ". $cl["TP_NO_PROJECT"]; ?></h2>
              <?php if(is_array($clog)) {
                foreach($clog as $ad) {
               ?>
              <section class="cd-horizontal-timeline">
                  <div class="timeline">
                    <div class="events-wrapper">
                      <div class="events">
                        <ol>
                          <li><a href="#0" data-date="<?php echo date('d/m/Y', strtotime($ad['TC_CREATED_AT'])) ?>" class="selected"><?= date("d M Y", strtotime($ad["TC_CREATED_AT"])) ?></a></li>
                        </ol> <span class="filling-line" aria-hidden="true"></span> </div>
                      <!-- .events -->
                      </div>
                      <!-- .events-wrapper -->
                      <ul class="cd-timeline-navigation">
                        <li><a href="#0" class="prev inactive">Prev</a></li>
                        <li><a href="#0" class="next">Next</a></li>
                      </ul>
                    <!-- .cd-timeline-navigation -->
                    </div>
                  <!-- .timeline -->
                    <div class="events-content">
                      <ol>
                        <li class="selected" data-date="<?php echo date('d/m/Y', strtotime($ad['TC_CREATED_AT'])) ?>">
                          Change Log Project <br/><?= $log->TanggalIndo($ad["TC_CREATED_AT"])."  ".date("H:i:s", strtotime($ad["TC_CREATED_AT"])); ?></h2>
                          <p class="mt-40">
                            <?= $ad["TC_KETERANGAN"]; ?>
                          </p>
                        </li>
                      </ol>
                  </div>
                <!-- .events-content -->
              </section>
              <?php }} ?>
          </div>
          <div class="modal-footer">
            <a class="btn btn-primary" data-dismiss="modal" style="color: #fff;"><i class="mdi mdi-arrow-left"></i> Close</a>
            <!-- <input type="submit" name="clientUp" class="btn btn-info" placeholder="Simpan"> -->
            <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
          </div>
        </div>
      </div>
 </div>
<!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- SlimScroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>

  <!-- Horizontal-timeline JavaScript -->
    <script src="../../assets/vendor_components/horizontal-timeline/js/horizontal-timeline.js"></script>
  
  <!-- Magnific popup JavaScript -->
    <script src="../../assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="../../assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>


  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script>

  <script type="text/javascript">
    function goBack() {
        window.history.back();
    }
  </script>
  
  
</body>
</html>
