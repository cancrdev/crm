<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getDetailProject($_GET["view"]);
$prd  = $log->getProduksi();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/favicon.ico">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">

  <!-- owlcarousel-->
  <link rel="stylesheet" href="../../assets/vendor_components/OwlCarousel2/dist/assets/owl.carousel.css">
  <link rel="stylesheet" href="../../assets/vendor_components/OwlCarousel2/dist/assets/owl.theme.default.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Detail Order</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Detail Order dengan Project No <?= $ctrl["TP_NO_PROJECT"]." - ".$ctrl["TP_NAMA_PROJECT"]; ?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
    
    <div class="row">
      <div class="col-md-5">
        <div class="box card-inverse bg-img text-center py-80" style="background-image: url(../../images/gallery/thumb/12.jpg)" data-overlay="5">
          <div class="card-body">
            <span class="bb-1 opacity-80 pb-2">Project Masuk : <?= $log->TanggalIndo($ctrl["TP_PROJECT_TGL"]); ?></span>
            <br><br>
            <span class="bb-1 opacity-80 pb-2"><h3>Detail Client / Customer</h3></span>
            <h4><?= $ctrl["TC_NAMA"]; ?></h4>
            <h6><?= $ctrl["TC_EMAIL"]; ?></h6>
            <h6><?= $ctrl["TC_TELPON"]; ?></h6>
            <br>
            <h5><?= $ctrl["TC_INSTANSI"]; ?></h5><br>
            <h6><?= $ctrl["TC_ALAMAT"]; ?></h6>
            <?php if($ctrl["TC_INSTANSI"] === "" || $ctrl["TC_INSTANSI"] === NULL) { ?>
            <!-- <a href="" data-toggle="modal" data-target="#modal-fill" class="btn btn-info"><i class="mdi mdi-account-multiple-plus"></i></a> -->
             <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-center">
                <i class="mdi mdi-account-multiple-plus"></i>
              </button>
            <?php } ?>
          </div>
        </div>
      </div>
      
      <div class="col-md-7 col-12">       
        <div class="box">
          <div class="box-body p-0">
            <div class="media">
            <!-- <a class="pull-left" href="#">
              <img class="media-object w-200" src="../../assets/images/avatar/gambar/<?= $ctrl["TO_BUKTI_PENDUKUNG"]; ?>" alt="">
            </a> -->
            <div class="media-body">
              <span class="bb-1 opacity-80 pb-2"><h4 class="media-heading mb-15"><a href="#"><?= $ctrl["TP_NAMA_PROJECT"]; ?></h4></span><br>
              <h6>Platform : <?= $ctrl["TP_PLATFORM"]." |  ".$log->TanggalIndo($ctrl["TP_PROJECT_TGL"]); ?></h6><br>
              <?php if($ctrl["TO_BUKTI_PENDUKUNG"] == "" && $ctrl["TO_BUKTI_PENDUKUNG"] == NULL) { ?>
              <a href="" class="btn btn-info data-kosong"><i class="fa fa-file"></i> Lihat Detail File</a>
              <?php } else { ?>
                <a href="detail-file?view=<?= $ctrl["TP_PROJECTID"]; ?>" class="btn btn-info"><i class="fa fa-file"></i> Lihat Detail File</a>
              <?php } ?>
             </div>
          </div>          
          <div class="col-12">
            <div class="box">

              <div class="row no-gutters">
                <div class="col-md-8">
                  <div class="box-body">
                    <h4 align="center">No Project : <a href="#"><?= $ctrl["TP_NO_PROJECT"]; ?></a></h4>

                    <p><?= $ctrl["TP_DESKRIPSI"]; ?></p>
                  </div>
                </div>
                <!-- <div class="col-4 bg-img d-none d-md-block" style="background-image: url(../../assets/images/avatar/gambar/<?= $ctrl["TO_BUKTI_PENDUKUNG"]; ?>)"></div> -->
              </div><br><br><br>

              <!-- <form action="coba" method="post"> -->
                <?php if($ctrl["TP_STATUS"] === "ORDER_MASUK") { ?>
                <p style="color: red;">(*) Ubah status siap dianalisa jika sudah sesuai.</p>
                <!-- <input type="hidden" name="id" value="<?= $ctrl["TP_PROJECTID"]; ?>"> -->
                <!-- <input type="hidden" name="status" value="WAITING_ANALIS"> -->
                <div class="flexbox align-items-center mt-3">
                  <a href="status/ubah-status?v=<?= $ctrl["TP_PROJECTID"] ?>" class="btn btn-primary ubah-status" style="color: #fff;"><i class="mdi mdi-bookmark-check"></i> Ubah Status Siap Analisa</a>
                  <!-- <button name="ubah" class="btn btn-xs btn-bold btn-primary ubah-status" style="color: #fff;">Ubah Status Siap Analisa</button> -->
                </div>
                <?php } elseif($ctrl["TP_STATUS"] === "WAITING_ANALIS") { ?>
                <h4 align="center">Tambah File Penawaran</h4><br>
                <form action="status/update-analis" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="projectId" value="<?= $ctrl["TP_PROJECTID"]; ?>">
                  <div class="form-group">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text bg-info border-info"><i class="mdi mdi-file-check"></i></span>
                      </div>
                      <input type="file" name="pendukung[]" id="pendukung" required class="form-control pl-15">
                    </div>
                  </div>
                  <div class="form-group">
                      <input type="button" id="more_fields" onclick="add_foto();" value="Add More" class="btn btn-primary waves-effect" />
                  </div>
                  <div id="foto_fields">
                  </div><br>
                  <div align="center">
                    <button name="analis" class="btn btn-info"><span class="fa fa-send"></span> Kirim Data</button>
                  </div>
                </form>
                <?php } elseif($ctrl["TP_STATUS"] === "READY_FOLLOWUP") { ?>
                <?php } elseif($ctrl["TP_STATUS"] === "PROJECT_DEAL") { ?>
                <div>
                  
                  <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-center1">
                    <i class="mdi mdi-account-multiple-plus"></i> Buat Akun Client
                  </button>
                  
                  <?php if($ctrl["TP_LEADER"] === NULL) { ?>
                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-center2">
                    <i class="mdi mdi-account-check"></i> Buat Leader Project
                  </button>
                  <?php } else { ?>
                  <button type="button" disabled class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-center2">
                    <i class="mdi mdi-account-check"></i> Buat Leader Project
                  </button>
                  <?php } ?>
                   
                   <?php if($ctrl["TP_BUKTI_PEMBAYARAN"] === NULL) { ?>
                  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-center3">
                    <i class="mdi mdi-file-cloud"></i> Update Berkas
                  </button>
                  <?php } else { ?>
                  <button type="button" disabled class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-center3">
                    <i class="mdi mdi-file-cloud"></i> Update Berkas
                  </button>
                  <?php } ?>
                </div>
                <?php } elseif($ctrl["TP_STATUS"] === "HOT_PROSPEK") { ?>
                <?php } ?>
            </div>
          </div>
        </div>
    </section>
    
    <!-- /.content -->
  </div>
   <div class="modal center-modal fade" id="modal-center" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Update Data Client</h5>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
            <?php
            if(isset($_POST["clientUp"])) {
              $cc["clientId"] = $_POST["clientId"];
              $cc["instansi"] = $_POST["instansi"];
              $cc["alamat"]   = $_POST["alamat"];
              $ll = $log->upClient($cc);
            }?>
            <form method="post">
              <input type="hidden" name="clientId" value="<?= $ctrl["TC_REGID"]; ?>">
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Nama Instansi</label>
                <input type="text" required class="form-control" name="instansi" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Instansi">
              </div>
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Alamat</label>
                <textarea class="form-control" name="alamat"></textarea>
              </div>
            
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="submit" name="clientUp" class="btn btn-info" placeholder="Simpan">
                <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal center-modal fade" id="modal-center1" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Buat Data Akun Client</h5>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
            <?php
            $gh = $log->getLock($ctrl["TC_REGID"]);
            if(isset($_POST["clientAkun"])) {
              $akun["clientId"] = $_POST["clientId"];
              $akun["username"] = $_POST["username"];
              $akun["fullname"] = $_POST["fullname"];
              $akun["email"]    = $_POST["email"];
              $akun["noTelpon"] = $_POST["noTelpon"];
              $akun["alamat"]   = $_POST["alamat"];
              $bc = $log->tambahClient($akun);
            }?>
            <form method="post">
              <input type="hidden" name="clientId" value="<?= $ctrl["TC_REGID"]; ?>">
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Usename</label>
                <input type="text" required class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username" value="<?= $gh["U_NAME"]; ?>" required>
              </div>
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Nama Lengkap</label>
                <input type="text" required class="form-control" name="fullname" placeholder="Nama Lengkap" value="<?= $ctrl["TC_NAMA"]; ?>">
              </div>
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Email</label>
                <input type="text" required class="form-control" name="email" placeholder="Email" value="<?= $ctrl["TC_EMAIL"]; ?>">
              </div>
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">No Telpon</label>
                <input type="text" required class="form-control" name="noTelpon" placeholder="No Telp" value="<?= $ctrl["TC_TELPON"]; ?>">
              </div>
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Alamat</label>
                <textarea class="form-control" name="alamat"><?= $ctrl["TC_ALAMAT"]; ?></textarea>
              </div>
            
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="submit" name="clientAkun" class="btn btn-info" placeholder="Simpan">
                <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal center-modal fade" id="modal-center2" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Buat Leader Project <?= $ctrl["TP_NO_PROJECT"] ." - ".$ctrl["TP_NAMA_PROJECT"]; ?></h5>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
            <?php
            if(isset($_POST["leader1"])) {
              if($_POST["leader"] == "-- Pilih Produksi --") { ?>
                <script type="text/javascript">swal("Oops...", "Anda Belum mimilih leader Project :)", "error");</script>
             <?php } else {
                $leaderId["projectId"] = $_POST["projectId"];
                $leaderId["leader"] = $_POST["leader"];
                $tt = $log->updateLeader($leaderId);
              }
            }?>
            <form method="post">
              <input type="hidden" name="projectId" value="<?= $ctrl["TP_REG_PROJECT"]; ?>">
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Nama Produksi</label>
                <select class="form-control" name="leader">
                  <option>-- Pilih Produksi --</option>
                  <?php
                  if(is_array($prd)) {
                  foreach($prd as $pp) {
                  ?>
                  <option value="<?= $pp["U_REGID"]; ?>"><?= $pp["U_FULLNAME"]; ?></option>
                  <?php }} ?>
                </select>
              </div>
            
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="submit" name="leader1" class="btn btn-info" placeholder="Simpan">
                <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal center-modal fade" id="modal-center3" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title">Update Berkas Pendukung</h5>
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>
          <div class="modal-body">
            <?php
            if(isset($_POST["berkasUp"])) {
              $berkas["projectId1"] = $_POST["projectId1"];
              $berkas["mou"] = $_FILES["mou"];
              $berkas["projectDetail"]   = $_FILES["projectDetail"];
              $berkas["buktiPembayaran"] = $_FILES["buktiPembayaran"];
              $berkas["invoice"]     = $_FILES["invoice"];
              $berkas["kwitansi"]    = $_FILES["kwitansi"];
              $ll = $log->updateBerkas($berkas);
            }?>
            <form method="post">
              <input type="hidden" name="projectId1" value="<?= $ctrl["TP_REG_PROJECT"]; ?>">
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Nama Project</label>
                <input type="text" required class="form-control" name="instansi" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?=  $ctrl["TP_NAMA_PROJECT"]; ?>">
              </div>
              <div class="form-group col-md-12">
                <label for="exampleInputEmail1">MOU</label>
                <input type="file" required class="form-control" name="mou" required>
              </div>
               <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Project Detail</label>
                <input type="file" required class="form-control" name="projectDetail" required>
              </div>
               <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Bukti Pembayaran</label>
                <input type="file" required class="form-control" name="buktiPembayaran">
              </div>
               <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Invoice</label>
                <input type="file" required class="form-control" name="invoice">
              </div>
               <div class="form-group col-md-12">
                <label for="exampleInputEmail1">Kwitansi</label>
                <input type="file" required class="form-control" name="kwitansi">
              </div>
              <input type="date" name="">
              <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="submit" name="berkasUp" class="btn btn-info" placeholder="Simpan">
                <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- date-range-picker -->
  <script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <script type="text/javascript">
      function add_foto() {
          var objTo = document.getElementById('foto_fields')
          var divtest = document.createElement("div");  
          divtest.innerHTML = "<div class='form-group'><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group'></span></div></div><input type='file' name='pendukung[]' id='pendukung' class='form-control'></div>";    
          objTo.appendChild(divtest)
      }
  </script>

  <script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-status').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin mengubah status?",
            text: "Status akan otomatis berubah",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Update it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>

<script type="text/javascript">
  jQuery(document).ready(function($){
        $('.data-kosong').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Oops....",
            text: "Data detail kosong :(",
            type: "error",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
</body>
</html>


