<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../assets/images/gallery/full/deals1.png">

    <title>CRM - Customer Relationship Management</title>
  
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../assets/css/bootstrap-extend.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/master_style.css">

  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition bg-img" style="background-image: url(../assets/images/gallery/full/deals4.jpg); width: 97%; background-size: 165%" data-overlay="4">
  <?php 
  include_once('../stucture/fungsi.php');
  $log = new Model();

  if(isset($_POST['g-recaptcha-response']) && isset($_POST['order'])) {
    $captcha = $_POST['g-recaptcha-response'];
    if (!$captcha) { ?>
      <script type="text/javascript">swal("Oops...", "Tolong cek akses anda dengan reCAPTCHA box. :)", "error");</script>
   <?php } else {
      $secret = '6LeN4oQUAAAAAKo_xuQTZ-yNszg7_pdR8U1G0nGR';
      $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
      if ($response != true) { ?>
        <script type="text/javascript">swal("Oops...", "lengkapi parameter anda :(", "error");</script>
     <?php } else {
        $order["nama"]      = $_POST["nama"];
        $order["email"]     = $_POST["email"];
        $order["noTelpon"]  = $_POST["noTelpon"];
        $order["subject"]   = $_POST["subject"];
        $order["platform"]  = $_POST["platform"];
        $order["deskripsi"] = $_POST["deskripsi"];
        $order["pendukung"] = $_FILES["pendukung"];
        $ctrl = $log->orderApp($order);
      }
    }
  } ?>
  <div class="container">
    <div class="row align-items-center justify-content-md-center">
      <div class="col-12"><br>
        <a href="http://can.co.id" class="btn"><img src="../assets/images/logo/logo.png" alt="can creative" width="75px;"></a><br><br>
        <div class="row no-gutters justify-content-md-center">
          <div class="col-lg-4 col-md-5 col-12">
            <div class="content-top-agile h-p100">
              <h2>Form Order</h2>
              <div class="text-center text-white">
                <p class="mt-20">- Hubungi Kami Secara Langsung -</p>
                <p class="gap-items-2 mb-20">
                  <a class="btn btn-social-icon btn-outline btn-white wt-wp" href="https://api.whatsapp.com/send?phone=6282220128072&amp;text=Pesan%20ini%20di%20kirim%20dari%20Website%20CAN%20Creative" target="_blank"><i class="mdi mdi-whatsapp"></i></a>
                  <a class="btn btn-social-icon btn-outline btn-white" href="tel:+6282220128072"><i class="mdi mdi-phone"></i></a>
                  <a class="btn btn-social-icon btn-outline btn-white" href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to=dika@can.web.id&tf=1" target="_blank"><i class="mdi mdi-contact-mail"></i></a>
                  <a class="btn btn-social-icon btn-outline btn-white" target="_blank" href="https://www.google.com/maps/place/CAN+Creative+%7C+Jasa+Pembuatan+Web+dan+Aplikasi+Mobile+Android+IOS/@-6.9825902,110.3666202,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708b4a573dd455:0xf275fb4e80f1afb5!8m2!3d-6.9825902!4d110.3688089"><i class="mdi mdi-map-marker-radius"></i></a>
                </p>  
              </div>
              
            </div>        
          </div>
          <div class="col-lg-6 col-md-5 col-12">
            <div class="p-40 bg-white1 content-bottom">
              <form method="post" action="" class="form-element" enctype="multipart/form-data">
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info"><i class="ti-user"></i></span>
                    </div>
                    <input type="text" name="nama" class="form-control pl-15" placeholder="Masukkan Nama Lengkap Anda">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info"><i class="ti-email"></i></span>
                    </div>
                    <input type="email" name="email" class="form-control pl-15" placeholder="Masukkan Email Anda">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info">+62</span>
                      <!-- <span class="input-group-text bg-info border-info">+62</span> -->
                    </div>
                    <input type="text" name="noTelpon" class="form-control pl-15" onkeypress="if(isNaN( String.fromCharCode(event.keyCode) )) return false;" placeholder="Masukkan nomer telepon anda">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info"><i class="mdi mdi-arrange-send-backward"></i></span>
                    </div>
                    <input type="text" name="subject" class="form-control pl-15" placeholder="Subject">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <!-- <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info"><i class="mdi mdi-autorenew"></i></span>
                    </div> -->
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group">
                        <h5>Pilih Platform sesuai aplikasi anda <span class="text-danger">*</span></h5>
                        <div class="controls">
                          <fieldset>
                            <input type="checkbox" name="platform[]" id="checkbox_2" value="Web">
                            <label for="checkbox_2">Aplikasi Web</label>
                          </fieldset>
                          <fieldset>
                            <input type="checkbox" name="platform[]" id="checkbox_3" value="Android">
                            <label for="checkbox_3">Aplikasi Android</label>
                          </fieldset>
                          <fieldset>
                            <input type="checkbox" name="platform[]" id="checkbox_4" value="IOS">
                            <label for="checkbox_4">Aplikasi IOS</label>
                          </fieldset>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info"></span>
                    </div>
                    <textarea name="deskripsi" class="form-control pl-10" id="" cols="10" rows="10"></textarea><br>
                  </div>
                  <p>(*) Masukkan Deskripsi Project Aplikasi sesuai dengan keinginan anda</p>
                </div>
                <div class="form-group">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text bg-info border-info"><i class="mdi mdi-file-check"></i></span>
                    </div>
                    <input type="file" name="pendukung[]" class="form-control pl-15" multiple>
                  </div>
                  <p>Masukkan file pendukung</p>
                </div>
                  <div class="row">
                    <!-- <div class="col-12">
                      <div class="checkbox">
                      <input type="checkbox" id="basic_checkbox_1" >
                      <label for="basic_checkbox_1">I agree to the <a href="#" class="text-warning"><b>Terms</b></a></label>
                      </div>
                    </div> -->

                    <!-- //SERVER  <div class="col-md-12 form-group">
                       <div class="g-recaptcha" data-sitekey="6LdtC30UAAAAAPaiKkh3G8bETa4Xi9wTL2AqPoTT"></div>
                    </div> -->
                    
                    <div class="col-md-12 form-group">
                       <div class="g-recaptcha" data-sitekey="6LeN4oQUAAAAAL-pVIextohazViRvsX63sP3r85x"></div>
                    </div>
                  <!-- /.col -->
                    <div class="col-12 text-center">
                      <button type="submit" name="order" class="btn btn-info btn-block margin-top-10">ORDER</button>
                    </div>
                  <!-- /.col -->
                  </div>
              </form> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- jQuery 3 -->
  <script src="../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>

  <script src='https://www.google.com/recaptcha/api.js'></script>
  
  <!-- popper -->
  <script src="../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
</body>
</html>
