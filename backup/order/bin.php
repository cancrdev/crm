<!DOCTYPE html>
<html>
<head>
	<title>Jasa Pembuatan Aplikasi Android & IOS | ORDER</title>
  <link rel="icon" href="../assets/images/logo/can.png">
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  <link rel="stylesheet" href="../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
   <!-- Theme style -->
  <link rel="stylesheet" href="../assets/css/master_style.css">

  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../assets/css/skins/_all-skins.css">
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../assets/css/bootstrap-extend.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<style type="text/css">
		/*! normalize.css v3.0.2 | MIT License | git.io/normalize */
html {
  font-family: sans-serif;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
}

.image-foir {
  position: relative;
}

.foir-slide1 {
  position: absolute;
  left: 200px;
  top: 250px;
  /*right: 300px;*/
  /*color: red;*/
}

.foir-slide2 {
  position: absolute;
  left: 500px;
  /*padding-left: 30px;*/
  /*padding-right: 30px;*/
  top: 250px;
  /*color: red;*/
}

.foir-slide3 {
  position: absolute;
  left: 800px;
  top: 250px;
  right: 300px;
  /*color: red;*/
}

/*.foir-slide4 {
  position: absolute;
  left: 800px;
  top: 250px;
  right: 300px;
  color: red;
}*/

body {
  margin: 0;
}

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
  display: block;
}

audio, canvas, progress, video {
  display: inline-block;
  vertical-align: baseline;
}

audio:not([controls]) {
  display: none;
  height: 0;
}

[hidden], template {
  display: none;
}

a {
  background-color: transparent;
}

a:active, a:hover {
  outline: 0;
}

abbr[title] {
  border-bottom: 1px dotted;
}

b, strong {
  font-weight: bold;
}

dfn {
  font-style: italic;
}

h1 {
  font-size: 2em;
  margin: 0.67em 0;
}

mark {
  background: #ff0;
  color: #000;
}

small {
  font-size: 80%;
}

sub, sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sup {
  top: -0.5em;
}

sub {
  bottom: -0.25em;
}

img {
  border: 0;
}

svg:not(:root) {
  overflow: hidden;
}

figure {
  margin: 1em 40px;
}

hr {
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  height: 0;
}

pre {
  overflow: auto;
}

code, kbd, pre, samp {
  font-family: monospace,monospace;
  font-size: 1em;
}

button, input, optgroup, select, textarea {
  color: inherit;
  font: inherit;
  margin: 0;
}

button {
  overflow: visible;
}

button, select {
  text-transform: none;
}

button, html input[type="button"], input[type="reset"], input[type="submit"] {
  -webkit-appearance: button;
  cursor: pointer;
}

button[disabled], html input[disabled] {
  cursor: default;
}

button::-moz-focus-inner, input::-moz-focus-inner {
  border: 0;
  padding: 0;
}

input {
  line-height: normal;
}

input[type="checkbox"], input[type="radio"] {
  box-sizing: border-box;
  padding: 0;
}

input[type="number"]::-webkit-inner-spin-button, input[type="number"]::-webkit-outer-spin-button {
  height: auto;
}

input[type="search"] {
  -webkit-appearance: textfield;
  -moz-box-sizing: content-box;
  -webkit-box-sizing: content-box;
  box-sizing: content-box;
}

input[type="search"]::-webkit-search-cancel-button, input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}

fieldset {
  border: 1px solid #c0c0c0;
  margin: 0 2px;
  padding: 0.35em 0.625em 0.75em;
}

legend {
  border: 0;
  padding: 0;
}

textarea {
  overflow: auto;
}

optgroup {
  font-weight: bold;
}

table {
  border-collapse: collapse;
  border-spacing: 0;
}

td, th {
  padding: 0;
}

/*
clearfix.scss
*/
.clearfix {
  *zoom: 1;
}
.clearfix:before, .clearfix:after {
  content: "";
  display: table;
  line-height: 0;
}
.clearfix:after {
  clear: both;
}

*, *:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

/*
grid.scss
*/
.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}

.container-full {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
}

.row {
  margin-left: -15px;
  margin-right: -15px;
  *zoom: 1;
}
.row:before, .row:after {
  content: "";
  display: table;
  line-height: 0;
}
.row:after {
  clear: both;
}

[class*='grid-'] {
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

[class*='grid-']:last-of-type {
  /*padding-right: 0px;*/
}

.container::before, .container-full::before {
  display: table;
  content: "";
}

[class*='grid-']::after, .container::after, .container-full::before {
  content: "";
  display: block;
  overflow: hidden;
  visibility: hidden;
  font-size: 0;
  line-height: 0;
  width: 0;
  height: 0;
  clear: both;
}

@media only screen and (max-width: 768px) {
  [class*='grid-xs-'] {
    float: left;
  }

  .grid-xs-1 {
    width: 8.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-1 {
    margin-left: 8.33333333%;
  }

  .grid-xs-pull-1 {
    right: 8.33333333%;
  }

  .grid-xs-push-1 {
    left: 8.33333333%;
  }

  .grid-xs-2 {
    width: 16.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-2 {
    margin-left: 16.66666667%;
  }

  .grid-xs-pull-2 {
    right: 16.66666667%;
  }

  .grid-xs-push-2 {
    left: 16.66666667%;
  }

  .grid-xs-3 {
    width: 25%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-3 {
    margin-left: 25%;
  }

  .grid-xs-pull-3 {
    right: 25%;
  }

  .grid-xs-push-3 {
    left: 25%;
  }

  .grid-xs-4 {
    width: 33.3333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-4 {
    margin-left: 33.3333333%;
  }

  .grid-xs-pull-4 {
    right: 33.3333333%;
  }

  .grid-xs-push-4 {
    left: 33.3333333%;
  }

  .grid-xs-5 {
    width: 41.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-5 {
    margin-left: 41.66666667%;
  }

  .grid-xs-pull-5 {
    right: 41.66666667%;
  }

  .grid-xs-push-5 {
    left: 41.66666667%;
  }

  .grid-xs-6 {
    width: 50%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-6 {
    margin-left: 50%;
  }

  .grid-xs-pull-6 {
    right: 50%;
  }

  .grid-xs-push-6 {
    left: 50%;
  }

  .grid-xs-7 {
    width: 58.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-7 {
    margin-left: 58.33333333%;
  }

  .grid-xs-pull-7 {
    right: 58.33333333%;
  }

  .grid-xs-push-7 {
    left: 58.33333333%;
  }

  .grid-xs-8 {
    width: 66.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-8 {
    margin-left: 66.66666667%;
  }

  .grid-xs-pull-8 {
    right: 66.66666667%;
  }

  .grid-xs-push-8 {
    left: 66.66666667%;
  }

  .grid-xs-9 {
    width: 75%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-9 {
    margin-left: 75%;
  }

  .grid-xs-pull-9 {
    right: 75%;
  }

  .grid-xs-push-9 {
    left: 75%;
  }


  .grid-xs-10 {
    width: 83.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-10 {
    margin-left: 83.33333333%;
  }

  .grid-xs-pull-10 {
    right: 83.33333333%;
  }

  .grid-xs-push-10 {
    left: 83.33333333%;
  }

  .grid-xs-11 {
    width: 91.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-11 {
    margin-left: 91.66666667%;
  }

  .grid-xs-pull-11 {
    right: 91.66666667%;
  }

  .grid-xs-push-11 {
    left: 91.66666667%;
  }

  .grid-xs-12 {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-xs-offset-12 {
    margin-left: 100%;
  }

  .grid-xs-pull-12 {
    right: 100%;
  }

  .grid-xs-push-12 {
    left: 100%;
  }
}
@media only screen and (min-width: 768px) {
  [class*='grid-s-'] {
    float: left;
  }

  .container {
    width: 750px;
  }

  .grid-s-1 {
    width: 8.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-1 {
    margin-left: 8.33333333%;
  }

  .grid-s-pull-1 {
    right: 8.33333333%;
  }

  .grid-s-push-1 {
    left: 8.33333333%;
  }

  .grid-s-2 {
    width: 16.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-2 {
    margin-left: 16.66666667%;
  }

  .grid-s-pull-2 {
    right: 16.66666667%;
  }

  .grid-s-push-2 {
    left: 16.66666667%;
  }

  .grid-s-3 {
    width: 25%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-3 {
    margin-left: 25%;
  }

  .grid-s-pull-3 {
    right: 25%;
  }

  .grid-s-push-3 {
    left: 25%;
  }

  .grid-s-4 {
    width: 33.3333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-4 {
    margin-left: 33.3333333%;
  }

  .grid-s-pull-4 {
    right: 33.3333333%;
  }

  .grid-s-push-4 {
    left: 33.3333333%;
  }

  .grid-s-5 {
    width: 41.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-5 {
    margin-left: 41.66666667%;
  }

  .grid-s-pull-5 {
    right: 41.66666667%;
  }

  .grid-s-push-5 {
    left: 41.66666667%;
  }

  .grid-s-6 {
    width: 50%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-6 {
    margin-left: 50%;
  }

  .grid-s-pull-6 {
    right: 50%;
  }

  .grid-s-push-6 {
    left: 50%;
  }

  .grid-s-7 {
    width: 58.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-7 {
    margin-left: 58.33333333%;
  }

  .grid-s-pull-7 {
    right: 58.33333333%;
  }

  .grid-s-push-7 {
    left: 58.33333333%;
  }

  .grid-s-8 {
    width: 66.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-8 {
    margin-left: 66.66666667%;
  }

  .grid-s-pull-8 {
    right: 66.66666667%;
  }

  .grid-s-push-8 {
    left: 66.66666667%;
  }

  .grid-s-9 {
    width: 75%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-9 {
    margin-left: 75%;
  }

  .grid-s-pull-9 {
    right: 75%;
  }

  .grid-s-push-9 {
    left: 75%;
  }

  .grid-s-10 {
    width: 83.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-10 {
    margin-left: 83.33333333%;
  }

  .grid-s-pull-10 {
    right: 83.33333333%;
  }

  .grid-s-push-10 {
    left: 83.33333333%;
  }

  .grid-s-11 {
    width: 91.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-11 {
    margin-left: 91.66666667%;
  }

  .grid-s-pull-11 {
    right: 91.66666667%;
  }

  .grid-s-push-11 {
    left: 91.66666667%;
  }

  .grid-s-12 {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-s-offset-12 {
    margin-left: 100%;
  }

  .grid-s-pull-12 {
    right: 100%;
  }

  .grid-s-push-12 {
    left: 100%;
  }
}
@media only screen and (min-width: 992px) {
  [class*='grid-m-'] {
    float: left;
  }

  .container {
    width: 970px;
  }

  .grid-m-1 {
    width: 8.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-1 {
    margin-left: 8.33333333%;
  }

  .grid-m-pull-1 {
    right: 8.33333333%;
  }

  .grid-m-push-1 {
    left: 8.33333333%;
  }

  .grid-m-2 {
    width: 16.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-2 {
    margin-left: 16.66666667%;
  }

  .grid-m-pull-2 {
    right: 16.66666667%;
  }

  .grid-m-push-2 {
    left: 16.66666667%;
  }

  .grid-m-3 {
    width: 25%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-3 {
    margin-left: 25%;
  }

  .grid-m-pull-3 {
    right: 25%;
  }

  .grid-m-push-3 {
    left: 25%;
  }

  .grid-m-4 {
    width: 33.3333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-4 {
    margin-left: 33.3333333%;
  }

  .grid-m-pull-4 {
    right: 33.3333333%;
  }

  .grid-m-push-4 {
    left: 33.3333333%;
  }

  .grid-m-5 {
    width: 41.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-5 {
    margin-left: 41.66666667%;
  }

  .grid-m-pull-5 {
    right: 41.66666667%;
  }

  .grid-m-push-5 {
    left: 41.66666667%;
  }

  .grid-m-6 {
    width: 50%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-6 {
    margin-left: 50%;
  }

  .grid-m-pull-6 {
    right: 50%;
  }

  .grid-m-push-6 {
    left: 50%;
  }

  .grid-m-7 {
    width: 58.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-7 {
    margin-left: 58.33333333%;
  }

  .grid-m-pull-7 {
    right: 58.33333333%;
  }

  .grid-m-push-7 {
    left: 58.33333333%;
  }

  .grid-m-8 {
    width: 66.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-8 {
    margin-left: 66.66666667%;
  }

  .grid-m-pull-8 {
    right: 66.66666667%;
  }

  .grid-m-push-8 {
    left: 66.66666667%;
  }

  .grid-m-9 {
    width: 75%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-9 {
    margin-left: 75%;
  }

  .grid-m-pull-9 {
    right: 75%;
  }

  .grid-m-push-9 {
    left: 75%;
  }

  .grid-m-10 {
    width: 83.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-10 {
    margin-left: 83.33333333%;
  }

  .grid-m-pull-10 {
    right: 83.33333333%;
  }

  .grid-m-push-10 {
    left: 83.33333333%;
  }

  .grid-m-11 {
    width: 91.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-11 {
    margin-left: 91.66666667%;
  }

  .grid-m-pull-11 {
    right: 91.66666667%;
  }

  .grid-m-push-11 {
    left: 91.66666667%;
  }

  .grid-m-12 {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-m-offset-12 {
    margin-left: 100%;
  }

  .grid-m-pull-12 {
    right: 100%;
  }

  .grid-m-push-12 {
    left: 100%;
  }
}
@media only screen and (min-width: 1200px) {
  [class*='grid-l-'] {
    float: left;
  }

  .container {
    width: 1170px;
  }

  .grid-l-1 {
    width: 8.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-1 {
    margin-left: 8.33333333%;
  }

  .grid-l-pull-1 {
    right: 8.33333333%;
  }

  .grid-l-push-1 {
    left: 8.33333333%;
  }

  .grid-l-2 {
    width: 16.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-2 {
    margin-left: 16.66666667%;
  }

  .grid-l-pull-2 {
    right: 16.66666667%;
  }

  .grid-l-push-2 {
    left: 16.66666667%;
  }

  .grid-l-3 {
    width: 25%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-3 {
    margin-left: 25%;
  }

  .grid-l-pull-3 {
    right: 25%;
  }

  .grid-l-push-3 {
    left: 25%;
  }

  .grid-l-4 {
    width: 33.3333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-4 {
    margin-left: 33.3333333%;
  }

  .grid-l-pull-4 {
    right: 33.3333333%;
  }

  .grid-l-push-4 {
    left: 33.3333333%;
  }

  .grid-l-5 {
    width: 41.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-5 {
    margin-left: 41.66666667%;
  }

  .grid-l-pull-5 {
    right: 41.66666667%;
  }

  .grid-l-push-5 {
    left: 41.66666667%;
  }

  .grid-l-6 {
    width: 50%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-6 {
    margin-left: 50%;
  }

  .grid-l-pull-6 {
    right: 50%;
  }

  .grid-l-push-6 {
    left: 50%;
  }

  .grid-l-7 {
    width: 58.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-7 {
    margin-left: 58.33333333%;
  }

  .grid-l-pull-7 {
    right: 58.33333333%;
  }

  .grid-l-push-7 {
    left: 58.33333333%;
  }

  .grid-l-8 {
    width: 66.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-8 {
    margin-left: 66.66666667%;
  }

  .grid-l-pull-8 {
    right: 66.66666667%;
  }

  .grid-l-push-8 {
    left: 66.66666667%;
  }

  .grid-l-9 {
    width: 75%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-9 {
    margin-left: 75%;
  }

  .grid-l-pull-9 {
    right: 75%;
  }

  .grid-l-push-9 {
    left: 75%;
  }

  .grid-l-10 {
    width: 83.33333333%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-10 {
    margin-left: 83.33333333%;
  }

  .grid-l-pull-10 {
    right: 83.33333333%;
  }

  .grid-l-push-10 {
    left: 83.33333333%;
  }

  .grid-l-11 {
    width: 91.66666667%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-11 {
    margin-left: 91.66666667%;
  }

  .grid-l-pull-11 {
    right: 91.66666667%;
  }

  .grid-l-push-11 {
    left: 91.66666667%;
  }

  .grid-l-12 {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    position: relative;
    min-height: 1px;
  }

  .grid-l-offset-12 {
    margin-left: 100%;
  }

  .grid-l-pull-12 {
    right: 100%;
  }

  .grid-l-push-12 {
    left: 100%;
  }
}
.grid-all-1 {
  width: 8.33333333%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-1 {
  margin-left: 8.33333333%;
}

.grid-all-pull-1 {
  right: 8.33333333%;
}

.grid-all-push-1 {
  left: 8.33333333%;
}

.grid-all-2 {
  width: 16.66666667%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-2 {
  margin-left: 16.66666667%;
}

.grid-all-pull-2 {
  right: 16.66666667%;
}

.grid-all-push-2 {
  left: 16.66666667%;
}

.grid-all-3 {
  width: 25%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-3 {
  margin-left: 25%;
}

.grid-all-pull-3 {
  right: 25%;
}

.grid-all-push-3 {
  left: 25%;
}

.grid-all-4 {
  width: 33.3333333%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-4 {
  margin-left: 33.3333333%;
}

.grid-all-pull-4 {
  right: 33.3333333%;
}

.grid-all-push-4 {
  left: 33.3333333%;
}

.grid-all-5 {
  width: 41.66666667%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-5 {
  margin-left: 41.66666667%;
}

.grid-all-pull-5 {
  right: 41.66666667%;
}

.grid-all-push-5 {
  left: 41.66666667%;
}

.grid-all-6 {
  width: 50%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-6 {
  margin-left: 50%;
}

.grid-all-pull-6 {
  right: 50%;
}

.grid-all-push-6 {
  left: 50%;
}

.grid-all-7 {
  width: 58.33333333%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-7 {
  margin-left: 58.33333333%;
}

.grid-all-pull-7 {
  right: 58.33333333%;
}

.grid-all-push-7 {
  left: 58.33333333%;
}

.grid-all-8 {
  width: 66.66666667%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-8 {
  margin-left: 66.66666667%;
}

.grid-all-pull-8 {
  right: 66.66666667%;
}

.grid-all-push-8 {
  left: 66.66666667%;
}

.grid-all-9 {
  width: 75%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-9 {
  margin-left: 75%;
}

.grid-all-pull-9 {
  right: 75%;
}

.grid-all-push-9 {
  left: 75%;
}

.grid-all-10 {
  width: 83.33333333%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-10 {
  margin-left: 83.33333333%;
}

.grid-all-pull-10 {
  right: 83.33333333%;
}

.grid-all-push-10 {
  left: 83.33333333%;
}

.grid-all-11 {
  width: 91.66666667%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-11 {
  margin-left: 91.66666667%;
}

.grid-all-pull-11 {
  right: 91.66666667%;
}

.grid-all-push-11 {
  left: 91.66666667%;
}

.grid-all-12 {
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  position: relative;
  min-height: 1px;
}

.grid-all-offset-12 {
  margin-left: 100%;
}

.grid-all-pull-12 {
  right: 100%;
}

.grid-all-push-12 {
  left: 100%;
}

*, *:before, *:after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

html, body {
  height: 100%;
  min-height: 100%;
  margin: 0;
  padding: 0;
  font-size: 14px;
}

body {
  background-color: #fcfcfc;
  color: #444;
  font-family: 'Roboto', sans-serif;
  -webkit-font-smoothing: antialiased;
}

ul li {
  list-style-type: none;
}

a {
  text-decoration: none;
}

h1, h2, h3, h4, h5, h6 {
  -webkit-font-smoothing: antialiased;
  font-weight: 400;
}

h1 {
  font-size: 4.2rem;
  line-height: 110%;
  margin: 2.1rem 0 1.68rem 0;
}

h2 {
  font-size: 3.56rem;
  line-height: 110%;
  margin: 1.78rem 0 1.424rem 0;
}

h3 {
  font-size: 2.92rem;
  line-height: 110%;
  margin: 1.46rem 0 1.168rem 0;
}

h4 {
  font-size: 2.28rem;
  line-height: 110%;
  margin: 1.14rem 0 0.912rem 0;
}

h5 {
  font-size: 1.64rem;
  line-height: 110%;
  margin: 0.82rem 0 0.656rem 0;
}

h6 {
  font-size: 1rem;
  line-height: 110%;
  margin: 0.5rem 0 0.4rem 0;
}

.left {
  float: left;
}

.right {
  float: right;
}

.center {
  text-align: center;
}

.clearfix {
  *zoom: 1;
}
.clearfix:before, .clearfix:after {
  content: "";
  display: table;
  line-height: 0;
}
.clearfix :after {
  clear: both;
}

/*
NAV
*/
.nav {
  transition: all .5s ease;
  font-size: 12px;
  font-family: 'Arial-black', sans-serif;
  width: 100%;
  z-index: 100;
  position: absolute;
  /* bottom: 0; */
  left: 0;
  letter-spacing: 2px;
  line-height: 100px;
  -webkit-transition-property: background-color, box-shadow, line-height, height;
  transition-property: background-color, box-shadow, line-height, height;
  -webkit-transition-timing-function: cubic-bezier(0.78, 0.13, 0.15, 0.86);
  transition-timing-function: cubic-bezier(0.78, 0.13, 0.15, 0.86);
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
}
.nav .brand {
  /* padding: 15px 60px; */
  /* line-height: 30px; */
  line-height: 100px;
  padding-left: 60px;
  padding-right: 60px;
  display: inline-block;
  float: left;
  font-size: 20px;
  font-family: 'Roboto', cursive;
  -webkit-transition-property: background-color, box-shadow, line-height, height;
  transition-property: background-color, box-shadow, line-height, height;
  -webkit-transition-timing-function: cubic-bezier(0.78, 0.13, 0.15, 0.86);
  transition-timing-function: cubic-bezier(0.78, 0.13, 0.15, 0.86);
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  /*background-image: url(../assets/images/logo/logo.png);*/
}
.nav .brand a {
  color: #E33B00;
  text-decoration: none;
}
.nav ul {
  margin: 0;
  text-transform: uppercase;
}
.nav ul li {
  text-align: center;
  display: inline-block;
  list-style: none;
  padding: 15px 15px;
  cursor: pointer;
  line-height: 30px;
}
.nav ul li:hover a {
  font-weight: bold;
}
.nav ul li a {
  color: #eee;
  text-decoration: none;
}

.sticky {
  position: fixed !important;
  top: 0;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3);
  /* box-shadow: 0 4px 5px -2px rgba(0,0,0,0.22), 0 2px 1px -1px rgba(0,0,0,0.05); */
  background-color: #fff;
  line-height: 30px;
}
.sticky .brand {
  line-height: 60px;
}
.sticky ul li a {
  color: #6E7982;
}
.sticky ul li:hover a {
  color: #E33B00;
}

.pattern-overlay {
  background: rgba(0, 0, 0, 0.3) url("http://codemydesign.ru/CMDSpace/image/css/images/overlay.png") repeat;
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 0;
}

.menu {
  display: none;
}

@media (max-width: 600px) {
  .sticky .menu {
    top: 0;
  }
  .sticky .menu .hamburger {
    background: #6E7982;
  }
  .sticky .menu .hamburger::before, .sticky .menu .hamburger::after {
    background: #6E7982;
  }

  .open.sticky .hamburger {
    background: transparent;
  }

  .open .hamburger {
    background-color: transparent;
  }
  .open .hamburger::before {
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  .open .hamburger::after {
    -webkit-transform: rotate(-45deg) translate(2px, -2px);
    -ms-transform: rotate(-45deg) translate(2px, -2px);
    transform: rotate(-45deg) translate(2px, -2px);
  }

  .menu {
    display: block;
    outline: none;
    position: relative;
    line-height: 60px;
    float: left;
    left: 20px;
    top: 20px;
    width: 60px;
    height: 60px;
    background: none;
    border: 0;
    padding: 0;
    margin: 0;
    cursor: pointer;
    opacity: 0.7;
    -webkit-transition: opacity 150ms;
    transition: opacity 150ms;
  }
  .menu:hover {
    opacity: 1;
  }

  .hamburger, .hamburger::after, .hamburger::before {
    margin: 0 auto;
    display: block;
    width: 24px;
    height: 3px;
    line-height: 0;
    -webkit-transition: all 150ms;
    transition: all 150ms;
  }

  .hamburger::before {
    content: '';
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    background: #fff;
  }

  .hamburger::after {
    content: '';
    -webkit-transform: rotate(-45deg) translate(2px, -2px);
    -ms-transform: rotate(-45deg) translate(2px, -2px);
    transform: rotate(-45deg) translate(2px, -2px);
    background: #fff;
  }

  .hamburger {
    background: #fff;
  }
  .hamburger::after {
    -webkit-transform: translateY(5px);
    -ms-transform: translateY(5px);
    transform: translateY(5px);
  }
  .hamburger::before {
    -webkit-transform: translateY(-8px);
    -ms-transform: translateY(-8px);
    transform: translateY(-8px);
  }

  /*.collapse nav{
    -webkit-transform:translateY(-100%);
    -ms-transform:translateY(-100%);
    transform:translateY(-100%);
  }*/
  .navbar {
    -webkit-transition: -webkit-transform 150ms;
    transition: transform 150ms;
    position: fixed;
  }

  ul.navbar {
    -webkit-transform: translate(-100%, 0);
    -ms-transform: translate(-100%, 0);
    transform: translate(-100%, 0);
    /* transform: translateY(-200%); */
    padding-left: 0;
    position: fixed;
  }
  ul.navbar li {
    line-height: calc((100vh - 60px) / 6);
    display: block;
    position: fixed;
  }

  .open .navbar {
    -webkit-transform: translate(0, 0);
    -ms-transform: translate(0, 0);
    transform: translate(0, 0);
    position: fixed;
  }

  .nav .brand {
    display: block;
    text-align: center;
    float: none;
  }

  .sticky .brand {
    background-color: white;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3);
  }

  #nav {
    height: 100px;
  }
  #nav.open {
    height: auto;
    min-height: 100%;
  }
  #nav.sticky {
    height: 60px;
  }
  #nav .open.sticky {
    height: auto;
  }
}
/*
SECTION
*/
.section {
  position: relative;
  margin: 0;
  padding: 0;
  /* height: 100%; */
  width: 100%;
  /* background-color: white; */
}
.section .section-content {
  /* padding: 30px; */
  padding: 15px;
}

.intro {
  position: relative;
  /*background-size: cover;*/
  /*background-repeat: no-repeat;*/
  /*margin: 0;*/
  /*padding: 0;*/
  height: 68%;
  width: 100%;
}

.feature-accordion {
  background-color: #ca3400;
  background-color: #fff;
  /* color: #fff; */
  /* height: 250px; */
  /* padding: 80px; */
}
.feature-accordion .accordion-text {
  padding: 15px;
  font-size: 42px;
  font-weight: 300px;
  background-color: #ca3400;
  color: #fff;
}
.feature-accordion .collapsible {
  border-top: 1px solid #ddd;
  border-right: 1px solid #ddd;
  border-left: 1px solid #ddd;
  margin: 0;
  padding: 0;
  color: rgba(0, 0, 0, 0.8);
  color: #444;
  /* margin: 0.5rem 0 1rem 0; */
  -webkit-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  -moz-box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
}
.feature-accordion .collapsible.popout {
  border: none;
  box-shadow: none;
}
.feature-accordion .collapsible.popout > li {
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
  transform: scaleX(0.9) translate3d(0, 0, 0);
  transition: margin 0.35s cubic-bezier(0.25, 0.46, 0.45, 0.94), transform 0.35s cubic-bezier(0.25, 0.46, 0.45, 0.94);
}
.feature-accordion .collapsible.popout > li:hover {
  will-change: margin, transform;
}
.feature-accordion .collapsible.popout > li.active {
  box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
  margin: 16px 0;
  transform: scaleX(1) translate3d(0, 0, 0);
}
.feature-accordion .collapsible-header {
  display: block;
  cursor: pointer;
  height: 3rem;
  line-height: 3rem;
  padding: 0 1rem;
  background-color: #fff;
  border-bottom: 1px solid #ddd;
}
.feature-accordion .collapsible-header i {
  width: 2rem;
  font-size: 1.6rem;
  line-height: 3rem;
  display: block;
  float: left;
  text-align: center;
  margin-right: 1rem;
}
.feature-accordion .collapsible-body {
  overflow: hidden;
  display: none;
  border-bottom: 1px solid #ddd;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  background-color: #fff;
}
.feature-accordion .collapsible-body p {
  margin: 0;
  padding: 2rem;
}

.feature {
  background-color: #ca3400;
  color: #fff;
  height: 250px;
  /* padding: 80px; */
}
.feature .feature-content {
  display: table;
  position: relative;
  margin: 0 auto;
}
.feature .feature-content .feature-text {
  /* font-weight: 300; */
  /* width: 100%; */
  display: table-cell;
  vertical-align: middle;
  text-align: left;
}
.feature .feature-content .feature-button {
  /* width: 100%; */
  display: table-cell;
  vertical-align: middle;
  text-align: right;
}
.feature .feature-content .feature-button .btn {
  padding: 11px 22px;
  background-color: white;
  color: #444;
  box-shadow: 0 2px 3px rgba(0, 0, 0, 0.1), 0 4px 8px rgba(0, 0, 0, 0.3);
  border: none;
  border-radius: 3px;
  text-transform: uppercase;
  cursor: pointer;
  -webkit-transition: background-color 0.3s, box-shadow 0.3s, color 0.3s;
  transition: background-color 0.3s, box-shadow 0.3s, color 0.3s;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  vertical-align: middle;
  text-align: center;
  white-space: nowrap;
  outline: 0;
}
.feature .feature-content .feature-button .btn:active, .feature .feature-content .feature-button .btn:focus, .feature .feature-content .feature-button .btn:hover {
  box-shadow: 0 3px 3px rgba(0, 0, 0, 0.1), 0 5px 8px rgba(0, 0, 0, 0.16), 0 9px 20px rgba(0, 0, 0, 0.16) !important;
}

/*
footer
*/
footer {
  width: 100%;
  /*background: url("");*/
  /*background-size: contain;*/
  /*background-repeat: no-repeat;*/
}
footer .footer-top {
  padding: 15px;
  padding-left: 40px;
  width: 100%;
  /* background-color: #212121; */
  /*background-color: rgba(0, 0, 0, 0.45);*/
  /*color: #757575;*/
}
footer .footer-top .footer-top-section {
  padding: 30px;
  background-color: white;
}
footer .footer-top .footer-top-section form.contact {
  font-size: 16px;
}
footer .footer-top .footer-top-section form.contact .form-group {
  margin-bottom: 20px;
}
footer .footer-top .footer-top-section form.contact label {
  font-weight: bold;
  padding: 7px 0px;
  display: block;
  cursor: text;
}
footer .footer-top .footer-top-section form.contact input, footer .footer-top .footer-top-section form.contact textarea {
  resize: vertical;
  width: 100%;
  border: 0;
  margin: 0;
  outline: 0;
  border-radius: 2px;
  border: 1px solid #f5f5f5;
  padding: 7px 10px;
  border-color: #e6e6e6;
  -webkit-transition: .2s ease-out;
  -moz-transition: .2s ease-out;
  -o-transition: .2s ease-out;
  -ms-transition: .2s ease-out;
  transition: .2s ease-out;
}
footer .footer-top .footer-top-section form.contact input:hover, footer .footer-top .footer-top-section form.contact input:active, footer .footer-top .footer-top-section form.contact input:focus, footer .footer-top .footer-top-section form.contact textarea:hover, footer .footer-top .footer-top-section form.contact textarea:active, footer .footer-top .footer-top-section form.contact textarea:focus {
  background-color: #fff;
  border-color: #f5f5f5;
  -webkit-box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);
}
footer .footer-top .footer-top-section form.contact #btnSend i {
  vertical-align: middle;
  font-size: 16px;
  transform: rotate(-45deg);
  top: -3px;
  position: relative;
}
footer .footer-top .footer-top-section form.contact .btn {
  text-decoration: none;
  color: #fff;
  background-color: #E33B00;
  text-align: center;
  letter-spacing: .5px;
  -webkit-transition: .2s ease-out;
  -moz-transition: .2s ease-out;
  -o-transition: .2s ease-out;
  -ms-transition: .2s ease-out;
  transition: .2s ease-out;
  cursor: pointer;
  border: none;
  border-radius: 2px;
  display: inline-block;
  height: 36px;
  line-height: 36px;
  outline: 0;
  padding: 0 2rem;
  text-transform: uppercase;
  vertical-align: middle;
  -webkit-tap-highlight-color: transparent;
  /*box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);*/
}
footer .footer-top .footer-top-section form.contact .btn:hover {
  background-color: #ed3e00;
  -webkit-box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 2px 3px rgba(0, 0, 0, 0.05);
  /* box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15); */
}
footer .footer-bottom {
  padding: 15px;
  background-color: #111;
  /*background-image: url(../assets/images/logo/footer-background.png);*/
  color: #757575;
  width: 100%;
  display: inline-flex;
}

.footer-bottom-a {
	padding: 15px;
  	background-color: #111;
  /*background-image: url(../assets/images/logo/footer-background.png);*/
  	color: #757575;
  	width: 100%;
  	display: inline-block;
}

.gotop {
  display: block;
  position: fixed;
  bottom: 15px;
  right: 15px;
  text-align: center;
  cursor: pointer;
  font-size: 14px;
  line-height: 54px;
  height: 54px;
  width: 54px;
  border-radius: 50%;
  z-index: 100;
  outline: none !important;
  -webkit-transition: background-color 0.3s, box-shadow 0.3s, bottom 0.3s;
  transition: background-color 0.3s, box-shadow 0.3s, bottom 0.3s;
  background-color: rgba(0, 0, 0, 0.3);
  color: #fff;
}
.gotop i {
  vertical-align: middle;
}
.gotop:active, .gotop:focus, .gotop:hover {
  background-color: #E33B00;
  color: white;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.25), 0 10px 30px rgba(0, 0, 0, 0.15);
}

/*
about
*/
.about .feature-box {
  display: inline-block;
  width: 100%;
  position: relative;
}
.about .feature-box .iconbox {
  padding: 15px;
}
.about .feature-box .iconbox .iconbox-inner {
  /* float: left; */
  /* margin: 15px; */
  /* width: 29%; */
  padding: 15px;
  box-shadow: 0 1.5px 4px rgba(0, 0, 0, 0.24), 0 1.5px 6px rgba(0, 0, 0, 0.12);
  -webkit-transition: all 0.2s cubic-bezier(0.3, 0.6, 0.2, 1.8);
  transition: all 0.2s cubic-bezier(0.3, 0.6, 0.2, 1.8);
}
.about .feature-box .iconbox .iconbox-inner:hover {
  -webkit-transform: scale(1.04);
  -ms-transform: scale(1.04);
  transform: scale(1.04);
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.23), 0 3px 12px rgba(0, 0, 0, 0.16);
}
.about .feature-box .iconbox .iconbox-inner .iconbox-icon {
  background-color: #E33B00;
  font-size: 26px;
  line-height: 60px;
  height: 60px;
  width: 60px;
  color: #fff;
  text-align: center;
  overflow: hidden;
  border-radius: 50%;
  float: left;
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12) inset, 0 1px 1px rgba(0, 0, 0, 0), 0 2px 3px rgba(0, 0, 0, 0);
}
.about .feature-box .iconbox .iconbox-inner .iconbox-icon i {
  line-height: inherit;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.25);
}
.about .feature-box .iconbox .iconbox-inner .iconbox-title {
  margin-left: 80px;
  text-align: left;
}
.about .feature-box .iconbox .iconbox-inner .iconbox-text {
  margin-left: 80px;
  text-align: left;
}
.about .feature-box .iconbox .iconbox-inner:hover {
  background-color: white;
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.23), 0 3px 12px rgba(0, 0, 0, 0.16);
}

/*
works
*/
/*
https://codepen.io/zerospree/pen/LVawqN
*/
.works {
  *zoom: 1;
}
.works:before, .works:after {
  content: "";
  display: table;
  line-height: 0;
}
.works:after {
  clear: both;
}
.works .section-content > img {
  -webkit-transition: width 150ms, height 150ms;
  transition: width 150ms, height 150ms;
  width: 150px;
  height: 150px;
  border-radius: 50%;
  position: absolute;
  z-index: 2;
  top: 0;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
}
.works .section-content > img.sticky {
  box-shadow: 0 2px 3px 2px rgba(0, 0, 0, 0.25);
  position: fixed;
  top: 40px;
  width: 75px;
  height: 75px;
  z-index: 100;
}
.works .works-box {
  /* text-decoration: none; */
  position: relative;
  width: 100%;
  /* width: calc(100% + 30px); */
}
.works .works-item-wrap {
  width: 100%;
  box-shadow: 0 1.5px 4px rgba(0, 0, 0, 0.24), 0 1.5px 6px rgba(0, 0, 0, 0.12);
  -webkit-transition: all 0.2s cubic-bezier(0.3, 0.6, 0.2, 1.8);
  transition: all 0.2s cubic-bezier(0.3, 0.6, 0.2, 1.8);
}
.works .works-item-wrap:hover {
  -webkit-transform: scale(1.04);
  -ms-transform: scale(1.04);
  transform: scale(1.04);
  box-shadow: 0 3px 12px rgba(0, 0, 0, 0.23), 0 3px 12px rgba(0, 0, 0, 0.16);
}
.works .works-item-wrap > img {
  width: 100%;
  vertical-align: baseline;
  margin: 0;
  padding: 0;
  border: 0;
}
.works .works-item {
  padding: 15px;
  height: auto;
  font-weight: 400;
  line-height: 0;
  box-sizing: border-box;
  /* display: inline-block; */
}
.works .works-item .works-item-social {
  position: relative;
  margin-top: -23px;
  margin-bottom: 0px;
  color: white;
  width: 100%;
  /* top: -19px; */
  /* right: 5px; */
  text-align: right;
}
.works .works-item .works-item-social .social-circle {
  cursor: pointer;
  border-radius: 50%;
  background-color: #00C853;
  width: 35px;
  height: 35px;
  padding: 5px;
  display: inline-block;
  box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
}
.works .works-item .works-item-social .social-circle i {
  font-size: 18px;
  padding: 3px;
  vertical-align: middle;
}
.works .works-item .works-item-content {
  background: #28261F;
  color: #fff;
  padding: 5px 20px 25px 20px;
}
.works .works-item .works-item-content h4 {
  margin-top: 0px;
}
.works .works-item .works-item-content.dark {
  color: #000;
}
.works .works-item .works-item-content .card-more {
  cursor: pointer;
  right: -5px;
  bottom: 15px;
  position: relative;
  float: right;
}
.works .works-item .works-item-reveal {
  display: none;
  transform: translateY(0px);
  padding: 20px;
  position: absolute;
  background-color: #fff;
  width: 100%;
  overflow-y: auto;
  top: 100%;
  height: 100%;
  z-index: 5;
  display: none;
}
.works .works-item .works-item-reveal.revealed {
  display: block;
  transform: translateY(-100%);
}
.works .works-item .works-item-reveal.revealed:hover {
  transform: translateY(-100%) scale(1.04);
}
.works .works-item p {
  padding: 20px;
  margin: 0;
  line-height: 26px;
  font-size: 16px;
  color: #8B8B8D;
}

@media (max-width: 900px) {
  .works .works-item {
    width: 50%;
  }
}
@media (max-width: 600px) {
  .works .works-item {
    width: 100%;
  }
}
/*itens-feature*/
.itens-feature .itens-feature-box {
  /* display: inline-block; */
}
.itens-feature .itens-feature-box .itens-feature-item .itens-feature-icon {
  color: #333333;
  float: left;
}
.itens-feature .itens-feature-box .itens-feature-item .itens-feature-icon i {
  font-size: 40px;
  border: 1px solid #dcdcdc;
  border-radius: 50%;
  padding: 10px;
}
.itens-feature .itens-feature-box .itens-feature-item .itens-feature-text {
  margin-left: 80px;
  margin-top: 5px;
  position: relative;
}
.itens-feature .itens-feature-box .itens-feature-item .itens-feature-text h5 {
  font-size: 20px;
  font-weight: bold;
  text-transform: uppercase;
  margin: 0px;
  margin-bottom: 10px;
  position: relative;
}
.itens-feature .itens-feature-box .itens-feature-item .itens-feature-text p {
  text-align: justify;
  line-height: 1.5;
  position: relative;
  color: #777777;
}

/* -- Ripple effect -- */
.ripple {
  position: relative;
  overflow: hidden;
  -webkit-transform: translate3d(0, 0, 0);
}

.wave {
  display: block;
  position: absolute;
  pointer-events: none;
  border-radius: 50%;
  -webkit-transform: scale(0);
  -moz-transform: scale(0);
  -ms-transform: scale(0);
  -o-transform: scale(0);
  transform: scale(0);
  background: #fff;
  opacity: 1;
}

.wave.animate {
  background-color: rgba(255, 255, 255, 0.2);
  -webkit-animation: ripple .5s linear;
  -moz-animation: ripple .5s linear;
  -ms-animation: ripple .5s linear;
  -o-animation: ripple .5s linear;
  animation: ripple .5s linear;
}

@keyframes ripple {
  100% {
    opacity: 0;
    transform: scale(2.5);
  }
}
@-webkit-keyframes ripple {
  100% {
    opacity: 0;
    -webkit-transform: scale(2.5);
    transform: scale(2.5);
  }
}
@-moz-keyframes ripple {
  100% {
    opacity: 0;
    -moz-transform: scale(2.5);
    transform: scale(2.5);
  }
}
@-ms-keyframes ripple {
  100% {
    opacity: 0;
    -ms-transform: scale(2.5);
    transform: scale(2.5);
  }
}
@-o-keyframes ripple {
  100% {
    opacity: 0;
    -o-transform: scale(2.5);
    transform: scale(2.5);
  }
}

	</style>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.1/masonry.pkgd.min.js"></script>
	<script type="text/javascript">
		//http://www.materialup.com/posts/wip-avg-forecast
//http://themeforest.net/item/joyson-personal-resume-cv-vcard-portfolio-html/full_screen_preview/12058136
/*http://callmenick.com/post/expanding-search-bar-using-css-transitions*/
(function($) {
    "use strict";
  
    var $navbar = $(".nav"),
        y_pos = $navbar.offset().top,
        height = $navbar.height();

    //scroll top 0 sticky
    $(document).scroll(function() {
        var scrollTop = $(this).scrollTop();
        if (scrollTop > 0) {
          $navbar.addClass("sticky");
        } else {
          $navbar.removeClass("sticky");  
        }
    });
    
    //section sticky
    /*$(document).scroll(function() {
        var scrollTop = $(this).scrollTop();
        if ($(window).height() > scrollTop) {
          $navbar.removeClass("sticky");
        } else {
          $navbar.addClass("sticky");  
        }
    });*/

})(jQuery, undefined);

$(".menu").click(function(){
  $("#nav").toggleClass("open");
});

$(window).load(function(){
	// Initialize portfolio masonry
	$('.works-box').masonry({
		itemSelector       : '.works-item',
		transitionDuration : 0,
    percentPosition: true
	});
});

$(".card-more").click(function(){
  $(this).parents().eq(1).find(".works-item-reveal").toggleClass("revealed");
});
$(".collapsible li").click(function(){
  $(this).siblings().removeClass("active"); //comment this line to make the accordion expansible
  $(this).toggleClass("active");
  if($(this).hasClass("active")){
    $(this).siblings().find('.collapsible-body').slideUp(300);
    $(this).find('.collapsible-body').slideDown(300);
  }else{
    $(this).find('.collapsible-body').slideUp(300);
  }
  //$(this).find('.collapsible-body').slideToggle(500); //change display property
});


// Ripple-effect animation
(function($) {
    $(".ripple").click(function(e){
        var rippler = $(this);

        // create .wave element if it doesn't exist
        if(rippler.find(".wave").length == 0) {
            rippler.append("<span class='wave'></span>");
        }

        var wave = rippler.find(".wave");

        // prevent quick double clicks
        wave.removeClass("animate");

        // set .wave diametr
        if(!wave.height() && !wave.width())
        {
            var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
            wave.css({height: d, width: d});
        }

        // get click coordinates
        var x = e.pageX - rippler.offset().left - wave.width()/2;
        var y = e.pageY - rippler.offset().top - wave.height()/2;

        // set .wave position and add class .animate
        wave.css({
          top: y+'px',
          left:x+'px'
        }).addClass("animate");
    })
})(jQuery);
	</script>
</head>
<body>
<nav id="nav" class="nav">
  <button class="menu">
    <em class="hamburger"></em>
  </button>
  <!-- <div class="brand">
    <a href="http://can.co.id">
    	<img src="../assets/images/logo/can.png" width="80px">
    	CAN CREATIVE
    </a>
  </div> -->
  <ul class="navbar">
    <li class="brand">
      <a href="http://can.co.id">
      <img src="../assets/images/logo/can.png" width="90px">
      <!-- CAN CREATIVE -->
    </a>
    </li>
    <li>
      <a href="http://can.co.id">Beranda</a>
    </li>
    <li>
      <a href="http://can.co.id/tentang-kami-2">Tentang Kami</a>
    </li>
    <li>
      <a href="http://can.co.id/layanan">Layanan</a>
    </li>
    <li>
      <a href="http://can.co.id/jasa-pembuatan-aplikasi-android-ios-portofolio">Portofolio</a>
    </li>
    <li>
      <a href="http://can.co.id/artikel" target="_blank">Artikel</a>
    </li>
    <li>
      <a href="#">Order</a>
    </li>
  </ul>
</nav>
<section class="section intro">
  <!-- <p>jhkhjkh</p> -->
  <img src="../assets/images/logo/background-atas.png" width="1300px" height="470px" alt="">
  <div class="pattern-overlay"></div>
</section>

<footer>
  <div class="footer-top" style="background-image: url(../assets/images/logo/pattern-center.png);">
  	<!-- <img src="../assets/images/logo/pattern-center.png" width="1500px" height="800px" alt=""> -->
  	<h4 align="center">Form Order Project</h4>
    <div class="row">
      <div class="grid-m-8 grid-offset-3 grid-s-8">
        <div class="footer-top-section">
        <?php 
          include_once('../stucture/fungsi.php');
          $log = new Model();

          if(isset($_POST['g-recaptcha-response']) && isset($_POST['order'])) {
            $captcha = $_POST['g-recaptcha-response'];
            if (!$captcha) { ?>
              <script type="text/javascript">swal("Oops...", "Tolong cek akses anda dengan reCAPTCHA box. :)", "error");</script>
           <?php } else {
              $secret = '6LeN4oQUAAAAAKo_xuQTZ-yNszg7_pdR8U1G0nGR';
              $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
              if ($response != true) { ?>
                <script type="text/javascript">swal("Oops...", "lengkapi parameter anda :(", "error");</script>
             <?php } else {
                $order["nama"]      = $_POST["nama"];
                $order["email"]     = $_POST["email"];
                $order["noTelpon"]  = $_POST["noTelpon"];
                $order["subject"]   = $_POST["subject"];
                $order["platform"]  = $_POST["platform"];
                $order["deskripsi"] = $_POST["deskripsi"];
                $order["pendukung"] = $_FILES["pendukung"];
                $ctrl = $log->orderApp($order);
              }
            }
          } ?>
	      <form method="post" action="" class="form-element" enctype="multipart/form-data">
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info"><i class="ti-user"></i></span>
              </div>
              <input type="text" required name="nama" class="form-control pl-15" placeholder="Masukkan Nama Lengkap Anda">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info"><i class="ti-email"></i></span>
              </div>
              <input type="email" name="email" class="form-control pl-15" placeholder="Masukkan Email Anda">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info">+62</span>
                <!-- <span class="input-group-text bg-info border-info">+62</span> -->
              </div>
              <input type="text" required name="noTelpon" class="form-control pl-15" onkeypress="if(isNaN( String.fromCharCode(event.keyCode))) return false;" placeholder="Masukkan nomer telepon / WA anda">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info"><i class="mdi mdi-arrange-send-backward"></i></span>
              </div>
              <input type="text" name="subject" required class="form-control pl-15" placeholder="Subject">
            </div>
          </div>
          <div class="form-group">
            <div class="input-group mb-3">
              <!-- <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info"><i class="mdi mdi-autorenew"></i></span>
              </div> -->
              <div class="row">
                <div class="col-md-12">
                <div class="form-group">
                  <h5>Pilih Platform sesuai aplikasi anda <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <!-- <fieldset> -->
                      <input type="checkbox" name="platform[]" id="checkbox_2" value="Web">
                      <label for="checkbox_2">Aplikasi Web</label><br><br>
                    <!-- </fieldset> -->
                    <!-- <fieldset> -->
                      <input type="checkbox" name="platform[]" id="checkbox_3" value="Android">
                      <label for="checkbox_3">Aplikasi Android</label><br><br>
                    <!-- </fieldset> -->
                    <!-- <fieldset> -->
                      <input type="checkbox" name="platform[]" id="checkbox_4" value="IOS">
                      <label for="checkbox_4">Aplikasi IOS</label>
                    <!-- </fieldset> -->
                  </div>
              </div>
            </div>
          </div>
          <div class="form-group col-md-12">
            <div class="input-group ">
              <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info"></span>
              </div>
              <textarea name="deskripsi" required class="form-control pl-10" id="" cols="30" rows="10"></textarea>
            </div>
            <p class="text-danger">(*) Masukkan Deskripsi Project Aplikasi sesuai dengan keinginan anda</p>
          </div>
          <div class="form-group col-md-12">
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text bg-info border-info"><i class="mdi mdi-file-check"></i></span>
              </div>
              <input type="file" name="pendukung[]" class="form-control pl-15" multiple>
            </div>
            <p>Masukkan file pendukung</p>
          </div>
            <div class="row">   
              <div class="col-md-12 form-group">
                 <div class="g-recaptcha" data-sitekey="6LeN4oQUAAAAAL-pVIextohazViRvsX63sP3r85x"></div>
              </div>
            <!-- /.col -->
              <div class="col-12 text-center">
                <button type="submit" name="order" class="btn btn-info btn-block margin-top-10">ORDER <span class="mdi mdi-send"></span></button>
              </div>
            <!-- /.col -->
            </div>
        </form> 
    	</div>
      </div>
    </div>
  </div>
</footer>
<!-- <footer> -->
	<div>
    <div class="image-foir">
      <img src="../assets/images/logo/footer-background.png" width="1400px">
      <div class="row">
        <div class="col-lg-4 text-white foir-slide1">
          <h5>Contact Us</h5><br>
          <a target="_blank" class="text-white" href="https://www.google.com/maps/place/CAN+Creative+%7C+Jasa+Pembuatan+Web+dan+Aplikasi+Mobile+Android+IOS/@-6.9825902,110.3666202,17z/data=!3m1!4b1!4m5!3m4!1s0x2e708b4a573dd455:0xf275fb4e80f1afb5!8m2!3d-6.9825902!4d110.3688089"><i class="mdi mdi-map-marker-radius"></i> Jl. Warigalit II No. 285,<br> Krapyak, Semarang.</a><br><br>
          <a href="tel:02476439023" class="text-white"><i class="mdi mdi-phone"></i> (024) 76439023</a><br><br>
          <a class="text-white" href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to=hallo@can.web.id&tf=1" target="_blank"><i class="mdi mdi-contact-mail"></i> hallo@can.co.id</a>
        </div>
        <div class="col-lg-4 text-white foir-slide2">
          <h5>Our Service</h5>
          <li>Jasa Pembuatan Aplikasi</li>
          <li>Jasa Pembuatan Website</li>
          <li>Jasa Pembuatan Logo</li>
          <li>Jasa Optimasi SEO</li>
          <li>Jasa Maintenace Jaringan</li>
        </div>
        <div class="col-lg-4 text-white foir-slide3">
          <h5>Site Links</h5>
          <a href="http://can.co.id" class="text-white"><p style="padding-bottom: 0em;">Beranda</p></a>
          <a href="http://can.co.id/tentang-kami-2/" class="text-white"><p style="padding-bottom: 0em;">Tentang Kami</p></a>
          <a href="http://can.co.id/layanan/" class="text-white"><p style="padding-bottom: 0em;">Layanan</p></a>
          <a href="http://can.co.id/jasa-pembuatan-aplikasi-android-ios-portofolio/" class="text-white"><p style="padding-bottom: 0em;">Portofolio</p></a>
          <a href="#" class="text-white"><p style="padding-bottom: 0em;">Order</p></a>
        </div>
      </div>
    </div>
		<div  class="footer-bottom-a">
			<a href="#" class="text-white"><span class="left">© <?= date("Y") ?> CAN CREATIVE All rights reserved.</span></a>
	   		<a href="http://can.co.id" class="text-white"><span class="right">CAN Creative | Jasa Pembuatan Aplikasi Mobile Android & IOS</span></a>
		</div>
 	 </div>
<!-- </footer> -->
<div class="gotop" title="Top">
  <a href="#" class="text-white" id="back2Top"><i class="material-icons">keyboard_arrow_up</i></a>
</div>
<script src="../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  $(window).scroll(function() {
      var height = $(window).scrollTop();
      if (height > 100) {
          $('#back2Top').fadeIn();
      } else {
          $('#back2Top').fadeOut();
      }
  });
  $(document).ready(function() {
      $("#back2Top").click(function(event) {
          event.preventDefault();
          $("html, body").animate({ scrollTop: 0 }, "slow");
          return false;
      });

  });
</script>
</body>
</html>