<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

if(isset($_POST["save"])) {
	$profil["regId"] = $_POST["regId"];
	$profil["fullname"] = $_POST["fullname"];
	$profil["email"] = $_POST["email"];
	$profil["noTelpon"] = $_POST["noTelpon"];
	$profil["alamat"]  = $_POST["alamat"];
	$ubah = $log->updateProfil($profil);
}

$ctrl = $log->profile();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="../../assets/css/master_style.css">
	
	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
	
	<!-- fullCalendar -->
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
	
	<!-- Data Table-->
	<link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
	
	<!-- Bootstrap switch-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
	
	<!-- Morris charts -->
	<link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php
   if($_SESSION["U_GROUP_RULE"] == "TO_CEO" || $_SESSION["U_GROUP_RULE"] == "TO_ACCOUNT") {
      include_once('../../layouts/sidebar.php'); 
   } else {
   	  include_once('../../layouts/sidebar-new.php'); 
   }
   ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">My Profile</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Dashboard</li>
							<li class="breadcrumb-item active" aria-current="page">My Profile</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</div>  

    <!-- Main content -->
    <section class="content">
	  
		<div class="row">
		  
          <div class="col-12 col-lg-4">
            <div class="box">
			  <div class="box-header no-border p-0">				
                <a href="#">
                  <img class="img-fluid" src="../../assets/images/avatar/profile/<?= $ctrl["U_AVATAR"]; ?>" alt="">
                </a>
			  </div>
			  <div class="box-body">
				  <div class="text-center">
					<div class="user-contact flexbox">
						<a href="#" class="btn btn-icon-circle btn-primary btn-shadow"><i class="fa fa-envelope"></i></a>
						<a href="#" class="btn btn-icon-circle btn-success btn-shadow"><i class="fa fa-phone"></i></a>					
					</div>
					<h3 class="my-5"><a href="#"><?= $ctrl["U_FULLNAME"]; ?></a></h3>
					<h6 class="user-info mt-0 mb-5 text-lighter"><?php if($ctrl["U_POSISI_JABATAN"] == NULL) {echo "CEO";} else {echo $ctrl["U_POSISI_JABATAN"];} ?></h6>
					 <!--  <div class="gap-items user-social font-size-16 p-15">
						<a class="text-facebook" href="#"><i class="fa fa-facebook"></i></a>
						<a class="text-instagram" href="#"><i class="fa fa-instagram"></i></a>
						<a class="text-google" href="#"><i class="fa fa-google"></i></a>
						<a class="text-twitter" href="#"><i class="fa fa-twitter"></i></a>
					  </div> -->
					<p class="text-fade"><?= $ctrl["U_EMAIL"]; ?></p>
					<p class="text-fade"><?= $ctrl["U_TELPON"]; ?></p>
					<p class="text-uppercase text-fade"><?= $ctrl["U_ALAMAT"]; ?></p>
				  </div>
			  </div>
            </div>
          </div>
          <div class="col-12 col-lg-8">
        		<div class="box">
        			<div class="box-body">
        				<h4 align="center"><b>Update Profil</b></h4>
        				<form novalidate method="POST">
						  <div class="row">
							<div class="col-12">						
								<div class="form-group">
									<h5>Reg Id<span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="text" name="text" class="form-control" disabled value="<?= $ctrl["U_REGID"]; ?>"> 
										<input type="hidden" name="regId" value="<?= $ctrl["U_REGID"]; ?>">
									</div>
								</div>
								<div class="form-group">
									<h5>Nama Lengkap</h5>
									<div class="controls">
										<input type="text" name="fullname" class="form-control" value="<?= $ctrl["U_FULLNAME"]; ?>">
									</div>
								</div>
								<div class="form-group">
									<h5>Email Field <span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="email" name="email" class="form-control" required data-validation-required-message="This field is required" value="<?= $ctrl["U_EMAIL"]; ?>"> </div>
								</div>

								<div class="form-group">
									<h5>No Telpon</h5>
									<div class="controls">
										<input type="text" name="noTelpon" value="<?= $ctrl["U_TELPON"]; ?>" class="form-control">
									</div>
								</div>
								
								<div class="form-group">
									<h5>Textarea <span class="text-danger">*</span></h5>
									<div class="controls">
										<textarea name="alamat" id="textarea" class="form-control" required placeholder="Textarea text"><?= $ctrl["U_ALAMAT"]; ?></textarea>
									</div>
								</div>
		        			</div>
		        		  </div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<h5>Checkbox <span class="text-danger">*</span></h5>
										<div class="controls">
											<input type="checkbox" id="checkbox_1" value="single">
											<label for="checkbox_1">Centang jika data sudah sesuai</label>
										</div>								
									</div>
								</div>
							</div>
							<div class="text-xs-right">
								<input name="save" type="submit" class="btn btn-info" value="Submit">
								<!-- <button type="submit" class="btn btn-info">Submit</button> -->
							</div>
						</form>
        			</div>
        			<div class="box-body">
        				<h4 align="center"><b>Update Avatar</b></h4>
        				<form novalidate method="POST" enctype="multipart/form-data">
						  <div class="row">
							<div class="col-12">						
								<div class="form-group">
									<h5>Reg Id<span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="text" name="text" class="form-control" disabled value="<?= $ctrl["U_REGID"]; ?>"> 
										<input type="hidden" name="regId" value="<?= $ctrl["U_REGID"]; ?>">
									</div>
								</div>

								<div class="form-group">
									<h5>Pilih Gambar<span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="file" name="IMG_PATH" class="form-control" required> </div>
								</div>
								
		        			</div>
		        		  </div>
							<div class="text-xs-right">
								<input name="update" type="submit" class="btn btn-primary" value="Update">
								<!-- <button type="submit" class="btn btn-info">Submit</button> -->
							</div>
						</form>
						<?php 
						if(isset($_POST["update"])) {
							$logo["regId"] = $_POST["regId"];
							$logo["IMG_PATH"] = $_FILES["IMG_PATH"]["name"];
							$hh = $log->updateAvatar($logo);
						}

						 ?>
        			</div>
        		</div>
        	</div>	
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	
	
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
	  
	<div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	 
	  
	<!-- jQuery 3 -->
	<script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- date-range-picker -->
	<script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>

	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Slimscroll -->
	<script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- peity -->
	<script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
	
	<!-- Morris.js charts -->
	<script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
	<script src="../../assets/vendor_components/morris.js/morris.min.js"></script>
	
	<!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
	
	<!-- Bootstrap WYSIHTML5 -->
	<script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
	
	<!-- Superieur Admin App -->
	<script src="../../assets/js/template.js"></script>
	
	<!-- Superieur Admin for demo purposes -->
	<script src="../../assets/js/demo.js"></script>	
	
	<!-- Superieur Admin dashboard demo-->
	<script src="../../assets/js/pages/dashboard6.js"></script>
	
	<!-- Superieur Admin for Data Table -->
	<script src="../../assets/js/pages/data-table.js"></script>	

	<script src="../../assets/js/pages/validation.js"></script>
    <script src="../../assets/js/pages/form-validation.js"></script>
	
</body>
</html>
