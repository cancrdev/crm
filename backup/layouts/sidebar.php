<?php 
include_once('../../stucture/fungsi.php');
$log1 = new Model();

$ctrl1 = $log1->profile();
$not1   = $log1->notif1();
$not2   = $log1->notif2();
?>
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">
      
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="user-profile treeview">
          <a href="index.html">
			<img height="43px;" src="../../assets/images/avatar/profile/<?= $ctrl1["U_AVATAR"]; ?>" alt="user">
              <span>
				<span class="d-block font-weight-600 font-size-16"><?php echo $ctrl1["U_FULLNAME"]; ?></span>
        <?php if($_SESSION["U_GROUP_RULE"] === "TO_PRODUKSI") { ?>
        <span class="email-id"><?php echo $ctrl1["U_POSISI_JABATAN"]; ?></span>
        <?php } else { ?>
				<span class="email-id"><?php echo $ctrl1["U_EMAIL"]; ?></span>
        <?php } ?>
			  </span>
            <span class="pull-right-container">
              <!-- <i class="fa fa-angle-right pull-right"></i> -->
            </span>
          </a>
        </li>
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>PERSONAL</li>
        
	
		    <?php if($_SESSION["U_GROUP_RULE"] === "TO_CEO") { ?>
         <li class="">
          <a href="../../ceo/home">
            <i class="mdi mdi-view-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li> 
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-account-circle"></i>
            <span>Management User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../ceo/produksi/"><i class="mdi mdi-toggle-switch-off"></i>User Produksi</a></li>
            <li><a href="../../ceo/account/"><i class="mdi mdi-toggle-switch-off"></i>User Account</a></li>
          </ul>
        </li>    
        <li>
          <a href="../../ceo/client">
            <i class="mdi mdi-account"></i>
            <span>Client</span>
          </a>
        </li> 
		  
		    <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>PROJECT CONDITION</li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-checkbox-multiple-marked-outline"></i>
            <span>List Project Pra Produksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="../../ceo/project/new">
                <i class="mdi mdi-email"></i>
                <span>Project Baru</span>
              </a>
            </li>
            <li>
              <a href="../../ceo/project/waiting-analis">
                <i class="mdi mdi-email"></i>
                <span>Menunggu Persetujuan(Analis)</span>
              </a>
            </li> 
            <li>
              <a href="../../ceo/project/ready-followup">
                <i class="mdi mdi-email"></i>
                <span>Project Followup</span>
              </a>
            </li>  
            <li>
              <a href="../../ceo/project/hot-prospek">
                <i class="mdi mdi-email"></i>
                <span>Project Hot Prospek</span>
              </a>
            </li> 
            <li>
              <a href="../../ceo/project/reject">
                <i class="mdi mdi-email"></i>
                <span>Project Reject</span>
              </a>
            </li> 
            <li>
              <a href="../../ceo/project/project-deal">
                <i class="mdi mdi-email"></i>
                <span>Project Deal</span>
              </a>
            </li> 
          </ul>
        </li> 
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-checkbox-multiple-marked"></i>
            <span>List Project Process Produksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="../../../progres">
                <i class="mdi mdi-email"></i>
                <span>On Production</span>
              </a>
            </li>
          </ul>
        </li> 
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>REPORT & REVIEW</li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-ungroup"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <a href="#">
            <i class="mdi mdi-ungroup"></i>
            <span>History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout_boxed.html"><i class="mdi mdi-toggle-switch-off"></i>Report Project</a></li>
            <!-- <li><a href="pages/layout_fixed.html"><i class="mdi mdi-toggle-switch-off"></i>User Account</a></li> -->
          </ul>
        </li>  
        <?php } elseif($_SESSION["U_GROUP_RULE"] === "TO_ACCOUNT") { ?>
         <li class="">
          <a href="../../account/home">
            <i class="mdi mdi-view-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li> 
        <li class="">
          <a href="../../account/help">
            <i class="mdi mdi-help-circle-outline"></i>
            <span>Data Help / Bantuan</span>
          </a>
        </li> 
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>PROJECT CONDITION</li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-folder-multiple"></i>
            <span>List Project Praproduksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="../../account/project/tambah-project">
                <i class="mdi mdi-email"></i>
                <span>Tambah Project Offline</span>
              </a>
            </li> 
            <li>
              <a href="../../account/project/new">
                <i class="mdi mdi-email"></i>
                <span>Projek Masuk Terbaru </span>
                <?php if($not1["jml1"] < 1) { ?>
                <span class="btn-danger"> 0 Notif</span>
                <?php } else { ?>
                <span class="btn-warning"> <?= $not1["jml1"]; ?> Notif</span>
                <?php } ?>
              </a>
            </li> 
            <li>
              <a href="../../account/project/waiting-analis">
                <i class="mdi mdi-email"></i>
                <span>Project Proses Analisa</span>
                <?php if($not2["jml2"] < 1) { ?>
                <span class="btn-danger"> 0 Notif</span>
                <?php } else { ?>
                <span class="btn-warning"> <?= $not2["jml2"]; ?> Notif</span>
                <?php } ?>
              </a>
            </li> 
            <li>
              <a href="../../account/project/waiting-confirmation">
                <i class="mdi mdi-email"></i>
                <span>Project Menunggu Konfirmasi</span>
              </a>
            </li> 
            <li>
              <a href="../../account/project/ready-followup">
                <i class="mdi mdi-email"></i>
                <span>Project Siap Followup</span>
              </a>
            </li>  
            <li>
              <a href="../../account/project/hot-prospek">
                <i class="mdi mdi-email"></i>
                <span>Project Hot Prospek</span>
              </a>
            </li> 
            <li>
              <a href="../../account/project/project-reject">
                <i class="mdi mdi-email"></i>
                <span>Project Reject</span>
              </a>
            </li>
            <li>
              <a href="../../account/project/project-deal">
                <i class="mdi mdi-email"></i>
                <span>Project Deal</span>
              </a>
            </li> 
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-folder-google-drive"></i>
            <span>List Project Proses Produksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="../../account/project/priority">
                <i class="mdi mdi-email"></i>
                <span>Project Priority</span>
              </a>
            </li> 
          </ul>
        </li>
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>REPORT & REVIEW</li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-ungroup"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <a href="#">
            <i class="mdi mdi-ungroup"></i>
            <span>History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
        </li>  
        <?php } elseif($_SESSION["U_GROUP_RULE"] === "TO_PRODUKSI") { ?>
        <li class="">
          <a href="../../produksi/home">
            <i class="mdi mdi-view-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li> 
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>PROJECT CONDITION</li>
        <li class="treeview">
          <!-- <a href="#">
            <i class="mdi mdi-ungroup"></i>
            <span>Project Develop</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a> -->
          <a href="#">
            <i class="mdi mdi-ungroup"></i>
            <span>Team Production</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../produksi/team"><i class="mdi mdi-toggle-switch-off"></i>List Team Project</a></li>
            <!-- <li><a href="pages/layout_fixed.html"><i class="mdi mdi-toggle-switch-off"></i>User Account</a></li> -->
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-history"></i>
            <span>History</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../ceo/produksi/"><i class="mdi mdi-toggle-switch-off"></i>User Produksi</a></li>
            <li><a href="../../ceo/account/"><i class="mdi mdi-toggle-switch-off"></i>User Account</a></li>
          </ul>
        </li>
        <?php } else { ?>
        <?php } ?>
      </ul>
    </section>
  </aside>