<?php 
include_once('../../stucture/fungsi.php');
$log2 = new Model();

$ctrl2 = $log2->profile();
?>
<header class="main-header">
    <!-- Logo -->
    <a href="index.html" class="logo">
      <!-- mini logo -->
	  <div class="logo-mini">
      <span style="color: #fff">CRM Platform</span>
		  <!-- <span class="light-logo"><img src="../../assets/images/logo-light.png" alt="logo"></span> -->
		  <!-- <span class="dark-logo"><img src="../../assets/images/logo-dark.png" alt="logo"></span> -->
	  </div>
      <!-- logo-->
      <div class="logo-lg">
		  <!-- <span class="light-logo"><img src="../../assets/images/logo-light-text.png" alt="logo"></span> -->
	  	  <!-- <span class="dark-logo"><img src="../../assets/images/logo-dark-text.png" alt="logo"></span> -->
	  </div>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" style="background-color: #69cce0;">
      <!-- Sidebar toggle button-->
	  <div>
		  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		  </a>
	  </div>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		  
		  <!-- <li class="search-box">
            <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
            <form class="app-search" style="display: none;">
                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
			</form>
          </li>	 -->
		  <!-- User Account-->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../assets/images/avatar/profile/<?= $ctrl2["U_AVATAR"]; ?>" class="user-image rounded-circle" alt="User Image">
            </a>
            <ul class="dropdown-menu animated flipInY">
              <!-- User image -->
              <li class="user-header bg-img" style="background-image: url(../../assets/images/view.jpg)" data-overlay="3">
				  <div class="flexbox align-self-center">					  
				  	<img src="../../assets/images/avatar/profile/<?= $ctrl2["U_AVATAR"]; ?>" class="float-left rounded-circle" alt="User Image">					  
					<h4 class="user-name align-self-center">
					  <span><?php echo $ctrl2["U_FULLNAME"]; ?></span>
            <?php if($_SESSION["U_GROUP_RULE"] === "TO_PRODUKSI") { ?>
            <small><?php echo $ctrl2["U_POSISI_JABATAN"]; ?></small>
            <?php } else { ?>
					  <small><?php echo $ctrl2["U_EMAIL"]; ?></small>
            <?php } ?>
					</h4>
				  </div>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
				    <!-- <a class="dropdown-item" href="../../ceo/home/my-profile"><i class="ion ion-person"></i> My Profile</a> -->
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="../../log/person/setting"><i class="ion ion-settings"></i> Account Setting</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="../../log" onclick="return confirm('Yakin anda ingin keluar??')"><i class="ion-log-out"></i> Logout</a>
					<div class="dropdown-divider"></div>
					<div class="p-10"><a href="../../log/person/my-profile" class="btn btn-sm btn-rounded btn-success">View Profile</a></div>
              </li>
            </ul>
          </li>		
		  
          <!-- Messages -->
          <!-- Notifications -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="mdi mdi-bell"></i>
            </a>
            <ul class="dropdown-menu animated fadeInDown">
				
      			  <li class="header">
        				<div class="bg-img text-white p-20" style="background-image: url(../../assets/images/user-info.jpg)" data-overlay="5">
        					<div class="flexbox">
        						<div>
        							<h3 class="mb-0 mt-0">7 New</h3>
        							<span class="font-light">Notifications</span>
        						</div>
        						<div class="font-size-40">
        							<i class="mdi mdi-message-alert"></i>
        						</div>
        					</div>
        				</div>
      			  </li>
				
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu sm-scrol">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-info"></i> Curabitur id eros quis nunc suscipit blandit.
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#" class="text-white bg-danger">View all</a></li>
            </ul>
          </li>
          <!-- Tasks-->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="mdi mdi-bulletin-board"></i>
            </a>
            <ul class="dropdown-menu animated fadeInDown">
				
			  <li class="header">
				<div class="bg-img text-white p-20" style="background-image: url(../../assets/images/user-info.jpg)" data-overlay="5">
					<div class="flexbox">
						<div>
							<h3 class="mb-0 mt-0">6 New</h3>
							<span class="font-light">Tasks</span>
						</div>
						<div class="font-size-40">
							<i class="mdi mdi-bulletin-board"></i>
						</div>
					</div>
				</div>
			  </li>
				
        <li>
          <!-- inner menu: contains the actual data -->
          <ul class="menu sm-scrol">
            <li><!-- Task item -->
              <a href="#">
                <h3>
                  Lorem ipsum dolor sit amet
                  <small class="pull-right">30%</small>
                </h3>
                <div class="progress xs">
                  <div class="progress-bar progress-bar-danger" style="width: 30%" role="progressbar"
                       aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">30% Complete</span>
                  </div>
                </div>
              </a>
            </li>
            <!-- end task item -->
            <li><!-- Task item -->
              <a href="#">
                <h3>
                  Vestibulum nec ligula
                  <small class="pull-right">20%</small>
                </h3>
                <div class="progress xs">
                  <div class="progress-bar progress-bar-info" style="width: 20%" role="progressbar"
                       aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">20% Complete</span>
                  </div>
                </div>
              </a>
            </li>
            <!-- end task item -->
            <li><!-- Task item -->
              <a href="#">
                <h3>
                  Donec id leo ut ipsum
                  <small class="pull-right">70%</small>
                </h3>
                <div class="progress xs">
                  <div class="progress-bar progress-bar-success" style="width: 70%" role="progressbar"
                       aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">70% Complete</span>
                  </div>
                </div>
              </a>
            </li>
            <!-- end task item -->
            <li><!-- Task item -->
              <a href="#">
                <h3>
                  Praesent vitae tellus
                  <small class="pull-right">40%</small>
                </h3>
                <div class="progress xs">
                  <div class="progress-bar progress-bar-warning" style="width: 40%" role="progressbar"
                       aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">40% Complete</span>
                  </div>
                </div>
              </a>
            </li>
            <!-- end task item -->
            <li><!-- Task item -->
              <a href="#">
                <h3>
                  Nam varius sapien
                  <small class="pull-right">80%</small>
                </h3>
                <div class="progress xs">
                  <div class="progress-bar progress-bar-primary" style="width: 80%" role="progressbar"
                       aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">80% Complete</span>
                  </div>
                </div>
              </a>
            </li>
            <!-- end task item -->
            <li><!-- Task item -->
              <a href="#">
                <h3>
                  Nunc fringilla
                  <small class="pull-right">90%</small>
                </h3>
                <div class="progress xs">
                  <div class="progress-bar progress-bar-info" style="width: 90%" role="progressbar"
                       aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                    <span class="sr-only">90% Complete</span>
                  </div>
                </div>
              </a>
            </li>
            <!-- end task item -->
          </ul>
        </li>
        <li class="footer"></li>
          </ul>
        </li>
          <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog fa-spin"></i></a>
        </li>
        </ul>
      </div>
    </nav>
  </header>