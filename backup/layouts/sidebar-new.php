<?php 
include_once('../../stucture/fungsi.php');
$log1 = new Model();

$ctrl1 = $log1->profile();
$gg = $log1->getProfileClient();
?>
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">
      
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="user-profile treeview">
          <a href="">
			      <img height="43px;" src="../../assets/images/avatar/profile/<?= $ctrl1["U_AVATAR"]; ?>" alt="user">
               <span>
                   <span class="d-block font-weight-600 font-size-16"><?php echo $ctrl1["U_FULLNAME"]; ?></span>
                    <span class="email-id"><?php echo $ctrl1["U_EMAIL"]; ?></span><br>
                    <span><?php echo $gg["TC_INSTANSI"]; ?></span>
               </span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
          </a>
		    <!--  <ul class="treeview-menu">
            <li><a href="javascript:void()"><i class="fa fa-user mr-5"></i>My Profile </a></li>
      			<li><a href="javascript:void()"><i class="fa fa-money mr-5"></i>My Balance</a></li>
      			<li><a href="javascript:void()"><i class="fa fa-envelope-open mr-5"></i>Inbox</a></li>
      			<li><a href="javascript:void()"><i class="fa fa-cog mr-5"></i>Account Setting</a></li>
      			<li><a href="javascript:void()"><i class="fa fa-power-off mr-5"></i>Logout</a></li>
          </ul> -->
        </li>
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>PERSONAL</li>
	      <li class="">
          <a href="../../client/home/">
            <i class="mdi mdi-view-dashboard"></i>
            <span>Dashboard</span>
          </a>
        </li> 
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>LAYANAN</li>
		  
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-mailbox"></i> <span>Project</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../client/project/proces-admin"><i class="mdi mdi-toggle-switch-off"></i>Process Administration</a></li>
            <li><a href="../../client/project/"><i class="mdi mdi-toggle-switch-off"></i>Project On Progress</a></li>
            <li><a href="../../client/project/project-selesai"><i class="mdi mdi-toggle-switch-off"></i>Project Selesai</a></li>
          </ul>
        </li>
        <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>BILLING INFORMATION</li>
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-widgets"></i>
            <span>BILLING</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../client/billing/invoice"><i class="mdi mdi-credit-card"></i>Info Tagihan</a></li>
            <li><a href="../../client/billing/add-ticket"><i class="mdi mdi-credit-card"></i>Buat Tiket</a></li>
            <li><a href="../../client/billing/ticket"><i class="mdi mdi-credit-card"></i>List Tiket</a></li>
          </ul>
        </li>  
         <li class="header nav-small-cap"><i class="mdi mdi-drag-horizontal mr-5"></i>REPORT</li>
      
        <li class="treeview">
          <a href="#">
            <i class="mdi mdi-widgets"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="mdi mdi-credit-card"></i>Report</a></li>
          </ul>
        </li>  
        <li class="">
          <a href="../../client/help/">
            <i class="mdi mdi-search-web"></i>
            <span>Bantuan</span>
          </a>
        </li> 
      </ul>
    </section>
  </aside>