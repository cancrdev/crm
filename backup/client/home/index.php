<?php
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();
$gg = $log->getProfileClient();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>CRM - Customer Relationship Management</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="../../assets/css/master_style.css">
	
	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
    
	<!--amcharts -->
	<link href="https://www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
	
	<!-- daterange picker -->	
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
	
	<!-- Morris charts -->
	<link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">
   
    <!-- Vector CSS -->
    <link href="../../assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

     
  </head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar-new.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto w-p50">
				<h3 class="page-title">Analytics <?= $gg["TC_INSTANSI"]; ?></h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline">Dashboard</i></a></li>
							<li class="breadcrumb-item active" aria-current="page">Analythic</li>
						</ol>
					</nav>
				</div>
			</div>
			<div class="right-title text-right w-170">
				<span class="subheader_daterange font-weight-600" id="dashboard_daterangepicker">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title"></span>
						<span class="subheader_daterange-date text-primary"></span>
					</span>
					<a href="#" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content">
		
	  <div class="row">
		  
		<div class="col-md-12 col-lg-4">
            <div class="box box-body bg-success">
              <div class="flexbox">
                <div id="linechart"></div>
                <div class="text-right">
                  <span>Project Process Administration</span><br>
                  <span>
                    <i class="ion-ios-arrow-up text-white"></i>
                    <span class="font-size-18 ml-1">589</span>
                  </span>
                </div>
              </div>
            </div>
        </div>  
		  
		<div class="col-md-12 col-lg-4">
            <div class="box box-body bg-warning">
              <div class="flexbox">
                <div id="barchart"></div>
                <div class="text-right">
                  <span>Project<br> Process Development</span><br>
                  <span>
                    <i class="ion-ios-arrow-up text-white"></i>
                    <span class="font-size-18 ml-1">%90</span>
                  </span>
                </div>
              </div>
            </div>
        </div>
		  
		<div class="col-md-12 col-lg-4">
            <div class="box box-body bg-primary">
              <div class="flexbox">
                <div id="discretechart"></div>
                <div class="text-right">
                  <span>Project<br>Selesai</span><br>
                  <span>
                    <i class="ion-ios-arrow-up text-white"></i>
                    <span class="font-size-18 ml-1">%90</span>
                  </span>
                </div>
              </div>
            </div>
        </div>
		  
		<div class="col-12 col-lg-7">
			<div class="row">
			  <div class="col-md-6 col-12">
				<div class="box">
				  <div class="box-body">
					  <div class="media align-items-center p-0">
						  <h3 class="mx-0 mb-5 font-weight-500">Daily Sales</h3>
					  </div>
					  <div class="flexbox align-items-center mt-5">
						<div>
						  <h4 class="no-margin"><span class="text-primary">+$17,800</span></h4>
						</div>
						<div class="text-right">
						  <h4 class="no-margin"><span class="text-success">+1.35%</span></h4>
						</div>
					  </div>
				</div>
				<div class="box-footer p-0 no-border">
					<div class="chart"><canvas id="chartjs1" class="h-80"></canvas></div>
				</div>
			   </div>
			  </div>

			  <div class="col-md-6 col-12">
					<div class="box">
					  <div class="box-body">
						  <div class="media align-items-center p-0">
							  <h3 class="mx-0 mb-5 font-weight-500">Member Profit</h3>
						  </div>
						  <div class="flexbox align-items-center mt-5">
							<div>
							  <h4 class="no-margin"><span class="text-success">+$17,800</span></h4>
							</div>
							<div class="text-right">
							  <h4 class="no-margin"><span class="text-danger">-1.35%</span></h4>
							</div>
						  </div>
					</div>
					<div class="box-footer p-0 no-border">
						<div class="chart"><canvas id="chartjs2" class="h-80"></canvas></div>
					</div>
				 </div>
			  </div>

			  <div class="col-md-6 col-12">
					<div class="box">
					  <div class="box-body">
						  <div class="media align-items-center p-0">
							  <h3 class="mx-0 mb-5 font-weight-500">Issue Reports</h3>
						  </div>
						  <div class="flexbox align-items-center mt-5">
							<div>
							  <h4 class="no-margin"><span class="text-warning">-27,497</span></h4>
							</div>
							<div class="text-right">
							  <h4 class="no-margin"><span class="text-danger">-1.35%</span></h4>
							</div>
						  </div>
					</div>
					<div class="box-footer p-0 no-border">
						<div class="chart"><canvas id="chartjs3" class="h-80"></canvas></div>
					</div>
				 </div>
			  </div>

			  <div class="col-md-6 col-12">
				  <div class="box">
					  <div class="box-body">
						  <div class="media align-items-center p-0">
							  <h3 class="mx-0 mb-5 font-weight-500">Orders</h3>
						  </div>
						  <div class="flexbox align-items-center mt-5">
							<div>
							  <h4 class="no-margin"><span class="text-info">17,800</span></h4>
							</div>
							<div class="text-right">
							  <h4 class="no-margin"><span class="text-success">+1.35%</span></h4>
							</div>
						  </div>
					  </div>
					<div class="box-footer p-0 no-border">
						<div class="chart"><canvas id="chartjs4" class="h-80"></canvas></div>
					</div>
				 </div>
			  </div>

			  <div class="col-12">
				  <!-- interactive chart -->
				  <div class="box">
					<div class="box-header with-border">
					  <h4 class="box-title">User Statistics</h4>
					</div>
					<div class="box-body">
					  <div id="interactive" style="height: 300px;"></div>
					</div>
					<!-- /.box-body-->
				  </div>
				  <!-- /.box -->
			 </div>
			<!-- /.col -->
			</div>
		</div> 
		  
		<div class="col-lg-5 col-12">
		  <!-- Chart -->
		  <div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title">Yeary Sales</h4>
				<ul class="box-controls pull-right">
				  <li><a class="box-btn-close" href="#"></a></li>
				  <li><a class="box-btn-slide" href="#"></a></li>	
				  <li><a class="box-btn-fullscreen" href="#"></a></li>
				</ul>
			</div>
			<div class="box-body">
			  <div id="morris-area-chart" style="height: 340px;"></div>
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->
		  <!-- Chart -->
		  <div class="box">
			<div class="box-header with-border">
			  <h4 class="box-title">Social Ads Campaigns</h4>
				<ul class="box-controls pull-right">
				  <li><a class="box-btn-close" href="#"></a></li>
				  <li><a class="box-btn-slide" href="#"></a></li>	
				  <li><a class="box-btn-fullscreen" href="#"></a></li>
				</ul>
			</div>
			<div class="box-body pt-0">
				<div id="e_chart_2" class="" style="height:300px;"></div>
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->
		</div>
		  
		<div class="col-lg-8 col-12">
			<div class="box">
				<div class="box-header with-border">
				  <h4 class="box-title">Sales</h4>
					<ul class="box-controls pull-right">
					  <li><a class="box-btn-close" href="#"></a></li>
					  <li><a class="box-btn-slide" href="#"></a></li>	
					  <li><a class="box-btn-fullscreen" href="#"></a></li>
					</ul>
				</div>
				<div class="box-body">
					<div class="chart">						
						<div id="chartdiv1" style="height:500px;"></div>
					</div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->

		  </div>
		  
	    <div class="col-12 col-lg-4">
		  <!-- Default box -->
		  <div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">USA</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div id="usa" style="height: 495px"></div>
			</div>
			<!-- /.box-body -->
		  </div>
		  <!-- /.box -->
	    </div>
		  
		<div class="col-xl-4 col-lg-6">
		  <div class="box box-inverse bg-google">
			<div class="box-body">
			  <h3 class="text-white mt-0">Vivamus condimentum erat non turpis placerat, at volutpat metus commodo.</h3>
			  <small>14 April, 2018 via web</small>
			  <div class="mt-20">
				<i class="fa fa-google-plus font-size-26"></i>
				<ul class="list-inline float-right mb-0">
				  <li class="list-inline-item">
					<i class="fa fa-thumbs-o-up"></i> 845
				  </li>
				  <li class="list-inline-item">
					<i class="fa fa-share"></i> 956
				  </li>
				</ul>
			  </div>
			</div>
		  </div>
	  </div>

	  <div class="col-xl-4 col-lg-6">
		  <div class="box box-inverse bg-twitter">
			<div class="box-body">
			  <h3 class="text-white mt-0">Vivamus condimentum erat non turpis placerat, at volutpat metus commodo.</h3>
			  <small>14 April, 2018 via web</small>
			  <div class="mt-20">
				<i class="fa fa-twitter font-size-26"></i>
				<ul class="list-inline float-right mb-0">
				  <li class="list-inline-item">
					<i class="fa fa-heart"></i> 845
				  </li>
				  <li class="list-inline-item">
					<i class="fa fa-thumbs-up"></i> 956
				  </li>
				</ul>
			  </div>
			</div>
		  </div>
	  </div>

	  <div class="col-xl-4 col-lg-6">
		  <div class="box box-inverse bg-facebook">
			<div class="box-body">
			  <h3 class="text-white mt-0">Vivamus condimentum erat non turpis placerat, at volutpat metus commodo.</h3>
			  <small>14 April, 2018 via web</small>
			  <div class="mt-20">
				<i class="fa fa-facebook font-size-26"></i>
				<ul class="list-inline float-right mb-0">
				  <li class="list-inline-item">
					<i class="fa fa-thumbs-o-up"></i> 845
				  </li>
				  <li class="list-inline-item">
					<i class="fa fa-star"></i> 956
				  </li>
				</ul>
			  </div>
			</div>
		  </div>
	  </div>
		  
      </div>
			      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
		  <li class="nav-item">
			<a class="nav-link" href="javascript:void(0)">FAQ</a>
		  </li>
		  <li class="nav-item">
			<a class="nav-link" href="#">Purchase Now</a>
		  </li>
		</ul>
    </div>
	  &copy; 2018 <a href="https://www.multipurposethemes.com/">Multi-Purpose Themes</a>. All Rights Reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
	  
	<div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-danger"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                <p>Will be July 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-warning"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                <p>New Email : jhone_doe@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-info"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                <p>disha@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-success"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Code Change</h4>

                <p>Execution time 15 Days</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Web Design
                <span class="label label-danger pull-right">40%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Data
                <span class="label label-success pull-right">75%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Order Process
                <span class="label label-warning pull-right">89%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Development 
                <span class="label label-primary pull-right">72%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <input type="checkbox" id="report_panel" class="chk-col-grey" >
			<label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>

            <p>
              general settings information
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <input type="checkbox" id="allow_mail" class="chk-col-grey" >
			<label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <input type="checkbox" id="expose_author" class="chk-col-grey" >
			<label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <input type="checkbox" id="show_me_online" class="chk-col-grey" >
			<label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <input type="checkbox" id="off_notifications" class="chk-col-grey" >
			<label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">              
              <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
              Delete chat history
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	 
	  
	<!-- jQuery 3 -->
	<script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- date-range-picker -->
	<script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
    <!--amcharts charts -->
	<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
	<script src="https://www.amcharts.com/lib/3/serial.js"></script>
	<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
	<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
	
	<!-- Morris.js charts -->
	<script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
	<script src="../../assets/vendor_components/morris.js/morris.min.js"></script>	
	
	
	<!-- Slimscroll -->
	<script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- echarts -->
	<script src="../../assets/vendor_components/echarts/dist/echarts-en.min.js"></script>
	<script src="../../assets/vendor_components/echarts/echarts-liquidfill.min.js"></script>
	
	<!-- FastClick -->
	<script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- Sparkline -->
	<script src="../../assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- FLOT CHARTS -->
	<script src="../../assets/vendor_components/Flot/jquery.flot.js"></script>
	
	<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
	<script src="../../assets/vendor_components/Flot/jquery.flot.resize.js"></script>
	
	<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
	<script src="../../assets/vendor_components/Flot/jquery.flot.pie.js"></script>
	
	<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
	<script src="../../assets/vendor_components/Flot/jquery.flot.categories.js"></script>
	
	<!-- Chart -->
	<script src="../../assets/vendor_components/chart.js-master/Chart.min.js"></script>
	<script src="../../assets/js/pages/chartjs-int.js"></script>
	
    <!-- Vector map JavaScript -->
    <script src="../../assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="../../assets/vendor_components/jvectormap/lib2/jquery-jvectormap-us-aea-en.js"></script>
	
	<!-- Superieur Admin App -->
	<script src="../../assets/js/template.js"></script>
	
	<!-- Superieur Admin for demo purposes -->
	<script src="../../assets/js/demo.js"></script>	
	
	<!-- Superieur Admin dashboard demo (This is only for demo purposes) -->
	<script src="../../assets/js/pages/dashboard4.js"></script>
	<script src="../../assets/js/pages/dashboard4-chart.js"></script>
	
	
	
</body>
</html>
