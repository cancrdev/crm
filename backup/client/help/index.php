<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getHelp();


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <!-- <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css"> -->


  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  <style type="text/css">
    /* Styles for page */
/*.container {
  width: 70%;
  margin: 150px auto;
}*/

/*body {
  color: #4B4B4B;
  font-family: sans-serif;
}
 a {
  cursor: pointer;
  color: #4B4B4B;
  text-decoration: none;
}*/
h1 {
  text-transform: uppercase;
  text-align: center;
  font-weight: normal;
  letter-spacing: 10px;
  font-size: 25px;
  line-height: 1.5;
}
  /*a {
  text-align: center;
  letter-spacing: 3px;
}*/

span {
  letter-spacing: 0px;
}

p {
  font-weight: 200;
  line-height: 1.5;
  font-size: 14px;
  text-align: justify;
  letter-spacing: 3px;
}

/* Styles for Accordion */
.toggle:last-child {
  border-bottom: 1px solid #dddddd;
}
.toggle .toggle-title {
  position: relative;
  display: block;
  border-top: 1px solid #dddddd;
  margin-bottom: 6px;
}
.toggle .toggle-title h3 {
  font-size: 20px;
  margin: 0px;
  line-height: 1;
  cursor: pointer;
  font-weight: 200;
}
.toggle .toggle-inner {
  padding: 7px 25px 10px 25px;
  display: none;
  margin: -7px 0 6px;
}
.toggle .toggle-inner div {
  max-width: 100%;
}
.toggle .toggle-title .title-name {
  display: block;
  padding: 25px 25px 14px;
}
.toggle .toggle-title a i {
  font-size: 22px;
  margin-right: 5px;
}
.toggle .toggle-title i {
  position: absolute;
  background: url("http://arielbeninca.com/Storage/plus_minus.png") 0px -24px no-repeat;
  width: 24px;
  height: 24px;
  transition: all 0.3s ease;
  margin: 20px;
  right: 0;
}
.toggle .toggle-title.active i {
  background: url("http://arielbeninca.com/Storage/plus_minus.png") 0px 0px no-repeat;
}

  </style>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php if($_SESSION["U_GROUP_RULE"] == "TO_CEO" || $_SESSION["U_GROUP_RULE"] == "TO_ACCOUNT") {
      include_once('../../layouts/sidebar.php'); 
   } else {
      include_once('../../layouts/sidebar-new.php'); 
   } ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Data Help / Bantuan</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Help / Bantuan</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">        
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Help / Bantuan</h3>
            <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
            <br>
            <!-- <a href="" class="btn btn-info mb-5"><i class="mdi mdi-account-multiple-plus"></i></a> -->
          </div>
              <!-- /.box-header -->
          <div class="box-body">
              <!-- <div class="container">  -->
                <?php 
                if(count($ctrl) > 0) {
                if(is_array($ctrl)) {
                foreach($ctrl as $cd) {
                ?>
                <div class="toggle">
                  <div class="toggle-title ">
                    <h3>
                    <i></i>
                    <span class="title-name"><?= $cd["TH_TITTLE"]; ?></span>
                    </h3>
                    <div style="padding-left: 1.8em;">
                      <h6><i></i> <span class="mdi mdi-account-outline "><?= $cd["TG_CREATED_BY"]; ?></span> || <span class="mdi mdi-calendar-clock "><?= $log->TanggalIndo($cd["TH_CREATED_AT"]); ?></span></h6>
                    </div>
                  </div>
                  <div class="toggle-inner">
                    <p><?= $cd["TH_ISI"]; ?></p>
                  </div>
               </div><!-- END OF TOGGLE --><br><br><br><br>
               <?php }} } else { ?>
              <div class="toggle">
                <div class="toggle-title " align="center">
                  <h5>
                  <i></i>
                  <span class="title-name">Data Help Kosong :)</span>
                  </h5>
                </div>
                <div class="toggle-inner" align="center">
                   
                </div>
              </div>  
              <?php } ?>          
          <!-- </div>            -->
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->          
      </div>
      </section>

    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <!-- <aside class="control-sidebar control-sidebar-light"> -->
    
  <!-- <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>   -->
    <!-- Create the tabs -->
    <!-- <ul class="nav nav-tabs control-sidebar-tabs"> -->
      <!-- <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li> -->
      <!-- <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li> -->
    <!-- </ul> -->
    <!-- Tab panes -->
  <!-- </aside> -->
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- SlimScroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- CK Editor -->
  <script src="../../assets/vendor_components/ckeditor/ckeditor.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin for editor -->
  <script src="../../assets/js/pages/editor.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 

   <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <script type="text/javascript">
    if( jQuery(".toggle .toggle-title").hasClass('active') ){
        jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
      }
      jQuery(".toggle .toggle-title").click(function(){
        if( jQuery(this).hasClass('active') ){
          jQuery(this).removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
        }
        else{ jQuery(this).addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
        }
      });
  </script>
</body>
</html>
