<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getAccount();

if(isset($_POST["save"])) {
  $account["username"] = $_POST["username"];
  $account["fullname"] = $_POST["fullname"];
  $account["email"]    = $_POST["email"];
  $account["noTelpon"] = $_POST["noTelpon"];
  $account["alamat"]   = $_POST["alamat"];
  $account["posisi"]   = $_POST["posisi"]; 
  $ctrl = $log->tambahAccount($account);
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">List Data User Account</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">User Account</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">        
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">List Data User Account</h3>
            <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
            <br>
            <a href="" data-toggle="modal" data-target="#myModal" class="btn btn-info"><i class="mdi mdi-account-multiple-plus"></i></a>

          </div>
              <!-- /.box-header -->
          <div class="box-body">
              <div class="table-responsive">
                  <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Pegawai</th>
                        <th>E-mail</th>
                        <th>Telpon</th>
                        <th>Jabatan</th>
                        <th>Status</th>
                        <td>Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(is_array($ctrl)) {
                        $no = 1;
                        foreach($ctrl as $cc) {
                      ?>
                      <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $cc["U_FULLNAME"]; ?></td>
                        <td><?= $cc["U_EMAIL"]; ?></td>
                        <td><?= $cc["U_TELPON"]; ?></td>
                        <td><?php if($cc["U_GROUP_RULE"] === "TO_ACCOUNT") {echo "Account";} ?></td>
                        <?php if($cc["U_STATUS"] === "USER_ACTIVE") { ?>
                        <td><a href="status?id=<?= $cc["U_REGID"]; ?>&status=USER_INACTIVE" class="btn btn-success ubah-status"><?php if($cc["U_STATUS"] === "USER_ACTIVE") {echo "AKTIF";} ?></a></td>
                        <?php } else { ?>
                        <td><a href="status?id=<?= $cc["U_REGID"]; ?>&status=USER_ACTIVE" class="btn btn-danger ubah-status"><?php if($cc["U_STATUS"] === "USER_INACTIVE") {echo "TIDAK AKTIF";} ?></a></td>
                        <?php } ?>
                        <td>
                          <a href="detail?id=<?= $cc["U_REGID"]; ?>" class="btn btn-info mb-5"><span class="mdi mdi-airplay"></span></a>
                          <a class="btn btn-warning" data-toggle="modal" id="edit" data-id="<?= $cc["U_REGID"]; ?>" data-target="#myModal1"><span class="glyphicon glyphicon-pencil"></span></a>
                          <!-- <a href="" class="btn btn-warning mb-5"><span class="mdi mdi-border-color"></span></a> -->
                          <a href="delete?id=<?= $cc["U_REGID"]; ?>" type="submit" class="btn btn-danger delete-link"><span class="mdi mdi-close-outline"></span></a>
                        </td>
                      </tr>
                      <?php } } ?>
                    </tbody>          
                  </table>
              </div>              
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->          
      </div>
      </section>
    <!-- /.content -->
  </div>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Account</h4>
        </div>
        <form method="post">
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" required class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username">
          </div>
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Nama Lengkap</label>
            <input type="text" class="form-control" name="fullname" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap">
          </div>
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" required class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">No Telpon</label>
            <input type="text" class="form-control" name="noTelpon" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter No Telpon">
          </div>
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Alamat</label>
            <input type="text" class="form-control" name="alamat" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Alamat">
          </div>
          <div class="form-group col-md-12">
            <label for="exampleInputEmail1">Posisi</label>
            <input type="text" class="form-control" name="posisi" id="exampleInputEmail1" aria-describedby="emailHelp" value="Account">
          </div>
        
          <div class="modal-footer">
            <a class="btn btn-default" data-dismiss="modal">Close</a>
            <input type="submit" name="save" class="btn btn-info" placeholder="Simpan">
            <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- /.content-wrapper -->
   <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ubah Account</h4>
        </div>
        <div class="modal-body">
          <div class="fetched-data"></div>
        </div>
      </div>
    </div>
  </div>
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- date-range-picker -->
  <script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Morris.js charts -->
  <script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
  <script src="../../assets/vendor_components/morris.js/morris.min.js"></script>
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <script src="../../assets/js/pages/dashboard6.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

  <script type="text/javascript">
  $(document).ready(function(){
        $('#myModal1').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : 'edit.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.delete-link').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin menghapus data?",
            text: "Data yang dihapus akan hilang",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-status').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah Kamu yakin mengubah status?",
            text: "Status akan otomatis berubah",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Update it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
  
</body>
</html>
