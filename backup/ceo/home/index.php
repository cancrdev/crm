<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();
//
$allP    = $log->getAllProject();
$pros    = $log->getHotProspek();
$deal    = $log->getDealProject();
$reject  = $log->get_project_reject();
$prio    = $log->getPriority();
//end

$selesai = $log->countProEnd();
$dev     = $log->countProdev();
$process = $log->countProcess();
//project process
$blnP1 = $log->get_chart_project_proces('01');
$blnP2 = $log->get_chart_project_proces('02');
$blnP3 = $log->get_chart_project_proces('03');
$blnP4 = $log->get_chart_project_proces('04');
$blnP5 = $log->get_chart_project_proces('05');
$blnP6 = $log->get_chart_project_proces('06');
$blnP7 = $log->get_chart_project_proces('07');
$blnP8 = $log->get_chart_project_proces('08');
$blnP9 = $log->get_chart_project_proces('09');
$blnP10 = $log->get_chart_project_proces('10');
$blnP11 = $log->get_chart_project_proces('11');
$blnP12 = $log->get_chart_project_proces('12');

//project selesai 
$thnS1 = $log->get_chart_project_selesai('01');
$thnS2 = $log->get_chart_project_selesai('02');
$thnS3 = $log->get_chart_project_selesai('03');
$thnS4 = $log->get_chart_project_selesai('04');
$thnS5 = $log->get_chart_project_selesai('05');
$thnS6 = $log->get_chart_project_selesai('06');
$thnS7 = $log->get_chart_project_selesai('07');
$thnS8 = $log->get_chart_project_selesai('08');
$thnS9 = $log->get_chart_project_selesai('09');
$thnS10 = $log->get_chart_project_selesai('10');
$thnS11 = $log->get_chart_project_selesai('11');
$thnS12 = $log->get_chart_project_selesai('12');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="../../assets/css/master_style.css">
	
	<!-- Superieur Admin skins -->
	<link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
	
	<!-- fullCalendar -->
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
	<link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
	
	<!-- Data Table-->
	<link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
	
	<!-- Bootstrap switch-->
	<link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
	
	<!-- Morris charts -->
	<link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->	  
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto w-p50">
				<h3 class="page-title">CRM - (Customer Relationship Management)</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item active" aria-current="page">Dashboard CEO</li>
						</ol>
					</nav>
				</div>
			</div>
			<div class="right-title text-right w-170">
				<span class="subheader_daterange font-weight-600" id="dashboard_daterangepicker">
					<span class="subheader_daterange-label">
						<span class="subheader_daterange-title"></span>
						<span class="subheader_daterange-date text-primary"></span>
					</span>
					<a href="#" class="btn btn-sm btn-primary">
						<i class="fa fa-angle-down"></i>
					</a>
				</span>
			</div>
		</div>
	</div>

    <!-- Main content -->
    <section class="content">		
	  	<div class="row">
			<div class="col-xl-4 col-md-6 col-12">
				<a href="../produksi/">
				<div class="flexbox flex-justified text-center mb-30 pull-up" style="background-color: #8edbea !important; color: #fff;">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-account-circle font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getProduksi()); ?></div>
					<span>Total User Produksi</span>
				  </div>
				</div></a>
			</div>
			<!-- /.col -->
			<div class="col-xl-4 col-md-6 col-12">
				<a href="../account/">
				<div class="flexbox flex-justified text-center bg-info mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-account font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getAccount()); ?></div>
					<span>Total User Account</span>
				  </div>
				</div></a>
			</div>
			<!-- /.col -->
			<div class="col-xl-4 col-md-6 col-12">
				<a href="../client/">
				<div class="flexbox flex-justified text-center mb-30 pull-up" style="background-color: #0f95af !important; color: #fff;">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-thumb-up-outline font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getClient()); ?></div>
					<span>Total Client</span>
				  </div>
				</div></a>
			</div>
			<!-- /.col -->
			<div class="col-xl-4 col-md-6 col-12">
				<div class="flexbox flex-justified text-center mb-30 pull-up" style="background-color: #0ec569 !important;color: #fff;">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getAllProject()); ?></div>
					<span>Total Project</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-4 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-success mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getProjectMasuk()); ?></div>
					<span>Total Project Masuk</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-4 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-success mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getWaitingCEO()); ?></div>
					<span>Menunggu Konfirmasi</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center mb-30 pull-up" style="background-color: #e66ecf !important; color: #fff;">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getWaitingAnalis()); ?></div>
					<span>Menunggu Analisis</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center mb-30 pull-up" style="background-color: #d44bba !important; color:#fff;">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getFollowup()); ?></div>
					<span>Total Project Followup</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center bg-primary mb-30 pull-up">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getHotProspek()); ?></div>
					<span>Total Project Hot Prospek</span>
				  </div>
				</div>
			</div>
			<div class="col-xl-3 col-md-6 col-12">
				<div class="flexbox flex-justified text-center mb-30 pull-up" style="background-color: #9c0d81 !important; color: #fff;">
				  <div class="no-shrink py-30">
					<span class="mdi mdi-ticket font-size-50"></span>
				  </div>

				  <div class="py-30 bg-white text-dark">
					<div class="font-size-30 countnm"><?= count($log->getDealProject()); ?></div>
					<span>Total Project Siap Produksi(Deal)</span>
				  </div>
				</div>
			</div>
			<!-- /.col -->
			<div class="col-12 col-lg-4">
				<div class="box">
				  <div class="box-header with-border">
					<h3 class="box-title">Persentase Project</h3>
					<div class="box-tools pull-right">
						<ul class="card-controls">
						  <li class="dropdown">
							<a data-toggle="dropdown" href="#"><i class="ion-android-more-vertical"></i></a>
							<!-- <div class="dropdown-menu dropdown-menu-right">
							  <a class="dropdown-item active" href="#">Today</a>
							  <a class="dropdown-item" href="#">Yesterday</a>
							  <a class="dropdown-item" href="#">Last week</a>
							  <a class="dropdown-item" href="#">Last month</a>
							</div> -->
						  </li>
						  <li><a href="" class="link card-btn-reload" data-toggle="tooltip" title="" data-original-title="Refresh"><i class="mdi mdi-toggle-switch-off-thin"></i></a></li>
						</ul>
					</div>
				  </div>

				  <div class="box-body">
					<div class="text-center py-20">                  
					  <div class="donut" data-peity='{ "fill": ["#ffb22b", "#20bf54", "#c31212", " #398bf7"], "radius": 120, "innerRadius": 80  }' ><?= $process["jml2"].",".$dev["jml"].",".$selesai["jml1"].",".$selesai["jml1"]; ?></div>
					</div>

					<ul class="list-inline">
					  <li class="flexbox mb-5" style="color: #ffb22b;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Proses Admin</span>
						</div>
						<div><?= $process["jml2"]; ?></div>
					  </li>

					  <li class="flexbox mb-5" style="color: #20bf54;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Proses Develop</span>
						</div>
						<div><?= $dev["jml"]; ?></div>
					  </li>

					  <li class="flexbox" style="color: #c31212;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Selesai</span>
						</div>
						<div><?= $selesai["jml1"]; ?></div>
					  </li>

					  <li class="flexbox" style="color: #398bf7;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Maintenance</span>
						</div>
						<div><?= $selesai["jml1"]; ?></div>
					  </li>
					  <li class="flexbox" style="color: #398bf7;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Total Project</span>
						</div>
						<div><?= count($allP); ?></div>
					  </li>
					</ul>
				  </div>
				</div>
			</div>
			<div class="col-12 col-lg-8">
				<div class="box">
				  <div class="box-header with-border">
					<h3 class="box-title">Persentase Status Project</h3>
					<div class="box-tools pull-right">
						<ul class="card-controls">
						  <li class="dropdown">
							<a data-toggle="dropdown" href="#"><i class="ion-android-more-vertical"></i></a>
							<!-- <div class="dropdown-menu dropdown-menu-right">
							  <a class="dropdown-item active" href="#">Today</a>
							  <a class="dropdown-item" href="#">Yesterday</a>
							  <a class="dropdown-item" href="#">Last week</a>
							  <a class="dropdown-item" href="#">Last month</a>
							</div> -->
						  </li>
						  <li><a href="" class="link card-btn-reload" data-toggle="tooltip" title="" data-original-title="Refresh"><i class="mdi mdi-toggle-switch-off-thin"></i></a></li>
						</ul>
					</div>
				  </div>

				  <div class="box-body">
					<div class="text-center py-20">                  
					  <div class="chart" id="e_chart_1" style="height: 258px;"></div>
					</div>

					<ul class="list-inline">
					  <li class="flexbox mb-5" style="color: #69cce0;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Hot Prospek</span>
						</div>
						<div><?= count($pros); ?></div>
					  </li>

					  <li class="flexbox mb-5" style="color: #05b085;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Deal</span>
						</div>
						<div><?= count($deal); ?></div>
					  </li>

					  <li class="flexbox" style="color: #ba69aa;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Priority</span>
						</div>
						<div><?= count($prio); ?></div>
					  </li>

					  <li class="flexbox" style="color: #ef483e;">
						<div>
						  <span class="badge badge-dot badge-lg mr-1"></span>
						  <span>Project Reject</span>
						</div>
						<div><?= $reject; ?></div>
					  </li>
					</ul>
				  </div>
				</div>
			</div>
			<!-- /.col -->
			<!-- /.col -->			
			<div class="col-lg-12 col-12">		  
			  <!-- AREA CHART -->
			  <div class="box">
				<div class="box-header with-border">
					<h4 class="box-title">Persentase Project masuk - Project Selesai Tahun <?= date("Y"); ?></h4>
				</div>
				<div class="box-body">
					<ul class="list-inline text-right mt-10 mr-10">
						<li>
							<h5 class="mb-0"><i class="mdi mdi-toggle-switch-off mr-5 text-success"></i>Project Masuk</h5>
						</li>
						<li>
							<h5 class="mb-0"><i class="mdi mdi-toggle-switch-off mr-5 text-danger"></i>Project Selesai</h5>
						</li>
					</ul>
				  <div id="morris-area-chart2-a" style="height: 330px;"></div>
				</div>
				<!-- /.box-body -->
			  </div>
			  <!-- /.box -->
			</div>
			<!-- /.col -->		      
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
	
	
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
	  
	<div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	 
	  
	<!-- jQuery 3 -->
	<script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
	
	<!-- popper -->
	<script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- date-range-picker -->
	<script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
	
	<!-- Slimscroll -->
	<script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- peity -->
	<script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
	
	<!-- Morris.js charts -->
	<script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
	<script src="../../assets/vendor_components/morris.js/morris.min.js"></script>

	<!-- e-chart -->
	<script src="../../assets/vendor_components/echarts/dist/echarts-en.min.js"></script>
	<script src="../../assets/vendor_components/echarts/echarts-liquidfill.min.js"></script>
	<script src="../../assets/vendor_components/echarts/ecStat.min.js"></script>
	
	<!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
	
	<!-- Bootstrap WYSIHTML5 -->
	<script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
	
	<!-- Superieur Admin App -->
	<script src="../../assets/js/template.js"></script>
	
	<!-- Superieur Admin for demo purposes -->
	<script src="../../assets/js/demo.js"></script>	
	
	<!-- Superieur Admin dashboard demo-->
	<script src="../../assets/js/pages/dashboard6.js"></script>
	
	<!-- Superieur Admin for Data Table -->
	<script src="../../assets/js/pages/data-table.js"></script>	
	<script type="text/javascript">

	    var morrisCharts = function() {

            var months = ['Januari', 'Febrari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

                Morris.Area({
                  element: 'morris-area-chart2-a',
                  data: [
                    { month: "<?php echo date('Y')?>-01", a: "<?= $blnP1; ?>", b: "<?= $thnS1; ?>"},
                    { month: "<?php echo date('Y')?>-02", a: "<?= $blnP2; ?>", b: "<?= $thnS2; ?>"},
                    { month: "<?php echo date('Y')?>-03", a: "<?= $blnP3; ?>", b: "<?= $thnS3; ?>"},
                    { month: "<?php echo date('Y')?>-04", a: "<?= $blnP4; ?>", b: "<?= $thnS4; ?>"},
                    { month: "<?php echo date('Y')?>-05", a: "<?= $blnP5; ?>", b: "<?= $thnS5; ?>"},
                    { month: "<?php echo date('Y')?>-06", a: "<?= $blnP6; ?>", b: "<?= $thnS6; ?>"},
                    { month: "<?php echo date('Y')?>-07", a: "<?= $blnP7; ?>", b: "<?= $thnS7; ?>"},
                    { month: "<?php echo date('Y')?>-08", a: "<?= $blnP8; ?>", b: "<?= $thnS8; ?>"},
                    { month: "<?php echo date('Y')?>-09", a: "<?= $blnP9; ?>", b: "<?= $thnS9; ?>"},
                    { month: "<?php echo date('Y')?>-10", a: "<?= $blnP10; ?>", b: "<?= $thnS10; ?>"},
                    { month: "<?php echo date('Y')?>-11", a: "<?= $blnP11; ?>", b: "<?= $thnS11; ?>"},
                    { month: "<?php echo date('Y')?>-12", a: "<?= $blnP12; ?>", b: "<?= $thnS12; ?>"}
                  ],
                  xkey: 'month',
                  ykeys: ['a', 'b'],
                  labels: ['Project Masuk', 'Project Selesai'],
                   pointSize: 0,
			        fillOpacity: 1,
			        pointStrokeColors:['#05b085', '#dc3545'],
			        behaveLikeLine: true,
			        gridLineColor: '#e0e0e0',
			        // lineWidth: 0,
			        smooth: false,
			        hideHover: 'auto',
			        lineColors: ['#05b085', '#dc3545'],
			        resize: true,
                   xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
                    var month = months[x.getMonth()];
                    return month;
                  },
                  dateFormat: function(x) {
                    var month = months[new Date(x).getMonth()];
                    return month;
                  }
                }); }();


        if( $('#e_chart_1').length > 0 ){
			var eChart_3 = echarts.init(document.getElementById('e_chart_1'));
			var colors = ['#ba69aa', '#69cce0'];
			var option3 = {
				tooltip: {
					backgroundColor: 'rgba(33,33,33,1)',
					borderRadius:0,
					padding:10,
					textStyle: {
						color: '#fff',
						fontStyle: 'normal',
						fontWeight: 'normal',
						fontFamily: "'Nunito Sans', sans-serif",
						fontSize: 12
					}	
				},
				series: [
					{
						name:'',
						type:'pie',
						radius: ['50%', '80%'],
						color: ['#ba69aa', '#05b085', '#69cce0', '#ef483e'],
						label: {
							normal: {
								formatter: '{b}\n{d}%'
							},
					  
						},
						data:[
							{value:<?= count($prio); ?>, name:'PROJECT PRIORITY'},
							{value:<?= count($deal); ?>, name:'PROJECT DEAL'},
							{value:<?= count($pros); ?>, name:'PROJECT HOT PROSPEK'},
							{value:<?= $reject; ?>, name:'PROJECT REJECT'},
						]
					}
				]
			};
			eChart_3.setOption(option3);
			eChart_3.resize();
		};
	</script>
	
</body>
</html>
