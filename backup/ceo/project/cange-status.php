<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$buff = $log->getPenawaranByKode($_GET["view"]);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <?php if($_GET["call"] === "reject") { ?>
          <h3 class="page-title">Keterangan Penawaran Reject Followup</h3>
          <?php } else { ?>
          <h3 class="page-title">Keterangan Penawaran Approve Followup</h3>
          <?php } ?>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Komentar Pada Penawaran No. <?= $buff["TPN_NO_PENAWARAN"]; ?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
       <div class="row">
        <div class="col-md-12">

          <?php 
          if (isset($_POST["kirim"])) {
            # code...\
            $reject["penId"] = $_POST["penId"];
            $reject["proId"] = $_POST["proId"];
            $reject["status"] = $_POST["status"];
            $reject["keterangan"] = $_POST["keterangan"];
            // $bn = $log->statusReject($reject);
          }
          ?>
          <form method="post">
            <input type="hidden" name="proId" value="<?= $buff["TPN_PROJECTID"]; ?>">
            <input type="hidden" name="penId" value="<?= $buff["TPN_KODE"]; ?>">
            <?php if($_GET["call"] === "reject") { ?>
            <input type="hidden" name="status" value="WAITING_ANALIS">
            <?php } else { ?>
            <input type="hidden" name="status" value="PROJECT_FOLLOWUP">
            <?php } ?>
            <div class="form-group col-md-8">
              <textarea class="form-control" name="keterangan" rows="6" cols="10" placeholder="Isikan Keterangan" required></textarea>
            </div>
            <div>
              <p style="color: red">(*) Berikan Keterangan untuk penawaran <?php if($_GET["call"] === "reject") {echo "reject";} else {echo "approve";} ?> untuk pencatatan Logs system</p>
            </div>
            <div class="form-group col-md-6">
              <!-- <a href="" name="kirim" class="btn btn-success">Kirim <i class="mdi mdi-send"></i></a> -->
              <input type="submit" name="kirim" class="btn btn-success" value="Kirim">
              <a class="btn btn-primary" href="detail-file?view=<?= $buff["TPN_PROJECTID"]; ?>"><i class="mdi mdi-arrow-left"></i> Kembali</a>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- date-range-picker -->
  <script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  å
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Morris.js charts -->
  <script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
  <script src="../../assets/vendor_components/morris.js/morris.min.js"></script>
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <script src="../../assets/js/pages/dashboard6.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 
  
</body>
</html>
