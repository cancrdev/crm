<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getDetailProject($_GET["view"]);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">

  <!-- owlcarousel-->
  <link rel="stylesheet" href="../../assets/vendor_components/OwlCarousel2/dist/assets/owl.carousel.css">
  <link rel="stylesheet" href="../../assets/vendor_components/OwlCarousel2/dist/assets/owl.theme.default.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Data Tables</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Detai Project dengan No <?= $ctrl["TP_NO_PROJECT"]." - ".$ctrl["TP_NAMA_PROJECT"]; ?></li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
    
    <div class="row">
          <div class="col-md-5">
            <div class="box card-inverse bg-img text-center py-80" style="background-image: url(../../images/gallery/thumb/12.jpg)" data-overlay="5">
              <div class="card-body">
                <span class="bb-1 opacity-80 pb-2">Project Masuk : <?= $log->TanggalIndo($ctrl["TP_PROJECT_TGL"]); ?></span>
                <br><br>
                <span class="bb-1 opacity-80 pb-2"><h3>Detail Client / Customer</h3></span>
                <h4><?= $ctrl["TC_NAMA"]; ?></h4>
                <h6><?= $ctrl["TC_EMAIL"]; ?></h6>
                <h6><?= $ctrl["TC_TELPON"]; ?></h6>
                <br>
                <h5><?= $ctrl["TC_INSTANSI"]; ?></h5>
              </div>
            </div>
          </div>
      
      <div class="col-md-7 col-12">       
        <div class="box">
          <div class="box-body p-0">
            <div class="media">
            <!-- <a class="pull-left" href="#">
              <img class="media-object w-200" src="../../assets/images/avatar/gambar/<?= $ctrl["TO_BUKTI_PENDUKUNG"]; ?>" alt="">
            </a> -->
            <div class="media-body">
              <span class="bb-1 opacity-80 pb-2"><h4 class="media-heading mb-15"><a href="#"><?= $ctrl["TP_NAMA_PROJECT"]; ?></h4></span><br>
              <h6>Platform : <?= $ctrl["TP_PLATFORM"]." |  ".$log->TanggalIndo($ctrl["TP_PROJECT_TGL"]); ?></h6><br>
              <?php 
              $ext = array("bmp", "jpg", "png");
              $fileName = $ctrl["TO_BUKTI_PENDUKUNG"];
              $pecah = explode(".", $fileName);
              $ekstensi = $pecah[1];
              if(in_array($ekstensi, $ext)) { ?>
              <a href="" class="btn btn-info data-kosong"><i class="fa fa-file"></i> Lihat Detail File</a>
              <?php } else { ?>
                <a href="detail-file?view=<?= $ctrl["TP_PROJECTID"]; ?>" class="btn btn-info"><i class="fa fa-file"></i> Lihat Detail File</a>
              <?php } ?>
             </div>
          </div>          
          <div class="col-12">
            <div class="box">

              <div class="row no-gutters">
                <div class="col-md-8">
                  <div class="box-body">
                    <h4 align="center">Project ID : <a href="#"><?= $ctrl["TP_NO_PROJECT"]; ?></a></h4>

                    <p><?= $ctrl["TP_DESKRIPSI"]; ?></p>
                  </div>
                </div>
                <!-- <div class="col-4 bg-img d-none d-md-block" style="background-image: url(../../assets/images/avatar/gambar/<?= $ctrl["TO_BUKTI_PENDUKUNG"]; ?>)"></div> -->
              </div><br><br><br>

              <!-- <form action="coba" method="post"> -->
                <?php if($ctrl["TP_STATUS"] === "PROJECT_ANALIS_ACC") { ?>
               
                <!-- <input type="hidden" name="id" value="<?= $ctrl["TP_PROJECTID"]; ?>"> -->
                <!-- <input type="hidden" name="status" value="WAITING_ANALIS"> -->              
                <?php } elseif($ctrl["TP_STATUS"] === "PROJECT_FOLLOWUP") { ?>
                <p style="color: red;">(*) Ubah status jika sudah sesuai.</p>
                <!-- <input type="hidden" name="id" value="<?= $ctrl["TP_PROJECTID"]; ?>"> -->
                <!-- <input type="hidden" name="status" value="WAITING_ANALIS"> -->
                <div class="flexbox align-items-center mt-3">
                  <a href="status/ubah-status?v=<?= $ctrl["TP_PROJECTID"] ?>&status=PROJECT_DEAL" class="btn btn-success ubah-status1" style="color: #fff;">Deal</a>
                  <a href="status/ubah-status?v=<?= $ctrl["TP_PROJECTID"] ?>&status=PROJECT_REJECT" class="btn btn-danger ubah-status2" style="color: #fff;">Reject</a>
                  <!-- <button name="ubah" class="btn btn-xs btn-bold btn-primary ubah-status" style="color: #fff;">Ubah Status Siap Analisa</button> -->
                </div>  
                <?php } elseif($ctrl["TP_STATUS"] === "PROJECT_DEAL") { ?>
                
                <?php } elseif($ctrl["TP_STATUS"] === "HOT_PROSPEK") { ?>
                <?php } ?>
            </div>
          </div>
        </div>
    </section>
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- date-range-picker -->
  <script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <script type="text/javascript">
      function add_foto() {
          var objTo = document.getElementById('foto_fields')
          var divtest = document.createElement("div");  
          divtest.innerHTML = "<div class='form-group'><div class='input-group mb-3'><div class='input-group-prepend'><span class='input-group'></span></div></div><input type='file' name='pendukung[]' id='pendukung' class='form-control'></div>";    
          objTo.appendChild(divtest)
      }
  </script>

  <script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-status1').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah yakin mengubah status Project Deal?",
            text: "Status Project akan otomatis berubah menjadi Project Deal",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.ubah-status2').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah yakin mengubah status Project Reject?",
            text: "Status Project akan otomatis berubah menjadi Project Reject",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>

<script type="text/javascript">
  jQuery(document).ready(function($){
        $('.data-kosong').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Oops....",
            text: "Data detail kosong :(",
            type: "error",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
</body>
</html>


