<?php
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->getDetailFile($_GET["view"]);

$cl = $log->getDetailProject($_GET["view"]);

$pnw = $log->getDetailPenawaran($_GET["view"]);

$dpnw = $log->detailBerkasPenawaran($_GET["view"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM </title>
  
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
   
    <!-- Popup CSS -->
    <!-- <link href="../../assets/vendor_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet"> -->

  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
   <style type="text/css">
    .alias {
      position: relative;
      z-index: 1;
      top: 0px;
    }
    .text-alias {
      position: absolute;
      top: 60px;
      z-index: 2;
      color: #000;
      text-align: center;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
</head>

<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>
  
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title">User Box</h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item" aria-current="page">Box Cards</li>
              <li class="breadcrumb-item active" aria-current="page">User Box</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
    <!-- Main content -->
    <section class="content">
     
      <!-- START Card With Image -->
      <h4 class="box-title mb-10">Detail File Project ID : <?= $cl["TP_PROJECTID"]; ?></h4><br>
        <!-- /.col -->
      <!-- START Card With Image -->
      <div class="row fx-element-overlay">
        <?php if(is_array($ctrl) || is_object($ctrl)) { 
          foreach($ctrl as $cc) {
        ?>
        <div class="col-md-12 col-lg-3">
          <div class="box box-default">
            <div class="fx-card-item">
              <div class="fx-card-avatar fx-overlay-1"> 
                <?php $ext = array("bmp", "jpg", "png");
                      $fileName = $cc["TO_BUKTI_PENDUKUNG"];
                      $pecah = explode(".", $fileName);
                      $ekstensi = $pecah[1];
                      if(in_array($ekstensi, $ext)) {
                 ?>
                <img src="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>" alt="user">
                <div class="fx-overlay scrl-dwn">
                  <ul class="fx-info">
                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>"><i class="ion-search"></i></a></li>
                    <li><a class="btn default btn-outline" href="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>" download><i class="ion-link"></i></a></li>
                  </ul>
                </div>
                <?php } else { ?>
                <div>
                  <iframe style="font-size: auto" src="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>"></iframe><br><br>
                  <a class="btn btn-success" target="_blank" href="../../assets/images/avatar/gambar/<?= $cc["TO_BUKTI_PENDUKUNG"]; ?>"><i class="mdi mdi-desktop-mac"></i> Lihat File</a>
                </div>
                <?php } ?>
              </div>
              <div class="fx-card-content">
                <h4 class="box-title">Project ID : <?= $cc["TP_NO_PROJECT"]; ?></h4> <small><?= $log->tanggalIndo($cc["TP_CREATED_AT"]); ?></small>
                <br> </div>
              </div>
          </div>
          <!-- /.box -->
        </div>
        <?php } } ?>
      </div><br>
      <h4>Detail Penawaran pada Project No : <?= $cl["TP_NO_PROJECT"]; ?></h4>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List Data Penawaran</h3>
        </div>
        <div class="box-body">
          <div class="table-responsive">
              <table id="example" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No Penawaran</th>
                    <th>Nama Project</th>
                    <th>Project Masuk</th>
                    <th style="text-align: center;">Detail Berkas</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(is_array($pnw)) {
                    $no = 1;
                    foreach($pnw as $ccd) {
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $ccd["TPN_NO_PENAWARAN"]; ?></td>
                    <td><?= $ccd["TP_NAMA_PROJECT"]; ?></td>
                    <td><?= $log->TanggalIndo($ccd["TP_PROJECT_TGL"]); ?></td>
                    <td style="text-align: center;">
                      <a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-center"><i class="mdi mdi-airplay"></i></a>
                    </td>
                    <td>
                      <a href="cange-status?call=approve&view=<?= $ccd["TPN_KODE"]; ?>" class="btn btn-success mb-5 check-approve"><span class="mdi mdi-check"></span> Approve</a>
                      <a href="cange-status?call=reject&view=<?= $ccd["TPN_KODE"]; ?>" class="btn btn-danger mb-5 check-reject"><span class="mdi mdi-cancel"></span> Reject</a>
                    </td>
                  </tr>
                  <?php } } ?>
                </tbody>          
              </table>
          </div>              
        </div>
      </div>
      <!-- <div class="flexbox align-items-center mt-3"> -->
          <!-- <p style="color: red;">(*) Ubah status siap difollowup jika sudah sesuai.</p><br> -->
          <a onclick="goBack()" class="btn btn-primary" style="color: #fff;"><i class="mdi mdi-arrow-left"></i> Kembali</a><br><br>
          <!-- <button name="ubah" class="btn btn-xs btn-bold btn-primary ubah-status" style="color: #fff;">Ubah Status Siap Analisa</button> -->
      <!-- </div>  -->
        <!-- /.col -->
      <!-- END Card with image -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
   <?php include_once('../../layouts/footer.php'); ?>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-danger"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Admin Birthday</h4>

                <p>Will be July 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-warning"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>

                <p>New Email : jhone_doe@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-info"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>

                <p>disha@demo.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-success"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Code Change</h4>

                <p>Execution time 15 Days</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Web Design
                <span class="label label-danger pull-right">40%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Data
                <span class="label label-success pull-right">75%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Order Process
                <span class="label label-warning pull-right">89%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Development 
                <span class="label label-primary pull-right">72%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <!-- /.tab-pane -->
    </div>
  </aside>
  <div class="modal center-modal fade" id="modal-center" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title"><i class="mdi mdi-history"></i> List Detail Berkas</h5>
          <a type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span>
          </a>
          </div>
          <div class="modal-body"><br><br>
            <div class="row fx-element-overlay">
              <?php if(is_array($dpnw) || is_object($dpnw)) { 
                foreach($dpnw as $ai) {
              ?>
              <div class="col-md-12 col-lg-6">
                <div class="box box-default">
                  <div class="fx-card-item">
                    <div class="fx-card-avatar fx-overlay-1"> 
                       <?php $ext = array("bmp", "jpg", "png");
                            $pdf = array("pdf");
                            $fileName = $ai["TPN_BERKAS"];
                            $pecah = explode(".", $fileName);
                            $ekstensi = $pecah[1];
                            if(in_array($ekstensi, $ext)) {
                       ?>
                      <img src="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>" alt="user">
                      <div class="fx-overlay scrl-dwn">
                        <ul class="fx-info">
                          <li><a class="btn default btn-outline image-popup-vertical-fit" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>"><i class="ion-search"></i></a></li>
                          <li><a class="btn default btn-outline" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>" download><i class="ion-link"></i></a></li>
                        </ul>
                      </div>
                      <?php }elseif(in_array($ekstensi, $pdf)) { ?>
                      <div>
                        <iframe style="font-size: auto" src="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>"></iframe><br><br>
                        <a class="btn btn-success" target="_blank" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>"><i class="mdi mdi-desktop-mac"></i> Lihat File</a>
                      </div>
                      <?php } else { ?>
                      <img src="../../assets/images/gallery/thumb/12.jpg" alt="aliases" class="alias"><h5 class="text-alias">File bisa berisi .doc,.csv,.xls dll</h5><br><br>
                      <a class="btn btn-info" href="../../assets/images/avatar/file/<?= $ai["TPN_BERKAS"]; ?>" download><i class="mdi mdi-download"></i> Download</a>
                      <?php } ?>
                    </div>
                    <div class="fx-card-content">
                      <h4 class="box-title">Penawaran No : <?= $ai["TPN_NO_PENAWARAN"]; ?></h4> <small><?= $log->tanggalIndo($cc["TP_CREATED_AT"]); ?></small>
                      <br> </div>
                    </div>
                </div>
                <!-- /.box -->
              </div>
              <?php } } ?>

            </div>
          </div>
          <div class="modal-footer">
            <a class="btn btn-primary" data-dismiss="modal" style="color: #fff;"><i class="mdi mdi-close"></i> Close</a>
            <!-- <input type="submit" name="clientUp" class="btn btn-info" placeholder="Simpan"> -->
            <!-- <button name="save" class="btn btn-primary">Simpan</button> -->
          </div>
        </div>
      </div>
 </div>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.min.js"></script>
  
  <!-- SlimScroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  
  <!-- FastClick -->
  <!-- <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script> -->

  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Magnific popup JavaScript -->
    <!-- <script src="../../assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script> -->
    <!-- <script src="../../assets/vendor_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script> -->
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>

  <!-- Superieur Admin for demo purposes -->
  <!-- <script src="../../assets/js/demo.js"></script> -->

   <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 

  <script type="text/javascript">
    function goBack() {
        window.history.back();
    }
  </script>

  <script type="text/javascript">
    jQuery(document).ready(function($){
        $('.check-approve').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah yakin Approve Project?",
            text: "Status akan otomatis berubah",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "cancel",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });

     jQuery(document).ready(function($){
        $('.check-reject').on('click',function(){
            var getLink = $(this).attr('href');
            swal({
            title: "Apakah yakin Reject Project ?",
            text: "Status akan otomatis berubah",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            cancelButtonText: "cancel",
            closeOnConfirm: false,
            closeOnCancel: true,
          },function(){
            window.location.href = getLink
          });
            return false;
          });
    });
</script>
  
</body>
</html>
