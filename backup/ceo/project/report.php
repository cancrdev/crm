<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/gallery/full/deals1.png">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="page-title">Data Tables</h3>
          <div class="d-inline-block align-items-center">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                <li class="breadcrumb-item active" aria-current="page">Report Project</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="row">        
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Report Project Per Status</h3>
            <!-- <h6 class="box-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6> -->
            <br>
            <!-- <a href="" data-toggle="modal" data-target="#myModal" class="btn btn-info mb-5"><i class="mdi mdi-account-multiple-plus"></i></a> -->
          </div>
              <!-- /.box-header -->
          <div class="box-body">
              <div class="box-body pb-0">
                <form action="" method="post">
                  <div class="row">
                      <div class="col-md-4 col-12">
                        <div class="form-group">
                        <label>Tanggal Awal</label>
                        <input type="text" class="form-control" required name="tanggalAwal" id="datepicker" data-date-format="dd-mm-yyyy">
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <div class="col-md-4 col-12">
                        <div class="form-group">
                        <label>Tanggal Akhir</label>
                        <input type="text" class="form-control" required name="tanggalAkhir" id="datepicker1" data-date-format="dd-mm-yyyy">
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <div class="col-md-4 col-12">
                        <div class="form-group">
                        <label>Status</label>
                        <select class="form-control select2 selectpicker" required name="status" style="width: 100%;">
                          <option selected="selected">- Pilih Status -</option>
                          <option value="ORDER_MASUK">Project Baru</option>
                          <option value="WAITING_ANALIS">Project Proses Analis</option>
                          <option value="PROJECT_ANALIS_ACC">Project Menunggu Approval CEO</option>
                          <option value="PROJECT_FOLLOWUP">Project Followup</option>
                          <!-- <option value="PROSPEK">Project Prospek</option> -->
                          <option value="PROJECT_DEAL">Project Deal</option>
                          <!-- <option value="PRIORITY">Project Priority</option> -->
                          <option value="PROJECT_DEVELOP">Project Develop</option>
                          <option value="PROJECT_SELESAI">Project Selesai</option>
                          <option value="PROJECT_REJECT">Project Reject</option>
                        </select>
                        </div>
                      </div>
                      <div class="col-md-12 col-12">
                        <div class="form-group" align="right">
                        <label></label>
                        <input type="submit" name="cari" class="btn btn-dark btn-lg" value="Cari">
                        </div>
                        <!-- /.form-group -->
                      </div>
                  </div>
                </form>
              </div>
              <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Project No</th>
                    <th>Nama Project</th>
                    <th>Platform</th>
                    <th>Project Masuk</th>
                    <th>Project Due</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(isset($_POST["cari"])) {
                    $report["status"]        = $_POST["status"];
                    $report["tanggalAwal"]   = $_POST["tanggalAwal"];
                    $report["tanggalAkhir"]  = $_POST["tanggalAkhir"];
                    $ht = $log->getReportPerStatus($report);
                    $no = 1;
                    if($ht == NULL || $ht == "") {
                    foreach($ht as $bc) :
                  ?>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $bc["TP_NO_PROJECT"]; ?></td>
                    <td><?= $bc["TP_NAMA_PROJECT"]; ?></td>
                    <td><?= $bc["TP_PLATFORM"]; ?></td>
                    <td><?= $log->TanggalIndo($bc["TP_PROJECT_TGL"]); ?></td>
                    <td><?= $log->TanggalIndo($bc["TP_PROJECT_DUE"]); ?></td>
                  </tr>
                  <?php endforeach; } else { ?>
                   <script type="text/javascript">swal("Oops...", "Data yang anda cari tidak tersedia :(", "error");</script>
                  <?php } } ?>
                </tbody>
                </table>
              </div>            
          </div>
              <!-- /.box-body -->
        </div>
            <!-- /.box -->          
      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Morris.js charts -->
  <!-- <script src="../../assets/vendor_components/raphael/raphael.min.js"></script> -->
  <!-- <script src="../../assets/vendor_components/morris.js/morris.min.js"></script> -->
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <!-- <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script> -->
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <!-- <script src="../../assets/js/pages/dashboard6.js"></script> -->
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript">
    $(function () {
      $("#datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    });

    $(function () {
      $("#datepicker1").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    });
  </script>
  
</body>
</html>
