<?php 
session_start();
include_once('../../stucture/fungsi.php');
$log = new Model();

$ctrl = $log->detailClient($_GET["id"]);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../assets/images/favicon.ico">

    <title>Dashboard - CRM</title>
    
  <!-- Bootstrap 4.0-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap/dist/css/bootstrap.css">
  
  <!-- Bootstrap extend-->
  <link rel="stylesheet" href="../../assets/css/bootstrap-extend.css">
  
  <!-- theme style -->
  <link rel="stylesheet" href="../../assets/css/master_style.css">
  
  <!-- Superieur Admin skins -->
  <link rel="stylesheet" href="../../assets/css/skins/_all-skins.css">
  
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../assets/vendor_components/fullcalendar/fullcalendar.print.min.css" media="print">
  
  <!-- Data Table-->
  <link rel="stylesheet" type="text/css" href="../../assets/vendor_components/datatable/datatables.min.css"/>
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
  
  <!-- Bootstrap switch-->
  <link rel="stylesheet" href="../../assets/vendor_components/bootstrap-switch/switch.css">
  
  <!-- Morris charts -->
  <link rel="stylesheet" href="../../assets/vendor_components/morris.js/morris.css">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]--> 
  </head>

<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <?php include_once('../../layouts/navbar.php'); ?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include_once('../../layouts/sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->   
    <div class="content-header">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="page-title">Detail Data Client</h3>
        <div class="d-inline-block align-items-center">
          <nav>
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
              <li class="breadcrumb-item" aria-current="page">Dashboard</li>
              <li class="breadcrumb-item active" aria-current="page">Detail Client <?= $ctrl["TC_NAMA"]; ?></li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="right-title">
        <div class="dropdown">
          <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i class="mdi mdi-dots-horizontal"></i></button>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
            <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
            <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
            <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
            <div class="dropdown-divider"></div>
            <button type="button" class="btn btn-success">Submit</button>
          </div>
          </div>
      </div>
    </div>
  </div>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-12">
        </div>
        <!-- /.col -->
        <div class="col-lg-12 col-12">
            <div class="box box-inverse bg-img" style="background-image: url(../../images/gallery/full/1.jpg);" data-overlay="2">
              <div class="flexbox px-20 pt-20">
                <label class="toggler toggler-danger text-white">
                  <input type="checkbox">
                  <i class="fa fa-heart"></i>
                </label>
                <div class="dropdown">
                  <a data-toggle="dropdown" href="#"><i class="ti-more-alt rotate-90 text-white"></i></a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Profile</a>
                    <!-- <a class="dropdown-item" href="#"><i class="fa fa-picture-o"></i> Shots</a> -->
                    <!-- <a class="dropdown-item" href="#"><i class="ti-check"></i> Follow</a> -->
                    <div class="dropdown-divider"></div>
                    <!-- <a class="dropdown-item" href="#"><i class="fa fa-ban"></i> Block</a> -->
                  </div>
                </div>
              </div>

              <div class="box-body text-center pb-50">
                <a href="#">
                  <img class="avatar avatar-xxl avatar-bordered" src="../../images/avatar/5.jpg" alt="">
                </a>
                <h4 class="mt-2 mb-0"><a class="hover-primary text-white" href="#"><?= $ctrl["TC_NAMA"]; ?></a></h4>
                <span><i class="mdi mdi-email w-20"></i> <?= $ctrl["TC_EMAIL"]; ?></span>
              </div>

              <ul class="box-body flexbox flex-justified text-center" data-overlay="4">
                <li>
                  <span class="opacity-60">No Telpon</span><br>
                  <span class="font-size-20"><?= $ctrl["TC_TELPON"]; ?></span>
                </li>
                <li>
                  <span class="opacity-60">Alamat</span><br>
                  <span class="font-size-20"><?= $ctrl["TC_ALAMAT"]; ?></span>
                </li>
                <li>
                  <span class="opacity-60">Instansi</span><br>
                  <span class="font-size-20"><?= $ctrl["TC_INSTANSI"]; ?></span>
                </li>
              </ul>
            </div>   
            <div>
              <a href="index" class="btn btn-primary"><i class="mdi mdi-keyboard-backspace"></i>Kembali</a>
            </div>  
        </div>   

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <?php include_once('../../layouts/footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    
  <div class="rpanel-title"><span class="btn pull-right"><i class="ion ion-close" data-toggle="control-sidebar"></i></span> </div>  
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab">Tasks</a></li>
      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab">General</a></li>
    </ul>
    <!-- Tab panes -->
  </aside>
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
   
    
  <!-- jQuery 3 -->
  <script src="../../assets/vendor_components/jquery-3.3.1/jquery-3.3.1.js"></script>
  
  <!-- popper -->
  <script src="../../assets/vendor_components/popper/dist/popper.min.js"></script>
  
  <!-- date-range-picker -->
  <script src="../../assets/vendor_components/moment/min/moment.min.js"></script>
  <script src="../../assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  
  <!-- Bootstrap 4.0-->
  <script src="../../assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>
  
  <!-- Slimscroll -->
  <script src="../../assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
  
  <!-- FastClick -->
  <script src="../../assets/vendor_components/fastclick/lib/fastclick.js"></script>
  
  <!-- peity -->
  <script src="../../assets/vendor_components/jquery.peity/jquery.peity.js"></script>
  
  <!-- Morris.js charts -->
  <script src="../../assets/vendor_components/raphael/raphael.min.js"></script>
  <script src="../../assets/vendor_components/morris.js/morris.min.js"></script>
  
  <!-- This is data table -->
    <script src="../../assets/vendor_components/datatable/datatables.min.js"></script>
  
  <!-- Bootstrap WYSIHTML5 -->
  <script src="../../assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
  
  <!-- Superieur Admin App -->
  <script src="../../assets/js/template.js"></script>
  
  <!-- Superieur Admin for demo purposes -->
  <script src="../../assets/js/demo.js"></script> 
  
  <!-- Superieur Admin dashboard demo-->
  <script src="../../assets/js/pages/dashboard6.js"></script>
  
  <!-- Superieur Admin for Data Table -->
  <script src="../../assets/js/pages/data-table.js"></script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</body>
</html>
